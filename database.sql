-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 29, 2017 at 11:48 AM
-- Server version: 5.6.36
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `unyqtv`
--

-- --------------------------------------------------------

--
-- Table structure for table `blocks`
--

CREATE TABLE `blocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `blocks`
--

INSERT INTO `blocks` (`id`, `title`, `position`, `content`, `active`, `created_at`, `updated_at`) VALUES
(1, 'КАКВО Е UnyQTV?', 'kakvo-e-unyqtv', '<div style=\"text-align: center;\"><span style=\"font-family: LatoBlack; color: rgb(227, 49, 59); font-size: 19px;\">UnyQ</span><strong style=\"font-family: LatoBlack; color: rgb(92, 92, 92); font-size: 19px;\">TV</strong><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px;\">&nbsp;e мобилно приложение за телевизия на живо за iOS и Android устройства в OTT (Over-the-top) среда</span></div>', 1, '2017-08-30 09:13:51', '2017-09-11 10:33:21'),
(2, 'РАЗПРОСТРАНЕНИЕ:', 'razprostranenie', '<p class=\"section-p\" style=\"text-align: justify; margin-bottom: 20px; color: rgb(92, 92, 92); font-size: 19px; font-family: LatoRegular;\"><strong style=\"font-family: LatoBlack;\">Чрез кабелните и интернет оператори</strong>&nbsp;– Услугата&nbsp;<span style=\"font-family: LatoBlack; color: rgb(227, 49, 59);\">UnyQ</span><strong style=\"font-family: LatoBlack;\">TV</strong>&nbsp;се разпространява единствено чрез операторите на телевизионна и интернет услуга.</p><p class=\"section-p\" style=\"text-align: justify; margin-bottom: 20px; color: rgb(92, 92, 92); font-size: 19px; font-family: LatoRegular;\">Услугата не е възможна за директна онлайн продажба! За да гледате услугата, моля свържете се с вашия кабелен или интернет оператор!</p>', 1, '2017-09-05 01:10:55', '2017-09-28 09:04:53'),
(3, 'УСТРОЙСТВА:', 'ustroistva', '<p style=\"text-align: justify;\"><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px;\">Приложението&nbsp;</span><span style=\"font-family: LatoBlack; color: rgb(227, 49, 59); font-size: 19px;\">UnyQ</span><strong style=\"font-family: LatoBlack; color: rgb(92, 92, 92); font-size: 19px;\">TV</strong><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px;\">&nbsp;е предназначено за всички устройства с операционни системи: Android с версия 4.2 и по-висока, iOS с версия 7.0 и по-висока, включително Android IPTV STB</span><br></p>', 1, '2017-09-05 01:56:08', '2017-09-28 08:38:20'),
(5, 'ЗАПОЧНИ ОТ НАЧАЛО', 'unyqtv', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoBold; text-align: center; text-transform: uppercase;\">ВРЪЩАТЕ ДО НАЧАЛОТО <br> НА ТЕКУЩОТО ПРЕДАВАНЕ ИЛИ ДО <br> ТОЧНО ИЗБРАН МОМЕНТ</span><br></p>', 1, '2017-09-05 02:00:01', '2017-09-11 11:50:06'),
(6, 'ДО 7 ДНИ НАЗАД', 'do-7-dni-nazad', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoBold; text-align: center; text-transform: uppercase;\">ГЛЕДАТЕ НА ЗАПИС ИЗБРАНИ КАНАЛИ И <br> ПРЕДАВАНИЯ ИЛИ ТЪРСИТЕ ЗАПИС НА ВАШЕТО <br> ЛЮБИМО ПРЕДАВАНЕ ИЛИ ФИЛМ</span><br></p>', 1, '2017-09-05 02:01:44', '2017-09-11 11:50:37'),
(7, 'ТВ ПРОГРАМА', 'tv-programa', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoBold; text-align: center; text-transform: uppercase;\">ИЗБИРАТЕ ОТ ПРОГРАМНИЯ СПРАВОЧНИК <br> ПРЕДАВАНИЯТА В ТЕКУЩО ВРЕМЕ ИЛИ ОТ <br> ИЗМИНАЛ ПЕРИОД</span><br></p>', 1, '2017-09-05 02:02:13', '2017-09-11 11:51:04'),
(8, 'UnyQTV КОГАТО КЛАСИКАТА СРЕЩА МОДЕРНОТО', 'parallax', NULL, 1, '2017-09-05 02:02:33', '2017-09-26 08:09:40'),
(9, 'UnyQTV МОБИЛНА ТЕЛЕВИЗИЯ!', 'mobile-tv', '<h3 class=\"section-subtitle-2\" style=\"text-align: left; font-family: LatoBold; color: rgb(227, 49, 59); margin-top: 50px; margin-bottom: 10px; font-size: 20px; text-transform: uppercase;\">РАЗЧУПИ ТРАДИЦИЯТА!</h3><p class=\"section-p\" style=\"text-align: justify; margin-bottom: 20px; color: rgb(92, 92, 92); font-size: 19px; font-family: LatoRegular;\">Гледай любимите си канали и филми навсякъде и по всяко време от своя мобилен телефон или таблет! Вече не само на традиционния ТВ приемник, а защо не и на дивана, на балкона, в двора, на път, на плажа, в планината, в парка! Вече няма значение къде си – с&nbsp;<span style=\"font-family: LatoBlack; color: rgb(227, 49, 59);\">UnyQ</span><strong style=\"font-family: LatoBlack;\">TV</strong>получаваш достъп над ТВ съдържанието за което плащаш и гледаш когато искаш и където искаш!</p>', 1, '2017-09-05 02:04:25', '2017-09-11 11:21:13'),
(10, 'АКТИВАЦИЯ', 'activation', '<h3 class=\"text-center\" style=\"font-family: LatoRegular; color: rgb(51, 51, 51); margin-top: 20px; margin-bottom: 10px; font-size: 20px; background-color: rgb(242, 242, 242);\">Активирай услугата&nbsp;<strong style=\"font-family: LatoBlack;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong>&nbsp;като следваш инструкциите по-долу!</h3>', 1, '2017-09-05 02:05:05', '2017-09-11 11:03:58'),
(11, 'Активация 1', 'activation1', 'СВЪРЖИ СЕ С ВАШИЯ ОПЕРАТОР И ЗАЯВИ УСЛУГАТА <strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong>– САМОСТОЯТЕЛНО ИЛИ В ПАКЕТА, ПРЕДЛАГАН ОТ ВАШИЯ ОПЕРАТОР!', 1, '2017-09-05 02:05:46', '2017-09-11 11:06:43'),
(12, 'Активация 2', 'activation2', 'ИДИ В ОФИСА НА ВАШИЯ ОПЕРАТОР И СКЛЮЧИ ДОГОВОР ЗА УСЛУГАТА <strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong>.<br>', 1, '2017-09-05 02:07:15', '2017-09-11 11:18:18'),
(13, 'Активация 3', 'activation3', '<p>РЕГИСТРИРАЙ СЕ В ПОРТАЛА www.<strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong>.com (ИЗПОЛЗВАЙ АБОНАТНИЯ СИ НОМЕР ИЛИ КОДА ЗА АКТИВАЦИЯ, ПРЕДОСТАВЕНИ ОТ ВАШИЯ ОПЕРАТОР)<br></p>', 1, '2017-09-05 02:07:57', '2017-09-11 11:08:03'),
(14, 'Активация 4', 'activation4', 'ИЗТЕГЛИ ПРИЛОЖЕНИЕТО <strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong> ЗА МОБИЛНИ ТЕЛЕФОНИ И ТАБЛЕТИ ОТ<img src=\"uploads/blocks/xpgV1voLEfWaRyxOkwg9YpGlw2MVjhJXV4Pvw8TX.png\" style=\"width: 133.047px; height: 30.4356px;\"> <small>(ЗА ANDROID УСТРОЙСТВА)</small><br> И ОТ  <img src=\"uploads/blocks/sKmKNYvLBx7YrNCFvmR3XiVyTU09tahzFSMM60Ob.png\" style=\"width: 87.0133px; height: 35.5156px;\"> <small>(ЗА IOS УСТРОЙСТВА)</small>', 1, '2017-09-05 02:08:26', '2017-09-26 08:32:26'),
(15, 'Активация 5', 'activation5', '<p>ВХОД - ВЛЕЗ В ПРИЛОЖЕНИЕТО <strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV </strong>И ВЪВЕДИ ЕДНОКРАТНО ПОТРЕБИТЕЛСКОТО СИ ИМЕ И ПАРОЛА ОТ ПОРТАЛА <strong style=\"font-family: LatoBlack; text-transform: none;\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong>.COM<br></p>', 1, '2017-09-05 02:08:54', '2017-09-26 08:33:07'),
(16, 'Активация 6', 'activation6', 'ГЛЕДАЙ ЛЮБИМИТЕ СИ ПРЕДАВАНИЯ И КАНАЛИ НАВСЯКЪДЕ И ПО ВСЯКО ВРЕМЕ<br>', 1, '2017-09-05 02:09:28', '2017-09-11 11:18:47'),
(17, 'UnyQTV ПАКЕТИ И ПРОГРАМИ', 'ceni-i-paketi', '<p></p><div style=\"text-align: center;\"><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; background-color: rgb(242, 242, 242);\">С&nbsp;</span><strong style=\"font-family: LatoBlack; color: rgb(92, 92, 92); font-size: 19px; background-color: rgb(242, 242, 242);\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; background-color: rgb(242, 242, 242);\">&nbsp;получавате достъп до най-гледаните канали на вашия мобилен екран!&nbsp;</span></div><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; background-color: rgb(242, 242, 242);\"><div style=\"text-align: center;\">Light – 25+ канала, Smart – 35+ канала, UnyQ – 40+ канала + HBO + HBO Go.</div></span><p></p>', 1, '2017-09-05 02:09:56', '2017-09-28 09:57:02'),
(18, 'КАКВИ ФУНКЦИИ ИМА UnyQTV?', 'functions', '<h3 class=\"red-subtitle text-center\" style=\"font-family: LatoRegular; color: rgb(227, 49, 59); margin-top: 20px; margin-bottom: 10px; font-size: 20px;\">КАКВО ПРЕДЛАГА ПЛАТФОРМАТА ЗА ПОТРЕБИТЕЛЯ?</h3><br>', 1, '2017-09-05 02:10:45', '2017-09-11 11:20:31'),
(19, 'Live-TV', 'live-tv', '<p style=\"text-align: center; \"><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; background-color: rgb(242, 242, 242);\">Tелeвизия на живо - с&nbsp;</span><strong style=\"font-family: LatoBlack; color: rgb(92, 92, 92); font-size: 19px; background-color: rgb(242, 242, 242);\"><span style=\"color: rgb(227, 49, 59);\">UnyQ</span>TV</strong><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; background-color: rgb(242, 242, 242);\">&nbsp;получаваш достъп до над 60 от най- гледаните български и чужди канали!</span><br></p>', 1, '2017-09-05 02:10:59', '2017-09-11 06:36:22'),
(20, 'СТРИЙМИНГ КЪМ ТВ ПРИЕМНИК ЧРЕЗ CHROMECAST И AIRPLAY', 'stream', '<ul class=\"functions-ul text-center\">\r\n        <li><i class=\"fa fa-pause\" aria-hidden=\"true\"></i> Пауза</li>\r\n        <li><i class=\"fa fa-forward\" aria-hidden=\"true\"></i> Връщане назад и превъртане напред; </li>\r\n        <li><i class=\"fa fa-fast-backward\" aria-hidden=\"true\"></i> Връщане от началото на текущото предаване; </li>\r\n        <li><i class=\"fa fa-play-circle-o\" aria-hidden=\"true\"></i> Връщане назад до точен момент от гледаното предаване; </li>\r\n    </ul>', 1, '2017-09-05 02:11:49', '2017-09-05 06:47:30'),
(21, 'START-OVER/ TIME SHIFT', 'start-over', NULL, 1, '2017-09-05 02:12:22', '2017-09-05 02:12:22'),
(22, 'CATCH-UP', 'catch-up', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; text-align: center; background-color: rgb(242, 242, 242);\">Гледай избрани предавания и канали на запис до 7 дни назад!</span><br></p>', 1, '2017-09-05 02:34:23', '2017-09-05 02:34:23'),
(23, 'SEARCH', 'search', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; text-align: center; background-color: rgb(242, 242, 242);\">Търси любимо предаване и гледай на запис до 7 дни назад!</span><br></p>', 1, '2017-09-05 02:36:11', '2017-09-05 02:36:11'),
(24, 'EPG (ELECTRONIC PROGRAM GUIDE)', 'epg', '<p><span style=\"color: rgb(92, 92, 92); font-family: LatoRegular; font-size: 19px; text-align: center; background-color: rgb(242, 242, 242);\">Програмен справочник</span><br></p>', 1, '2017-09-05 02:36:21', '2017-09-05 02:36:21'),
(25, 'USER GUIDE', 'user-guide', NULL, 1, '2017-09-05 02:36:51', '2017-09-05 02:36:51'),
(26, 'ЗА НАС / ПРАВИЛА ЗА ПОЛЗВАНЕ НА ИНТЕРНЕТ САЙТА / ОБЩИ УСЛОВИЯ ЗА ПОЛЗВАНЕ НА УСЛУГАТА UnyQTV', 'footer-slogan', '<p>ЗА НАС / ПРАВИЛА ЗА ПОЛЗВАНЕ НА ИНТЕРНЕТ САЙТА / ОБЩИ УСЛОВИЯ ЗА ПОЛЗВАНЕ НА УСЛУГАТА UnyQTV<br></p>', 1, '2017-09-05 02:39:06', '2017-09-05 02:39:06'),
(27, 'UnyQTV - ПРИЛОЖЕНИЕ ЗА МОБИЛНА ТЕЛЕВИЗИЯ', 'slogan', '<p>UnyQTV - ПРИЛОЖЕНИЕ ЗА МОБИЛНА ТЕЛЕВИЗИЯ<br></p>', 1, '2017-09-05 05:43:43', '2017-09-11 06:25:04');

-- --------------------------------------------------------

--
-- Table structure for table `blocks_images`
--

CREATE TABLE `blocks_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `blocks_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `blocks_images`
--

INSERT INTO `blocks_images` (`id`, `blocks_id`, `filename`, `is_main`, `created_at`, `updated_at`) VALUES
(7, 6, '05.01.52-3708b45cba64fdf8f2a3df833daa10cd.png', 1, '2017-09-05 02:01:52', '2017-09-05 02:01:55'),
(9, 9, '05.04.37-d87f16ea5bb774a47b3a28720fab99ed.png', 0, '2017-09-05 02:04:37', '2017-09-11 10:46:58'),
(16, 19, '05.11.18-6e29e05043e4ee523a0da21bb5bf1bb9.png', 1, '2017-09-05 02:11:18', '2017-09-05 02:11:19'),
(17, 20, '05.11.59-a558fe2065c2482a0e71bd9d9ed05601.png', 1, '2017-09-05 02:11:59', '2017-09-05 02:12:00'),
(18, 21, '05.32.07-db408d2b482434dd395daaeceb0f1b20.png', 1, '2017-09-05 02:32:07', '2017-09-05 02:32:08'),
(19, 24, '05.36.31-40925c165c5a099e5e4b2c9e53ed0726.png', 1, '2017-09-05 02:36:31', '2017-09-05 02:36:32'),
(20, 25, '05.37.06-8f4e6b0a1c9c89b3ccdb40c69cba3adb.png', 1, '2017-09-05 02:37:06', '2017-09-05 06:56:55'),
(25, 7, '09.29.40-848315dd60ef6714fe644fd5aad8efcb.png', 1, '2017-09-05 06:29:40', '2017-09-05 06:29:42'),
(32, 5, 'ggopzcIzVcIGH67bgxyk5XJ7mT48Thy2IAdy6Oe2.png', 1, '2017-09-07 06:12:40', '2017-09-07 06:14:49'),
(33, 14, 'sKmKNYvLBx7YrNCFvmR3XiVyTU09tahzFSMM60Ob.png', 0, '2017-09-11 06:45:57', '2017-09-26 08:32:25'),
(37, 14, 'xpgV1voLEfWaRyxOkwg9YpGlw2MVjhJXV4Pvw8TX.png', 0, '2017-09-11 06:51:13', '2017-09-26 08:32:25'),
(39, 9, '2NuhYQ58FDAns1UkH9PhlK7Cn6ExcX6Vpo8DpICE.jpeg', 1, '2017-09-11 10:46:54', '2017-09-11 10:46:58'),
(47, 8, 'nzbe4m3ko92ieCtdxXULMiwu3X6VorwuIs0kJrIH.jpeg', 1, '2017-09-26 08:09:49', '2017-09-26 08:09:51'),
(49, 11, 'gyysSUTnIRCCeSqf8Q40HRqpjM93Iuh9Z91ZHqwq.jpeg', 1, '2017-09-26 08:23:03', '2017-09-26 08:23:04'),
(50, 12, 'WQJEjC2RaDqPSI5JX2GDqVYohl2gvHohMxpOXiam.jpeg', 1, '2017-09-26 08:31:40', '2017-09-26 08:31:43'),
(51, 13, 'b9IxALiLLmzjqS2eHbE6gbEx3IkI96ZRX81mvFWG.jpeg', 1, '2017-09-26 08:31:58', '2017-09-26 08:32:00'),
(52, 14, 'MdonaqphxzCYRActbtVQfXu11gaAMLOT6xFMAhNH.jpeg', 1, '2017-09-26 08:32:23', '2017-09-26 08:32:25'),
(53, 15, 'sAr7Z2WCKSZvmQTuh8glNLM3kGYM9eY98a4vuMnj.jpeg', 1, '2017-09-26 08:33:05', '2017-09-26 08:33:06'),
(54, 16, '2SExpd423xzZHrLSNkmtUKJcDQeaVeUmy9z3GJ1q.jpeg', 1, '2017-09-26 08:33:36', '2017-09-26 08:33:37');

-- --------------------------------------------------------

--
-- Table structure for table `channels`
--

CREATE TABLE `channels` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` int(11) DEFAULT NULL,
  `func1` int(11) DEFAULT NULL,
  `func2` int(11) DEFAULT NULL,
  `func3` int(11) DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `channels`
--

INSERT INTO `channels` (`id`, `title`, `content`, `metaTitle`, `metaDescription`, `category`, `func1`, `func2`, `func3`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Bnt1', 'Bnt1', 'Bnt1', 'Bnt1', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:22:23'),
(2, 'Btv', 'Btv', 'Btv', 'Btv', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(3, 'Nova', 'Nova', 'Nova', 'Nova', 1, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:32:46'),
(4, 'Kanal3', 'Kanal3', 'Kanal3', 'Kanal3', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(5, 'Diema', 'Diema', 'Diema', 'Diema', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(6, 'Bloomberg Tv Bg', 'Bloomberg Tv Bg', 'Bloomberg Tv Bg', 'Bloomberg Tv Bg', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:05:09'),
(7, 'Bnt 2', 'Bnt 2', 'Bnt 2', 'Bnt 2', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(8, 'Bnt World', 'Bnt World', 'Bnt World', 'Bnt World', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(9, 'Evrokom', 'Evrokom', 'Evrokom', 'Evrokom', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(10, 'Btv Comedy', 'Btv Comedy', 'Btv Comedy', 'Btv Comedy', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(11, 'Btv Cinema', 'Btv Cinema', 'Btv Cinema', 'Btv Cinema', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(12, 'Kino Nova', 'Kino Nova', 'Kino Nova', 'Kino Nova', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(13, 'Fox Tv', 'Fox Tv', 'Fox Tv', 'Fox Tv', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(14, 'Foxlife', 'Foxlife', 'Foxlife', 'Foxlife', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(15, 'Foxcrime', 'Foxcrime', 'Foxcrime', 'Foxcrime', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(16, 'Diema Family', 'Diema Family', 'Diema Family', 'Diema Family', 2, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(17, 'Filmboxplus', 'Filmboxplus', 'Filmboxplus', 'Filmboxplus', 2, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:44:50'),
(18, 'Btv Lady', 'Btv Lady', 'Btv Lady', 'Btv Lady', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(19, '24kitchen', '24kitchen', '24kitchen', '24kitchen', 3, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:39:02'),
(20, 'Tlc', 'Tlc', 'Tlc', 'Tlc', 3, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:45:05'),
(21, 'Ohota Ribalka', 'Ohota Ribalka', 'Ohota Ribalka', 'Ohota Ribalka', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(22, 'Wness', 'Wness', 'Wness', 'Wness', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(23, 'City Tv', 'City Tv', 'City Tv', 'City Tv', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(24, 'The Voice', 'The Voice', 'The Voice', 'The Voice', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(25, 'Planeta', 'Planeta', 'Planeta', 'Planeta', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(26, 'Fen TV', 'Fen TV', 'Fen TV', 'Fen TV', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:42:24'),
(27, 'Magic Tv', 'Magic Tv', 'Magic Tv', 'Magic Tv', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(28, 'Dm Sat', 'Dm Sat', 'Dm Sat', 'Dm Sat', 4, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(29, 'Btv Action', 'Btv Action', 'Btv Action', 'Btv Action', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:36:56'),
(30, 'Edgesport', 'Edgesport', 'Edgesport', 'Edgesport', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(31, 'Nova Sport', 'Nova Sport', 'Nova Sport', 'Nova Sport', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(32, 'Ring TV', 'Ring TV', 'Ring TV', 'Ring TV', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:37:15'),
(33, 'E1', 'E1', 'E1', 'E1', 3, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:47:19'),
(34, 'BNT HD', 'BNT HD', 'BNT HD', 'BNT HD', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:45:54'),
(35, 'Auto Motor Sport', 'Auto Motor Sport', 'Auto Motor Sport', 'Auto Motor Sport', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:41:26'),
(36, 'Fightbox', 'Fightbox', 'Fightbox', 'Fightbox', 3, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:50:49'),
(37, 'Ngc', 'Ngc', 'Ngc', 'Ngc', 6, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:54:09'),
(38, 'Discovery Channel', 'Discovery Channel', 'Discovery Channel', 'Discovery Channel', 6, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:48:31'),
(39, 'Da Vinci', 'Da Vinci', 'Da Vinci', 'Da Vinci', 6, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:48:11'),
(40, 'Travel Channel', 'Travel Channel', 'Travel Channel', 'Travel Channel', 3, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:53:05'),
(41, 'Cn', 'Cn', 'Cn', 'Cn', 7, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(42, 'Neokelodeon', 'Neokelodeon', 'Neokelodeon', 'Neokelodeon', 7, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(43, 'Ducktv', 'Ducktv', 'Ducktv', 'Ducktv', 7, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:51:11'),
(44, 'E-kids', 'E-kids', 'E-kids', 'E-kids', 7, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-26 07:41:44'),
(45, 'On Air', 'On Air', 'On Air', 'On Air', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(46, 'Tv Europa', 'Tv Europa', 'Tv Europa', 'Tv Europa', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(47, 'Bit', 'Bit', 'Bit', 'Bit', 1, 1, 1, 1, 1, '2017-09-12 05:34:30', '2017-09-12 05:34:30'),
(48, 'Hustler Tv', 'Hustler Tv', 'Hustler Tv', 'Hustler Tv', 9, 1, 1, 1, 0, '2017-09-12 05:34:30', '2017-09-26 07:51:30'),
(49, 'TV 1000', '<p>\r\n\r\n\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 10.0px \'Trebuchet MS\'; color: #000000; -webkit-text-stroke: #ffffff}\r\nspan.s1 {font-kerning: none}\r\n</style>\r\n\r\n\r\n</p><p class=\"p1\"><span class=\"s1\">TV 1000</span></p>', 'TV 1000', 'TV 1000', 2, 1, 1, 1, 1, '2017-09-26 07:33:49', '2017-09-26 07:33:58'),
(50, 'AXN', '<p>AXN<br></p>', 'AXN', 'AXN', 2, 1, 1, 1, 1, '2017-09-26 07:34:33', '2017-09-26 07:35:06'),
(51, 'AXN BLACK', '<p>AXN BLACK    AXN BLACK<br></p>', 'AXN BLACK', 'AXN BLACK', 2, 1, 1, 1, 1, '2017-09-26 07:34:47', '2017-09-26 07:35:09'),
(52, 'AXN WHITE', '<p>AXN WHITE<br></p>', 'AXN WHITE', 'AXN WHITE', 2, 1, 1, 1, 1, '2017-09-26 07:34:57', '2017-09-26 07:35:12'),
(53, 'HBO', '<p>HBO    <br></p>', 'HBO', 'HBO', 2, 1, 1, 1, 1, '2017-09-26 07:35:36', '2017-09-26 07:36:22'),
(54, 'HBO 2', '<p>HBO 2<br></p>', 'HBO 2', 'HBO 2', 2, 1, 1, 1, 1, '2017-09-26 07:36:05', '2017-09-26 07:36:24'),
(55, 'HBO 3', '<p>HBO 3<br></p>', 'HBO 3', 'HBO 3', 2, 1, 1, 1, 1, '2017-09-26 07:36:13', '2017-09-26 07:36:27'),
(56, 'Fashion TV', '<p>Fashion TV<br></p>', 'Fashion TV', 'Fashion TV', 3, 1, 1, 1, 1, '2017-09-26 07:37:52', '2017-09-26 07:37:58'),
(58, 'Viasat Explorer', '<p>\r\n\r\n\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 10.0px \'Trebuchet MS\'; color: #000000; -webkit-text-stroke: #ffffff}\r\nspan.s1 {font-kerning: none}\r\n</style>\r\n\r\n\r\n</p><p class=\"p1\"><span class=\"s1\">Viasat Explorer</span></p>', 'Viasat Explorer', 'Viasat Explorer', 3, 1, 1, 1, 1, '2017-09-26 07:39:39', '2017-09-26 07:40:10'),
(59, 'Viasat History', '<p>\r\n\r\n\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 10.0px \'Trebuchet MS\'; color: #000000; -webkit-text-stroke: #ffffff}\r\nspan.s1 {font-kerning: none}\r\n</style>\r\n\r\n\r\n</p><p class=\"p1\"><span class=\"s1\">Viasat History</span></p>', 'Viasat History', 'Viasat History', 3, 1, 1, 1, 1, '2017-09-26 07:39:47', '2017-09-26 07:40:16'),
(60, 'Viasat Nature', '<p>\r\n\r\n\r\n\r\n\r\n<style type=\"text/css\">\r\np.p1 {margin: 0.0px 0.0px 0.0px 0.0px; font: 10.0px \'Trebuchet MS\'; color: #000000; -webkit-text-stroke: #ffffff}\r\nspan.s1 {font-kerning: none}\r\n</style>\r\n\r\n\r\n</p><p class=\"p1\"><span class=\"s1\">Viasat Nature</span></p>', 'Viasat Nature', 'Viasat Nature', 3, 1, 1, 1, 1, '2017-09-26 07:39:55', '2017-09-26 07:40:20'),
(61, 'Fine Living', 'Fine Living', 'Fine Living', 'Fine Living', 3, 1, 1, 1, 1, '2017-09-26 07:40:35', '2017-09-26 07:43:35'),
(62, 'Food Network', 'Food Network', 'Food Network', 'Food Network', 3, 1, 1, 1, 1, '2017-09-26 07:40:53', '2017-09-26 07:41:05'),
(63, 'Disney', 'Disney', 'Disney', 'Disney', 7, 1, 1, 1, 1, '2017-09-26 07:42:05', '2017-09-26 07:43:43'),
(64, 'Disney Junior', '<p>Disney Junior<br></p>', 'Disney Junior', 'Disney Junior', 7, 1, 1, 1, 1, '2017-09-26 07:42:14', '2017-09-26 07:43:47'),
(65, 'Hit Mix', '<p>Hit Mix<br></p>', 'Hit Mix', 'Hit Mix', 4, 1, 1, 1, 1, '2017-09-26 07:42:45', '2017-09-26 07:43:56'),
(66, 'RODINA TV', '<p>RODINA TV<br></p>', 'RODINA TV', 'RODINA TV', 4, 1, 1, 1, 1, '2017-09-26 07:43:03', '2017-09-26 07:44:01'),
(67, 'Box TV', '<p>Box TV<br></p>', 'Box TV', 'Box TV', 4, 1, 1, 1, 1, '2017-09-26 07:43:20', '2017-09-26 07:44:06'),
(68, 'TV 1', '<p>TV 1<br></p>', 'TV 1', 'TV 1', 3, 1, 1, 1, 1, '2017-09-26 07:46:34', '2017-09-26 07:46:45');

-- --------------------------------------------------------

--
-- Table structure for table `channels_images`
--

CREATE TABLE `channels_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `channels_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `channels_images`
--

INSERT INTO `channels_images` (`id`, `channels_id`, `filename`, `is_main`, `created_at`, `updated_at`) VALUES
(2, 1, 'nHveutSkrThOnCf3lwqAOZcwaNMykLQEQaGLUt51.png', 1, '2017-09-12 09:03:22', '2017-09-12 09:03:23'),
(3, 2, 'MCauGqJOcvPlui86ViKEBQ9zudbDZwwUFyR0LdRl.png', 1, '2017-09-12 09:03:34', '2017-09-12 09:03:37'),
(4, 3, 'v81dGbYU7SAzOlErwff24oWWQSzzHwVez7g2OMsx.png', 1, '2017-09-12 09:03:46', '2017-09-12 09:03:47'),
(5, 4, 'pSRR3FYmBNVIgnXGWfboPMQbOSKQN6yffJRGpqTx.png', 1, '2017-09-12 09:03:52', '2017-09-12 09:03:54'),
(6, 5, 'yFKU4LVW0oNx1edlq787FxRLD1jQ93IEnZc1TrQ7.png', 1, '2017-09-12 09:04:02', '2017-09-12 09:04:03'),
(7, 6, 'Trnkfkz9WFGIqebBN1ieNWxldkES0gDMnXvIVzl6.png', 1, '2017-09-12 09:04:08', '2017-09-12 09:04:09'),
(8, 7, 'vcopZ2tGf01kHndjUBohloWx1tWI1e7ZtM8WbF9U.png', 1, '2017-09-12 09:04:17', '2017-09-12 09:04:19'),
(9, 8, 'vJ8Q7trWiEuijnX27rrZBGR5vkH6UMu3lnInZnH3.png', 1, '2017-09-12 09:04:30', '2017-09-12 09:04:33'),
(20, 9, '3k06rtdkwAynidBNYBmyY738IF2pHxbwI3TzSNJK.png', 1, '2017-09-26 07:20:56', '2017-09-26 07:21:22'),
(21, 10, 'xO3ZlNMV96los7h23tqS8i1BKvcphIfnxRNMgXrv.png', 1, '2017-09-26 07:21:47', '2017-09-26 07:21:49'),
(22, 11, 'Sv4TCterlxZGDILBen02RN4ldhGUfSMf7PapkJcc.png', 1, '2017-09-26 07:22:17', '2017-09-26 07:22:18'),
(23, 12, 'GdBqFToncJgPxvRpX2IOJTlaeOUwL5R3PU8r444K.png', 1, '2017-09-26 07:25:28', '2017-09-26 07:25:29'),
(24, 13, 'QuKQBM02Aha5DAPgXPwzD3665pzRf6Xgv7kt32Jk.png', 1, '2017-09-26 07:25:45', '2017-09-26 07:25:47'),
(25, 14, '6TkfQQELH5ek32a81WSglXuWFGkspCRVDy1FJS6G.png', 1, '2017-09-26 07:26:12', '2017-09-26 07:26:14'),
(26, 15, '5fDTfeU0b0rNzODu6ieuAre8DXxNP7Yq6xZieHoa.png', 1, '2017-09-26 07:26:29', '2017-09-26 07:50:36'),
(27, 16, 'c9EUjaqetYq6QeZPEio0V2GS76AsbaQ21wHytrku.png', 1, '2017-09-26 07:26:40', '2017-09-26 07:26:41'),
(28, 17, 'L6A51aBJnSNY0lI60BDv71ALSFfGeq6KBd8hFjL7.png', 1, '2017-09-26 07:27:04', '2017-09-26 07:27:08'),
(29, 18, 'n51zwMe3jG6rO8mYTXKXcffynvLD7hZww7AR75Y3.png', 1, '2017-09-26 07:27:23', '2017-09-26 07:27:24'),
(30, 19, 'T91ckrwMYNR5ycGqDZCkFoCXfbvtWbDH14icYN8n.png', 1, '2017-09-26 07:27:34', '2017-09-26 07:27:36'),
(31, 20, '5BJJGCxcH2niHpDXv4rIOspUh7O4dvKPJiqbi4N8.png', 1, '2017-09-26 07:27:45', '2017-09-26 07:27:47'),
(32, 21, '6TmYgxwf919Vycy1UZHEGVmHnMeHgpJsCYbz8dQg.png', 1, '2017-09-26 07:28:11', '2017-09-26 07:28:12'),
(33, 22, 'nHuo3d8b6124LRA6xvRnLalEz8uTiEZQN5nxss6w.png', 1, '2017-09-26 07:28:41', '2017-09-26 07:28:42'),
(34, 23, '3GzAq9YzOXvCNQ1I4HnaLjqDujsL5d6ew3uGt7Zp.png', 1, '2017-09-26 07:29:42', '2017-09-26 07:29:44'),
(35, 24, 'zzygP93Wv5ZtWqVtY1m2nDnnGL8bA99QFoPnFayH.png', 1, '2017-09-26 07:29:57', '2017-09-26 07:29:59'),
(36, 25, 'AMfmZBDIaQ879NIp3AmgKab9LQoA5vDGCLVy5efP.png', 1, '2017-09-26 07:30:10', '2017-09-26 07:30:20'),
(37, 26, 'P4UrascP2svqD9i3TYHwVphKQLbO6uVqnTAytvIK.png', 1, '2017-09-26 07:30:46', '2017-09-26 07:30:47'),
(38, 27, 'rFoqoqMxKq66se78HBDLxOrUx5F4UDYPBt6jccRA.png', 1, '2017-09-26 07:30:58', '2017-09-26 07:30:59'),
(39, 28, 'O2RCO1Tbmvcn6zNcAs1zScJ64CY0aUbbOSqLK1GF.png', 1, '2017-09-26 07:31:26', '2017-09-26 07:31:27'),
(40, 29, 'OwYHZW4bpqz0KUlPmDbAPEoD7VcjdDqk6bNBFB3p.png', 1, '2017-09-26 07:32:06', '2017-09-26 07:32:08'),
(41, 30, 'G95Xq2YZxSKYz259U1tCVWCo7w7zGtdedsiaz9Dp.png', 1, '2017-09-26 07:32:41', '2017-09-26 07:32:42'),
(42, 31, 'X39ZIWlrCVLJegTb8R2WQKjTUKygLznHpWpmB65N.png', 1, '2017-09-26 07:33:00', '2017-09-26 07:33:01'),
(43, 32, 'GZH1XC3hf2AaNUk5wSFnujSd9LYnC4wgcLqIdo6S.png', 1, '2017-09-26 07:33:24', '2017-09-26 07:33:25'),
(44, 33, 'UKVEcGKNByfmBrZdDHNf5Uf1p63DPl8mtr3crijx.png', 1, '2017-09-26 07:33:41', '2017-09-26 07:33:42'),
(45, 34, 'OhK7R0VcG9SrX5tTNyMBUuQpYqnaYuanfG1qR3w1.png', 1, '2017-09-26 07:34:10', '2017-09-26 07:34:12'),
(46, 35, 'or26EZznaVw3dKv4jHyZB8xMLuWGYIVtqJVjqd0m.png', 1, '2017-09-26 07:34:48', '2017-09-26 07:34:49'),
(47, 36, 'jaXMMZmBpbrMgJib6pg5ygy1ioE1EGhrDytmKChk.png', 1, '2017-09-26 07:35:23', '2017-09-26 07:35:24'),
(48, 37, 'QchKvGR8NuF4d9Z9GUrjDMGCFB2bhZ4D8OFtAftW.png', 1, '2017-09-26 07:35:41', '2017-09-26 07:35:44'),
(49, 38, 'qRC119JoeMCq8Zo7yEcETajeQJ115AldJQJZIURr.png', 1, '2017-09-26 07:36:01', '2017-09-26 07:36:02'),
(50, 39, '81N8iJCgkOF5uGo8zLyBHOanbRPeXKImAoRTNkiS.png', 1, '2017-09-26 07:36:16', '2017-09-26 07:36:17'),
(52, 40, 'JZTB3QsC4vg9kAuRPJKOYhLBh3tWnudJHTvwJByz.png', 1, '2017-09-26 07:37:41', '2017-09-26 07:38:01'),
(53, 41, 'A2SCWLpDfeQWGksmgOXOKkiJOqjBqTghi0iS8SmY.png', 1, '2017-09-26 07:38:28', '2017-09-26 07:38:29'),
(54, 42, 'CPllIFTCfEebn0iAFx9GroikNJQhRKOxX9x1tg65.png', 1, '2017-09-26 07:38:43', '2017-09-26 07:38:45'),
(55, 43, 'woLoQ1ARVeZ8zZDE89TgiMJG9pbDj4sy891UHoz3.png', 1, '2017-09-26 07:39:43', '2017-09-26 07:39:44'),
(56, 44, 'wWVVJqP9M73oyEQ4nhHJObWlVOKGxfCi8Zo5k0mj.png', 1, '2017-09-26 07:40:04', '2017-09-26 07:40:06'),
(57, 45, 'oagp18Pzu470l84jye0dSevcME4iWZLFbAkBHQlU.png', 1, '2017-09-26 07:40:17', '2017-09-26 07:40:19'),
(58, 46, 'y8Uck2qYrTnMCRKcZm0I3fTfS3IMqFXiRVIKLETF.png', 1, '2017-09-26 07:40:34', '2017-09-26 07:40:36'),
(59, 47, 'x8qWJWCBAjpyXTuPr8aQAAoKQGtmoKqWJ0AU5ymL.png', 1, '2017-09-26 07:41:02', '2017-09-26 07:41:04'),
(60, 48, 'ar5RicPAxBFl6QVm9WPDQySWaV9DrQ17LU3JKS3p.png', 1, '2017-09-26 07:41:26', '2017-09-26 07:41:27'),
(61, 49, 'smK8WwTrXYAQVh3JRbD737iN1OwawljTmkRKsXr0.png', 1, '2017-09-26 07:41:43', '2017-09-26 07:41:44'),
(62, 50, '3HIqEiexGNdPgXdBxI55y4JKsAkBwYoS3enFJ1oj.png', 1, '2017-09-26 07:42:01', '2017-09-26 07:42:02'),
(63, 51, '1NjYTXiVodNsHzzDyWogBV8hndxQAP7C9hZP3dEM.png', 1, '2017-09-26 07:42:21', '2017-09-26 07:42:22'),
(64, 52, 'l1ZrmJeZu8qcHIxxxialP5ULNtQVyS73O6r7UaIV.png', 1, '2017-09-26 07:42:38', '2017-09-26 07:42:39'),
(65, 53, 'M9IrBwLFME7zkn52LOmhXBgn085CZvStlzeFuDAC.png', 1, '2017-09-26 07:42:52', '2017-09-26 07:42:53'),
(66, 54, '1OzU0JyjyqEcyAeJBykuIe6DZ8pnus4PnoADcqqf.png', 1, '2017-09-26 07:43:05', '2017-09-26 07:43:07'),
(67, 55, 'FMkijzJrbckYulRT2lK9au5DKDLLXSWlibxuQ1x4.png', 1, '2017-09-26 07:43:18', '2017-09-26 07:43:19'),
(68, 56, 'lGo49TaIy9Gpkyws3akcQmYraroENJhm6glT6cfL.png', 1, '2017-09-26 07:43:36', '2017-09-26 07:43:37'),
(70, 58, 'oyvuKzNCG3DIzq8MPECEBDKn2MNG5q8dhDfujR2s.png', 1, '2017-09-26 07:44:25', '2017-09-26 07:44:27'),
(71, 59, 'wJdbmzy5A0114JFyXT3XFtCzwpIXJ0XNRnxy9Rvi.png', 1, '2017-09-26 07:44:37', '2017-09-26 07:44:41'),
(72, 60, 'lEjHqUV3KFeSFEK2Od0YHrm2LuRexuoZY49B5Uv5.png', 1, '2017-09-26 07:44:50', '2017-09-26 07:44:51'),
(73, 61, 'rT1wPLjPqLvfxfbJXUkNLJ2c7oH9svuVQWKnjUk9.png', 1, '2017-09-26 07:45:07', '2017-09-26 07:45:08'),
(74, 62, 'PPCyOVniLM24RUpPxCMkk5KWTZdSzhuDbhVoP0XK.png', 1, '2017-09-26 07:45:19', '2017-09-26 07:45:20'),
(75, 63, '2CDuJP4HdUkqE3Z4wDF5Pg6rQnadiPXcFW0Bmu1G.png', 1, '2017-09-26 07:48:13', '2017-09-26 07:48:14'),
(76, 64, 'tVjSIBxaNT0gZO9cpoVSGrXPRVDxK8tNo3XnAQ7T.png', 1, '2017-09-26 07:48:29', '2017-09-26 07:48:30'),
(77, 65, 'BUfnANf6b5cWN53zygCqFlJR5h6h2IttaDv9gjm1.png', 1, '2017-09-26 07:48:45', '2017-09-26 07:48:46'),
(78, 66, 'jA1UhR6YRL8Bmmdm5vK53SThbFATPIR5Cmpl8FwP.png', 1, '2017-09-26 07:49:00', '2017-09-26 07:49:02'),
(79, 67, 'VI5TR8SvHBuBoc32FOs7AIOZ3R3urea1BqQzWo6t.png', 1, '2017-09-26 07:49:13', '2017-09-26 07:49:14'),
(80, 68, 'ZAbahBFM4lIRvoF96YL8ocfUOH7c1W63GWjr2GV5.png', 1, '2017-09-26 07:49:27', '2017-09-26 07:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `show_gallery` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `title`, `slug`, `content`, `metaTitle`, `metaDescription`, `show_gallery`, `active`, `created_at`, `updated_at`) VALUES
(1, 'UnyQTV', 'unyqtv', '<p><!--[if gte mso 9]><xml>\r\n <o:OfficeDocumentSettings>\r\n  <o:AllowPNG></o:AllowPNG>\r\n </o:OfficeDocumentSettings>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:WordDocument>\r\n  <w:View>Normal</w:View>\r\n  <w:Zoom>0</w:Zoom>\r\n  <w:TrackMoves></w:TrackMoves>\r\n  <w:TrackFormatting></w:TrackFormatting>\r\n  <w:HyphenationZone>21</w:HyphenationZone>\r\n  <w:PunctuationKerning></w:PunctuationKerning>\r\n  <w:ValidateAgainstSchemas></w:ValidateAgainstSchemas>\r\n  <w:SaveIfXMLInvalid>false</w:SaveIfXMLInvalid>\r\n  <w:IgnoreMixedContent>false</w:IgnoreMixedContent>\r\n  <w:AlwaysShowPlaceholderText>false</w:AlwaysShowPlaceholderText>\r\n  <w:DoNotPromoteQF></w:DoNotPromoteQF>\r\n  <w:LidThemeOther>BG</w:LidThemeOther>\r\n  <w:LidThemeAsian>X-NONE</w:LidThemeAsian>\r\n  <w:LidThemeComplexScript>X-NONE</w:LidThemeComplexScript>\r\n  <w:Compatibility>\r\n   <w:BreakWrappedTables></w:BreakWrappedTables>\r\n   <w:SnapToGridInCell></w:SnapToGridInCell>\r\n   <w:WrapTextWithPunct></w:WrapTextWithPunct>\r\n   <w:UseAsianBreakRules></w:UseAsianBreakRules>\r\n   <w:DontGrowAutofit></w:DontGrowAutofit>\r\n   <w:SplitPgBreakAndParaMark></w:SplitPgBreakAndParaMark>\r\n   <w:EnableOpenTypeKerning></w:EnableOpenTypeKerning>\r\n   <w:DontFlipMirrorIndents></w:DontFlipMirrorIndents>\r\n   <w:OverrideTableStyleHps></w:OverrideTableStyleHps>\r\n  </w:Compatibility>\r\n  <m:mathPr>\r\n   <m:mathFont m:val=\"Cambria Math\"></m:mathFont>\r\n   <m:brkBin m:val=\"before\"></m:brkBin>\r\n   <m:brkBinSub m:val=\"--\"></m:brkBinSub>\r\n   <m:smallFrac m:val=\"off\"></m:smallFrac>\r\n   <m:dispDef></m:dispDef>\r\n   <m:lMargin m:val=\"0\"></m:lMargin>\r\n   <m:rMargin m:val=\"0\"></m:rMargin>\r\n   <m:defJc m:val=\"centerGroup\"></m:defJc>\r\n   <m:wrapIndent m:val=\"1440\"></m:wrapIndent>\r\n   <m:intLim m:val=\"subSup\"></m:intLim>\r\n   <m:naryLim m:val=\"undOvr\"></m:naryLim>\r\n  </m:mathPr></w:WordDocument>\r\n</xml><![endif]--><!--[if gte mso 9]><xml>\r\n <w:LatentStyles DefLockedState=\"false\" DefUnhideWhenUsed=\"false\"\r\n  DefSemiHidden=\"false\" DefQFormat=\"false\" DefPriority=\"99\"\r\n  LatentStyleCount=\"371\">\r\n  <w:LsdException Locked=\"false\" Priority=\"0\" QFormat=\"true\" Name=\"Normal\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" QFormat=\"true\" Name=\"heading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"9\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"heading 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"toc 9\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Indent\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"header\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footer\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"index heading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"35\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"caption\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of figures\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope address\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"envelope return\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"footnote reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"line number\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"page number\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"endnote text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"table of authorities\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"macro\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"toa heading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Bullet 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Number 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"10\" QFormat=\"true\" Name=\"Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Closing\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Signature\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Default Paragraph Font\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"List Continue 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Message Header\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"11\" QFormat=\"true\" Name=\"Subtitle\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Salutation\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Date\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text First Indent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Note Heading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Body Text Indent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Block Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Hyperlink\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"FollowedHyperlink\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"22\" QFormat=\"true\" Name=\"Strong\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"20\" QFormat=\"true\" Name=\"Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Document Map\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Plain Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"E-mail Signature\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Top of Form\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Bottom of Form\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal (Web)\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Acronym\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Address\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Cite\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Code\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Definition\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Keyboard\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Preformatted\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Sample\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Typewriter\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"HTML Variable\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Normal Table\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"annotation subject\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"No List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Outline List 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Simple 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Classic 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Colorful 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Columns 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Grid 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 7\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table List 8\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table 3D effects 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Contemporary\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Elegant\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Professional\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Subtle 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Web 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Balloon Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" Name=\"Table Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" UnhideWhenUsed=\"true\"\r\n   Name=\"Table Theme\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Placeholder Text\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"1\" QFormat=\"true\" Name=\"No Spacing\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" SemiHidden=\"true\" Name=\"Revision\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"34\" QFormat=\"true\"\r\n   Name=\"List Paragraph\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"29\" QFormat=\"true\" Name=\"Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"30\" QFormat=\"true\"\r\n   Name=\"Intense Quote\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"60\" Name=\"Light Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"61\" Name=\"Light List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"62\" Name=\"Light Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"63\" Name=\"Medium Shading 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"64\" Name=\"Medium Shading 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"65\" Name=\"Medium List 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"66\" Name=\"Medium List 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"67\" Name=\"Medium Grid 1 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"68\" Name=\"Medium Grid 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"69\" Name=\"Medium Grid 3 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"70\" Name=\"Dark List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"71\" Name=\"Colorful Shading Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"72\" Name=\"Colorful List Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"73\" Name=\"Colorful Grid Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"19\" QFormat=\"true\"\r\n   Name=\"Subtle Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"21\" QFormat=\"true\"\r\n   Name=\"Intense Emphasis\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"31\" QFormat=\"true\"\r\n   Name=\"Subtle Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"32\" QFormat=\"true\"\r\n   Name=\"Intense Reference\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"33\" QFormat=\"true\" Name=\"Book Title\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"37\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" Name=\"Bibliography\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"39\" SemiHidden=\"true\"\r\n   UnhideWhenUsed=\"true\" QFormat=\"true\" Name=\"TOC Heading\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"41\" Name=\"Plain Table 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"42\" Name=\"Plain Table 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"43\" Name=\"Plain Table 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"44\" Name=\"Plain Table 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"45\" Name=\"Plain Table 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"40\" Name=\"Grid Table Light\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"Grid Table 1 Light\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"Grid Table 6 Colorful\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"Grid Table 7 Colorful\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"Grid Table 1 Light Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"Grid Table 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"Grid Table 3 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"Grid Table 4 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"Grid Table 5 Dark Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"Grid Table 6 Colorful Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"Grid Table 7 Colorful Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\" Name=\"List Table 1 Light\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\" Name=\"List Table 6 Colorful\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\" Name=\"List Table 7 Colorful\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 1\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 2\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 3\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 4\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 5\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"46\"\r\n   Name=\"List Table 1 Light Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"47\" Name=\"List Table 2 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"48\" Name=\"List Table 3 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"49\" Name=\"List Table 4 Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"50\" Name=\"List Table 5 Dark Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"51\"\r\n   Name=\"List Table 6 Colorful Accent 6\"></w:LsdException>\r\n  <w:LsdException Locked=\"false\" Priority=\"52\"\r\n   Name=\"List Table 7 Colorful Accent 6\"></w:LsdException>\r\n </w:LatentStyles>\r\n</xml><![endif]--><!--[if gte mso 10]>\r\n<style>\r\n /* Style Definitions */\r\n table.MsoNormalTable\r\n	{mso-style-name:\"Table Normal\";\r\n	mso-tstyle-rowband-size:0;\r\n	mso-tstyle-colband-size:0;\r\n	mso-style-noshow:yes;\r\n	mso-style-priority:99;\r\n	mso-style-parent:\"\";\r\n	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;\r\n	mso-para-margin-top:0cm;\r\n	mso-para-margin-right:0cm;\r\n	mso-para-margin-bottom:8.0pt;\r\n	mso-para-margin-left:0cm;\r\n	line-height:107%;\r\n	mso-pagination:widow-orphan;\r\n	font-size:11.0pt;\r\n	font-family:\"Calibri\",sans-serif;\r\n	mso-ascii-font-family:Calibri;\r\n	mso-ascii-theme-font:minor-latin;\r\n	mso-hansi-font-family:Calibri;\r\n	mso-hansi-theme-font:minor-latin;\r\n	mso-bidi-font-family:\"Times New Roman\";\r\n	mso-bidi-theme-font:minor-bidi;\r\n	mso-fareast-language:EN-US;}\r\n</style>\r\n<![endif]-->\r\n\r\n<br></p><p class=\"MsoNormal\" align=\"justify\"><i>Unicoms\r\nServices пуска end-to-end, хоствана OTT услуга за оператори\r\nза платена телевизия, OTT стрийминг платформа с технологии\r\nот Conax GO Live и Appear\r\nTV поддържаща Android и iOS устройства</i><i><span style=\"mso-ansi-language:EN-US\" lang=\"EN-US\">.</span></i></p><p class=\"MsoNormal\" align=\"justify\">За това информираха популярните новинарски сайтове в областта\r\nна телевизията и телекомуникациите:</p><p class=\"MsoNormal\"><a href=\"http://www.broadbandtvnews.com/2017/03/28/white-label-ott-for-the-balkans/#more-144422\">http://www.broadbandtvnews.com/2017/03/28/white-label-ott-for-the-balkans/#more-144422</a></p><p class=\"MsoNormal\"><a href=\"http://www.digitaltveurope.net/677772/tv-connect-2017-bulgarias-unicoms-taps-conax-and-appear-tv-for-white-label-ott/\">http://www.digitaltveurope.net/677772/tv-connect-2017-bulgarias-unicoms-taps-conax-and-appear-tv-for-white-label-ott/</a><a href=\"http://www.digitaltveurope.net/677772/tv-connect-2017-bulgarias-unicoms-taps-conax-and-appear-tv-for-white-label-ott/\"><span style=\"mso-ansi-language:EN-US\" lang=\"EN-US\"></span></a></p><p class=\"MsoNormal\">\r\n\r\n</p><p class=\"MsoNormal\"><a href=\"https://tmt.knect365.com/tv-connect/speakers/nastya-popova#\">https://tmt.knect365.com/tv-connect/speakers/nastya-popova#</a></p><p class=\"MsoNormal\" align=\"justify\">Като клиент на Conax от 2010, новата\r\nплатформа помага на Unicoms services в миграцията\r\nкъм OTT арената, поддържана от усъвършенстваната, OTT стрийм\r\nплатформа <a href=\"http://www.conax.com/products-solutions/multiscreen-2/conax-go-live/?utm_source=Test&utm_campaign=72b3f3c336-NAB_2016&utm_medium=email&utm_term=0_6c076e7810-72b3f3c336-\">Conax\r\nGO Live</a>,<i> </i>предварително интегрирана с платформата на Appear\r\nTV за премиум съдържание. Unicoms Services вече хоства\r\nнад 50 оператора от Балканите с Conax Contego условен\r\nдостъп и е първото професионално „white label“ OTT решение,\r\nдостъпно в България.</p><p class=\"MsoNormal\" align=\"justify\"><font color=\"#FF0000\"><span style=\"background-color: rgb(255, 0, 0);\"><span style=\"background-color: rgb(255, 255, 255);\"></span></span></font> <font color=\"#FF0000\">UnyQ</font>TV<font color=\"#FF0000\"><span style=\"background-color: rgb(255, 0, 0);\"><span style=\"background-color: rgb(255, 255, 255);\"></span></span> </font> платформата предоставя два бизнес модела:</p><p class=\"MsoNormal\" align=\"justify\"> -  „white label“ техническо\r\nрешение, което позволява просто ребрандиране и предоставяне на платените\r\nот оператора канали;</p><p class=\"MsoNormal\" align=\"justify\">-  <font color=\"#FF0000\">UnyQ</font>TV<font color=\"#FF0000\"><span style=\"background-color: rgb(255, 0, 0);\"><span style=\"background-color: rgb(255, 255, 255);\"></span></span></font> модел на “Споделен приход”. Последният\r\nпредоставя завършена екосистема за оператори на платена телевизия за\r\nстартиране без риск на OTT услуги, драстично намаляващи\r\nразходите на операторите и първоначалните инвестиции и бързо пускане\r\nна Live TV канали за смартфони и таблети, включително поддръжка\r\nна catch-up TV. Conax GO Live OTT платформата предоставя лесно\r\nза ползване приложение, което работи изключително бързо. <b>Unicoms\r\nServices хостваното решение предлага на абонатите на малките и\r\nсредни DVB-C, DTH и IPTV оператори, достъп до български\r\nи чужди телевизионни канали.</b></p><p class=\"MsoNormal\" align=\"justify\">OTT не само предоставя най-гъвкавият начин за пренос на\r\nсъдържание до всяко устройство в рамките на различни бизнес\r\nмодели, но и предлага изключителен потенциал за\r\nпечалба. Партньорството между Conax и Appear TV предоставя\r\nзавършено, високо функционално и с възможности за допълнения решение,\r\nкоето прави OTT услугата лесна за къстъмизиране и в същото време\r\nвсеки компонент е предварително тестван и интегриран, което\r\nпревръща OTT услугата в лесна и бърза опция за пазара. Вместо\r\nда се борят за овладяване на технологии, клиентите са свободни да се\r\nконцентрират в предоставянето на високо качествена, високо\r\nиновативна нова услуга, базирана на  <font color=\"#FF0000\">UnyQ</font>TV<font color=\"#FF0000\"><span style=\"background-color: rgb(255, 0, 0);\"><span style=\"background-color: rgb(255, 255, 255);\"></span></span></font> Cloud\r\nOTT платформата<span style=\"mso-ansi-language:EN-US\" lang=\"EN-US\">.</span></p><p>\r\n\r\n<br></p><p align=\"justify\">\r\n\r\n</p><p></p>', 'НОВИНА 1', 'НОВИНА 1', 1, 1, '2017-09-04 09:23:49', '2017-09-25 05:55:49');

-- --------------------------------------------------------

--
-- Table structure for table `news_images`
--

CREATE TABLE `news_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `news_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news_images`
--

INSERT INTO `news_images` (`id`, `news_id`, `filename`, `is_main`, `created_at`, `updated_at`) VALUES
(7, 1, 'MyZFhPxmdzVHuNllIJ5IaiH5930uPL6fHNxkiWYi.jpeg', 1, '2017-09-25 05:55:43', '2017-09-25 05:55:45');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `menu` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'main',
  `show_gallery` int(11) NOT NULL DEFAULT '0',
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `slug`, `content`, `metaTitle`, `metaDescription`, `menu`, `show_gallery`, `active`, `created_at`, `updated_at`) VALUES
(1, 'За нас', 'za-nas', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris vestibulum leo mi, nec maximus leo lacinia eu. Etiam laoreet dictum mi in placerat. Nunc vel magna hendrerit, faucibus tortor convallis, pharetra nulla. Duis fermentum mauris in aliquet ullamcorper. Integer sollicitudin sem id purus ultrices congue. Nunc sagittis tempus quam. Vestibulum vel eros finibus, tempus metus et, imperdiet arcu. Phasellus commodo ac nisl ut consequat. Etiam commodo venenatis leo at porttitor. Morbi ipsum est, ultrices sit amet sem id, vestibulum finibus turpis. Suspendisse pharetra lacus et erat sodales volutpat vitae nec metus. Praesent et fermentum nunc, sit amet eleifend mauris. Vestibulum interdum nisi et aliquet vehicula. In ultricies vestibulum sapien, eget porttitor augue interdum et. In molestie mollis lectus, vel semper magna luctus a. In imperdiet libero sit amet mattis cursus.\r\n\r\nCras a tellus sed tellus tempor luctus. Quisque nec augue vitae neque porta maximus. Pellentesque vel mi id nibh interdum placerat vel ac libero. Etiam pharetra venenatis lobortis. Aenean congue aliquam consectetur. Fusce vitae quam purus. Etiam blandit commodo tortor vel maximus. Vestibulum suscipit diam eu ligula bibendum finibus. Pellentesque in ligula ut urna tincidunt ultricies. Mauris viverra, dui id venenatis dictum, purus eros sollicitudin risus, sed dapibus nibh orci vitae libero. Vivamus semper varius lorem eget aliquam.\r\n\r\nAenean egestas eget odio quis egestas. Sed elementum augue nec efficitur efficitur. Integer ut lectus maximus, aliquam augue nec, viverra turpis. Ut quis sapien erat. In in pellentesque arcu. Suspendisse faucibus maximus quam non tincidunt. Donec efficitur risus et facilisis ultrices. Nullam lectus diam, ultrices a mauris a, iaculis scelerisque diam. Aliquam erat volutpat. Nulla nisl felis, scelerisque eget porta sed, molestie vitae lorem. Suspendisse facilisis, lorem in aliquam gravida, massa quam sodales mi, eu mattis dui ipsum sed lorem. Vivamus porta turpis id nulla porta, eu pellentesque diam faucibus. Sed vehicula, leo id tempor tempus, nisl eros varius est, quis consectetur arcu nulla id risus. </p><p><br></p><p><img src=\"http://webstart.dev/uploads/pages/13.36.58-004b0b5b90c180bb7615bc830ab7be8a.jpeg\" style=\"width: 696px;\"><br></p>', 'За нас', 'За нас', 'main', 0, 1, NULL, '2017-09-21 05:34:06'),
(9, 'Общи условия за ползване на услугата UnyQTV', 'obschi-usloviya-za-polzvane-na-uslugata-unyqtv', '<p>ОБЩИ УСЛОВИЯ<br>за взаимоотношенията с крайните потребители на Уникомс сървисиз ЕООД за ползване на услугата UnyQTV на територията на Република България<br>&nbsp;<br>РАЗДЕЛ I<br>Предмет и общи положения<br>1.&nbsp;&nbsp; &nbsp;С настоящите Правила (“Правилата”) се определят реда, начина и условията, при които Уникомс сървисиз ЕООД, наричан по-нататък „Доставчикът“, предоставя на Потребители услугата UnyQTV.<br>2.&nbsp;&nbsp; &nbsp;Адрес за кореспонденция и телефон за контакт с Доставчика:<br>&nbsp;„Уникомс сървисиз“ ЕООД, вписано в Търговския регистър към Агенцията по вписванията с ЕИК 204329810, със седалище в гр. София и адрес на управление София 1766, София парк, сграда 16Б, офис 3, тел.: +359 2 9741414.<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 3. Тези Общи условия са задължителни за Уникомс сървисиз ЕООД и ПОТРЕБИТЕЛИТЕ и са неразделна част от индивидуалния договор, сключен между тях.<br>&nbsp;&nbsp;&nbsp;&nbsp; 4. ПОТРЕБИТЕЛИ на услугите могат да бъдат всички физически лица.<br>&nbsp;<br>РАЗДЕЛ II<br>Понятия<br>5. По смисъла на настоящите Общи условия следните думи и изрази, включително и когато са членувани и/или употребени в множествено число ще имат посочените в тази точка значения:<br>5.1. „Услугата” означава услугата UNYQTV, която се предоставя от Доставчика и позволява на абонатите по действащ договор за услуга, предоставяна от УНИКОМС СЪРВИСИЗ ЕООД, нови абонати, подписващи срочен договор или друг вид абонати определени от УНИКОМС СЪРВИСИЗ ЕООД да гледат телевизионно съдържание на различни крайни мобилни устройства (таблет или мобилен телефон – смартфон) посредством мобилното приложение UNYQTV.<br>5.2. „Телевизионна услуга“ означава всяка телевизионна услуга (в това число допълнителни услуги, свързани с нея като услуги, позволяващи гледане на телевизионно съдържание през различни устройства и др.), предоставяна от Доставчика посредством Мрежата, съгласно изискванията на Закона за електронните съобщения (ЗЕС) и действащото българско законодателство, която осигурява на абонатите на тази услуга достъп до телевизия (в това число до Съдържание) чрез различен технически способ, както и достъп до допълнителни услуги.<br>5.3. „Мрежа“ означава всяка електронна съобщителна мрежа, чрез която УНИКОМС СЪРВИСИЗ ЕООД разпространява Телевизионното съдържание до и предоставя Услугата на Потребителите.<br>5.4. „Потребител” означава дееспособно физическо лице, което отговаря на изискванията, посочени по-долу в раздел III от тези Общи условия, в резултата на което е оторизирано да ползва Услугата.<br>5.5. „Потребителски/Основен договор” е договорът, сключен от Потребителя с УНИКОМС СЪРВИСИЗ ЕООД за ползване на услуги, предоставяни от УНИКОМС СЪРВИСИЗ ЕООД, които позволяват активирането на Услугата.<br>5.6. „Съдържание” означава и включва телевизионно съдържание (телевизионни програми и/или техни елементи като филми, видео и др.) и излъчване на радио-програми, което съдържание е достъпно чрез Услугата.<br>5.7. „Действащо законодателство” означава всеки действащ (сега или в бъдеще) законов и/или подзаконов (нормативен или ненормативен (общ или индивидуален административен)) акт, който засяга Услугата иили Доставчика (и/или неговата дейност) и/или Потребителя и/или неговата дейност.<br>&nbsp;<br>РАЗДЕЛ III<br>Изисквания към потребителя<br>6. Потребител на Услугата може да е всяко физическо лице, което:<br>6.1. е абонат по действащ Потребителски договор за услуга, предоставяна от УНИКОМС СЪРВИСИЗ ЕООД, която включва в параметрите си и услугата UnyQTV. &nbsp;<br>7. При условие, че е изпълнил посочените в предходната точка изисквания, Потребителят е оторизиран да ползва Услугата единствено за лично ползване и единствено на територията на Република България.<br>РАЗДЕЛ IV<br>Описание на услугата<br>8. Услугата включва:<br>8.1. гледане/ слушане в реално време (едновременно с предаването) на Съдържание, което обхваща определен брой телевизионни и радио програми;<br>8.2. отложено във времето гледане на определени телевизионни програми (time shift) - до 24 часа след първоначалното излъчване;<br>8.3. записване на цялото или част от включеното в някои телевизионни програми съдържание (recording), стартиране, спиране и превъртане (pause, forward and reverse) на съответния запис. Общата продължителност на записите, които могат да бъдат направени в рамките на един период на ползване е до 24&nbsp; (двадесет и четири) часа като същите се съхраняват в рамките на 1 (един) ден след като са направени. Записите не се съхраняват на крайните устройства на Потребителите. Посочените в тази т.8.3 възможности са налице за част от телевизионните програми, за които Доставчикът разполага със съответни разрешения.<br>8.4. Други функции и менюта включени в приложението.<br>8.5. Услугата се предоставя от Доставчика посредством:<br>8.5.1. Приложение за смартфон и таблет: iOS, versions 7.0 и следващи версии, Android 4.2.x и следващи версии.<br>9. Достъп на Потребителите до Услугата може да бъде осигурен и по други начини, извън посочените в предходната точка, при наличие на възможност на съответния Доставчик за такова предоставяне. При осигуряване на допълнителен начин за ползване на Услугата от потребителите, информация за това се публикува на интернет страницата http://www.unyqtv.com<br>10. Ползването на Услугата започва от активирането на последната съгласно условията на раздел V по-долу.<br>11. При предоставянето на Услугата по преценка на доставчика могат да бъдат включвани рекламни банери и други видове нотификации.<br>&nbsp;<br>РАЗДЕЛ V<br>Заявяване и активиране на услугата<br>12. Заявяване<br>12.1. Потребителят подписва Заявление/Договор по образец на УНИКОМС СЪРВИСИЗ ЕООД, в което се определят вида и срока на Услугата избрани от Потребителя и други специфични условия за предоставяне на Услугата.<br>12.1.1.Преди подписването на Заявлението/Договор за ползване на Услугата UNYQTV, Потребителят предоставя на УНИКОМС СЪРВИСИЗ ЕООД достоверна информация, документ за самоличност, както и други необходими документи.<br>12.1.2.Потребителят е длъжен да информира УНИКОМС СЪРВИСИЗ ЕООД за настъпили промени в предоставените данни за идентификация в рамките на 3 (три) календарни дни от настъпване на промяната.<br>12.2.В случай че Потребителят не заяви писмено желание Заявлението за ползване на Услугата да влезе в сила незабавно след подписването му, услугата се активира от момента, в който изтече 7-дневния срок от датата на подписване на Заявлението/Договор, в който срок Потребителят може да се откаже от последното.<br>13. Инсталиране и достъп до Услугата<br>13.1. За да ползва активираната услуга потребителят следва самостоятелно да инсталира, осигуреното от УНИКОМС СЪРВИСИЗ ЕООД софтуерно приложение за достъп до нея.<br>Приложението се сваля от съответните платформи за сваляне на приложения – Apple Store и Google Play Store и се инсталира съгласно инструкциите публикувани там и предоставяни по време на инсталацията.<br>13.1.1. След като инсталира приложението, Потребителят следва да се регистрира в него, съгласно инструкциите в същото.<br>13.1.2. Регистрацията се извършва чрез потребителското име и парола за достъп до профила на потребителя.<br>13.1.3. Ако Потребителят не е регистриран, следва да направи такава регистрация, преди да може да получи достъп до Услугата.<br>13.1.4.&nbsp;&nbsp; След извършване на регистриранията Потребителят трябва да въведе персонален идентификационен код (потребителско име и парола), предоставен му от УНИКОМС СЪРВИСИЗ ЕООД при подписване на договора/заявлението за Услугата, във връзка с активиране на пълната функционалност на приложението.<br>13.1.5. Потребителят е длъжен да пази конфиденциалността на своите потребителско име, парола и идентификационен код и да не ги преотстъпва на други лица.<br>13.1.6. Всяко ползване на потребителско име, парола и идентификационен код включително чрез създадени от този потребител допълнителни регистрации, се счита, че е извършено от лицето, на което те са предоставени от УНИКОМС СЪРВИСИЗ ЕООД и обвързва това лице, независимо дали е разрешило и/или одобрило ползването на потребителското име и паролата му, освен ако докаже, че те са узнати по противоправен начин от трето лице.<br>13.1.7. Електронните изявления, потвърдени чрез потребителското име и парола създадени от Потребителя, ще имат валидност като със саморъчен подпис, по силата на чл. 13 от Закона за електронния документ и електронния подпис.<br>13.2. Включени допълнителни услуги и съдържание в приложението:<br>13.2.1. Достъп и до определени радио програми.<br>13.2.2 Телевизионна програма на включените в приложението телевизионни канали.<br>13.2.3. Рекламни съобщения и специални предложения.<br>&nbsp;<br>РАЗДЕЛ VI<br>Изисквания за достъпност и съвместимост на Услугата<br>14.1. УНИКОМС СЪРВИСИЗ ЕООД предоставя на потребителите актуална информация относно техническите изисквания за нормална работа на приложението за достъп до Услугата, необходимата свързаност и оборудване, с които следва да разполага Потребителят.<br>Посочената информация се публикува и актуализира на интернет страницата http://www.unyqtv.com/&nbsp; и в самото приложение.<br>14.1.2. Услугата UNYQTV е базирана на разпространение на стрийминг сигнал през интернет, което осигурява достъпност до Услугата на територията на България, там където Потребителят има мобилен или фиксиран достъп до високоскоростен интернет, осигуряван от използвания от него доставчик.<br>Услугата не се предоставя за конкретен адрес, а на цялата територия на България. Достъпността до Услугата е в зависимост от параметрите на мрежата в конкретния момент и място за ползване на Услугата.<br>14.1.3. В зависимост от техническите параметри на свързаността, може да няма техническа възможност за предоставяне само на част от пакетите или част от съдържанието, разпространявано чрез платформата.<br>14.1.4. Услугата може да се ползва на до 3 устройства. Потребителят може да променя регистрираните от него крайни устройства в рамките на максимално допустимия брой, по своя преценка.<br>14.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ограничения за ползване на Услугата.<br>14.2.2.&nbsp;&nbsp; Потребителят няма право да препродава или предоставя под каквато и да е форма ползването на Услугата и/или предоставеното от УНИКОМС СЪРВИСИЗ ЕООД приложение на трети лица.<br>14.2.3.&nbsp;&nbsp; Потребителят няма право да препродава, да излъчва или да позволява излъчване на програмата в места с публичен достъп, или да използва по какъвто и да било начин услугата за търговски цели, като гарантира, че Услугата ще се ползва в помещения с ограничен достъп, предназначени за трайно ползване от конкретни физически лица.<br>14.2.4.&nbsp;&nbsp; С подписване на Заявлението/Договор за ползване на Услугата UNYQTV Потребителят декларира, че Услугата няма да бъде ползвана в обществени или публични зони или места включително хотелски лобита, барове, ресторанти или обществени площади, както и всякакви помещения или места, експлоатирани с търговска цел или такива, които имат социално или друго обществено предназначение, което предполага публично и/или масово разпространение, включително хотели, мотели, пансиони, затвори, болници, частни клиники и други подобни функционални структури (всяко \"място, обитаемо от много хора \"), които приемат телевизионните канали, видео съдържание и приложения, включени в Услугата.<br>14.2.5.&nbsp;&nbsp; Потребителят няма право да записва на други устройства и носители съдържание, предоставяно чрез UnyQTV платформата, както и да го изменя и разпространява допълнително по какъвто и да било начин.<br>&nbsp;<br>РАЗДЕЛ VII<br>Срок на предоставяне на услугата<br>15.1. Срокът на предоставяне на Услугата UNYQTV е изцяло в зависимост от продължителността на срока на основния договор за ползване на услуги, предоставяни от УНИКОМС СЪРВИСИЗ ЕООД, който конкретният абонат е сключил или сключва с УНИКОМС СЪРВИСИЗ ЕООД.<br>15.1.1. Достъпът до Услугата се преустановява от момента на прекратяване на основния договор без значение от основанието, във връзка с което последният се прекратява.<br>&nbsp;<br>РАЗДЕЛ VIII<br>Цена и начин на плащане<br>16.1. УНИКОМС СЪРВИСИЗ ЕООД събира от Потребителя суми за предоставяната от него Услуга - UNYQTV, както следва:<br>16.1.1. Месечна абонаментна цена, осигуряваща достъп до Услугата UNYQTV.<br>16.1.2.&nbsp; Други цени, договорени с УНИКОМС СЪРВИСИЗ ЕООД.<br>16.2. Цената по т.16.1.1. за предоставяната Услуга се заплаща в офисите на оторизирани от УНИКОМС СЪРВИСИЗ фирми.<br>16.3. УНИКОМС СЪРВИСИЗ ЕООД или оторизираната от УНИКОМС СЪРВИСИС фирма издава индивидуален документ на Потребителя (например: фактура, касова бележка) за получено плащане по различните видове цени при всяка от използваните форми на разплащане. Фактура се предоставя на Потребителя, в случай че той е направил изрично писмено волеизявление за това.<br>16.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Неплащане<br>16.4.1. В случай че Потребителят не е платил за Услугата след определената за плащане крайна дата, Потребителят дължи на УНИКОМС СЪРВИСИЗ ЕООД обезщетение, за всички просрочени задължения в размер на законната лихва от деня на забавата до момента на заплащане на дължимите суми, съгласно Закона за задълженията и договорите.<br>16.4.2. В случаите на неплащане на дължимите суми в срок или при неизпълнение на други съществени задължения от страна на Потребителя, УНИКОМС СЪРВИСИЗ ЕООД може по всяко време да спре предоставянето на Услугата.<br>16.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Промяна на цените<br>16.5.1. УНИКОМС СЪРВИСИЗ ЕООД прави публична ценовата листа за предоставяната от него Услуга. При промяна на размера на цените в ценовата листа, тя се публикува преди датата на влизане в сила. УНИКОМС СЪРВИСИЗ ЕООД предоставя ценовата листа на разположение на Потребителите на общодостъпни места, включително във всеки свой офис, на територията, на който предоставя услугите си.<br>&nbsp;<br>РАЗДЕЛ IX<br>Спиране и прекратяване на ползването на Услугата<br>17.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Временно спиране на предоставянето на Услугата<br>УНИКОМС СЪРВИСИЗ ЕООД си запазва правото да спира временно предоставянето на Услугата в следните случаи:<br>17.1.1.&nbsp;&nbsp; Планирани профилактични ремонти или мероприятия с цел оптимизиране на качествени параметри на Услугата и аварийни ремонти;<br>17.1.2.&nbsp;&nbsp; Повреда или смущения в мрежата за разпространяване на телевизионните програми до отстраняване на повредата или смущението и/или спиране на електрическото захранване на регионалната или национална електрическа мрежа до възстановяване на захранването;<br>17.1.3.&nbsp;&nbsp; При настъпване на обстоятелства на непреодолима сила до отпадането им;<br>17.1.4.&nbsp;&nbsp; Когато Потребителят ползва Услугата по начин, който затруднява или пречи на нормалната работа на други потребители на УНИКОМС СЪРВИСИЗ ЕООД или им причинява вреди;<br>17.1.5.&nbsp;&nbsp; Когато телевизионна програма не е достъпна от мрежата на УНИКОМС СЪРВИСИЗ ЕООД до осигуряване на такъв достъп;<br>17.1.6.&nbsp;&nbsp; Когато Потребителят забавя плащането на Услугата след определената крайна дата на плащане (падежа);<br>17.1.7.&nbsp;&nbsp; Когато Потребителят предоставя Услугата на трети лица или извърши друго нарушение на ограниченията по тези Общи условия.<br>17.2. Прекратяване на предоставянето на Услугата<br>17.2.1. Предоставянето на Услугата се преустановява от момента на прекратяване на Основния договор, който Потребителят е сключил с УНИКОМС СЪРВИСИЗ ЕООД.<br>17.3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Прекратяване на Услугата преди изтичане на избрания със Заявлението/Договор срок на ползване се допуска при следните обстоятелства:<br>17.3.1.&nbsp;&nbsp; При взаимно съгласие на двете страни;<br>17.3.2.&nbsp;&nbsp; При настъпване на обстоятелства от непреодолима сила съгласно чл. 306 от Търговския закон, които трайно препятстват предоставяне на Услугата;<br>17.3.3.&nbsp;&nbsp; В резултат на действия или актове на компетентни държавни органи, които водят до ограничаване възможността за предоставяне на Услугата, от всяка от страните с писмено уведомление;<br>17.3.4.&nbsp;&nbsp; При смърт или поставяне под запрещение на Потребителя;<br>17.3.5.&nbsp;&nbsp; Ликвидация или обявяване в несъстоятелност на една от страните по Договора, считано от датата на влизане в сила на съответното решение;<br>17.3.6. Едностранно и без предизвестие от УНИКОМС СЪРВИСИЗ ЕООД, в случай на неплащане на цените по т. 16 от тези Общи условия от страна на Потребителя;<br>17.3.7. Едностранно и без предизвестие от УНИКОМС СЪРВИСИЗ ЕООД, при неизпълнение на задълженията по т. 14.2 от тези Общи условия;<br>17.3.8.&nbsp;&nbsp; Едностранно, от страна на УНИКОМС СЪРВИСИЗ ЕООД, при липса на техническа възможност за предоставяне на Услугата. УНИКОМС СЪРВИСИЗ ЕООД ще уведоми Потребителя за прекратяване на Договора, като посочи причината за липса на техническа възможност;<br>18.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Отговорност за неизпълнение<br>18.1. За всички случаи на неизпълнение на задълженията си по Общите условия страните носят отговорност в съответствие с разпоредбите на действащото българско законодателство.<br>18.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; УНИКОМС СЪРВИСИЗ ЕООД не отговаря пред Потребителя за:<br>1.&nbsp;&nbsp; &nbsp;Липса на умения от страна на Потребителя да използва Услугата; Претенции от трети лица срещу Потребителя при и по повод ползване на Услугата;<br>2.&nbsp;&nbsp; &nbsp;Повреди по мрежата за достъп до интернет, които правят технически невъзможно предоставянето на Услугата, както и за всички други случаи, при които прекъсването на излъчването на програма се дължи на обстоятелства извън контрола на УНИКОМС СЪРВИСИЗ ЕООД;<br>3.&nbsp;&nbsp; &nbsp;УНИКОМС СЪРВИСИЗ ЕООД не отговаря за вреди, причинени на трети лица при използване на Услугата от Потребителите, както и използването на Услугата за незаконни цели по какъвто и да е начин или извършването на незаконни действия чрез Услугата. УНИКОМС СЪРВИСИЗ ЕООД не носи отговорност за съдържанието и качеството на сигнала на телевизионните програми, пренасяни чрез Услугата и мултимедийното съдържание.<br>18.3. За неотстранени повреди в Софтуера, в резултат, на които Потребителят не е могъл да ползва Услугата повече от 10 дни през един календарен месец, Потребителят заплаща част от дължимата месечна абонаментна цена, пропорционална на периода, през който е ползвал Услугата. Не е необходимо посочените дни да са последователни. УНИКОМС СЪРВИСИЗ ЕООД приспада съответната сума от стойността на месечната абонаментна цена за следващия месец на базата на получени и регистрирани уведомления, изпратени до дружеството за повреди, прекъсвания и други форми на неизправно получаване на Услугата.<br>18.4. УНИКОМС СЪРВИСИЗ ЕООД не дава никакви други гаранции, които не са изрично предвидени в тези общи условия, относно информация, съдържание, услуги, функционирането и възможностите за достъп до услугата.<br>18.4.1. УНИКОМС СЪРВИСИЗ ЕООД не отговаря за съдържанието на препредаваното съдържание, осигурявано от трети лица.<br>18.4.2. УНИКОМС СЪРВИСИЗ ЕООД не носи отговорност, ако при нарушения на работоспособността на програмното или техническото осигуряване на други оператори в Интернет или на телекомуникационните връзки в страната, регистриран Потребител не може да ползва частично или напълно възможностите на услугите, предоставяни в Портала на УНИКОМС СЪРВИСИЗ ЕООД.<br>&nbsp;<br>РАЗДЕЛ X<br>Интелектуална собственост<br>19. Съдържанието на Услугата и приложението за достъп до нея са обект на права на интелектуална собственост на УНИКОМС СЪРВИСИЗ ЕООД или на трети лица.<br>19.1. Съдържанието включва, но не се ограничава до всякакви текстове, изображения, музикални и аудио-визуални произведения, дизайн, лога, подредба, технологии и др.<br>19.2. Използваните в Услугата търговски марки, лога и наименования са интелектуална собственост на съответните им притежатели.<br>19.3. Услугата е предназначена за ползване само на територията на Република България.<br>19.4. УНИКОМС СЪРВИСИЗ ЕООД е единствен носител на правото да разпространява звукозаписи и аудио-визуални произведения чрез Услугата. Всяко копиране и разпространение на записите и аудио-визуалните произведения, доколкото не е изрично разрешено в настоящите Общи условия, е нарушение на Закона за авторското право и сродните му права и други приложими нормативни актове и подлежи на съответни санкции.<br>19.5. УНИКОМС СЪРВИСИЗ ЕООД предоставя единствено неизключително, непрехвърлимо право за лично ползване, включващо възпроизвеждане на съдържанието само на устройства за лична употреба като не се предоставят никакви права освен тези за лично ползване.<br>&nbsp;<br>РАЗДЕЛ X<br>Други условия<br>20.1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Лични данни<br>20.1.1. С приемане на настоящите Общи условия, Потребителят се счита за информиран, че част от данните, които предоставя или е предоставил, са лични данни и попадат под специална защита по смисъла на Закона за защита на личните данни (ЗЗЛД). С приемане на тези Общи условия Потребителят се счита за информиран и дава съгласие, че УНИКОМС СЪРВИСИЗ ЕООД може да събира и обработва, както и да предоставя тези лични данни, в съответствие с разпоредбите на действащото законодателство, с оглед на предоставяне на UnyQTV услугата, например за целите на събиране на дължимите от Потребителя суми към УНИКОМС СЪРВИСИЗ ЕООД, както и за извършване на кредитна оценка на Потребителя на УНИКОМС СЪРВИСИЗ ЕООД, както и за изчисляване броя на потребителите на Услугата. УНИКОМС СЪРВИСИЗ ЕООД се задължава да обработва, използва и съхранява тези лични данни, гарантирайки тяхната конфиденциалност. Потребителят предоставя личните си данни доброволно и дава съгласие, за да бъдат реализирани правата и задълженията му по договора, УНИКОМС СЪРВИСИЗ ЕООД да ги предоставя на компетентни държавни органи и институции в предвидените от закона случаи, както и да ги предоставя на трети лица, в това число банки, кредитни бюра и агенции за събиране на вземания, дистрибуторите на услуги на УНИКОМС СЪРВИСИЗ ЕООД, включително партньорските дистрибуторски мрежи, както и на доставчиците на телевизионните и радио-програми, включени в Услугата UNYQTV, когато това е необходимо за осигуряване на обслужването и реализиране на правата и интересите на Потребителя, както и когато това е необходимо за реализиране правата и интересите на УНИКОМС СЪРВИСИЗ ЕООД, включително с цел събиране на неизплатени задължения по договора.<br>20.2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; За всички неуредени в Общите условия за предоставяне на Услугата UNYQTV въпроси се прилагат разпоредбите на действащото българско законодателство.<br>20.3. Всички спорове, възникнали във връзка с действителността, изпълнението, тълкуването или прекратяването на настоящото споразумение, ще бъдат решавани от компетентния съд в гр. София.<br>20.4.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Тълкуването на Договора между страните ще се извършва при следния ред на предпочитание: Заявлението/Договор за ползване на Услугата UNYQTV, заявки за предоставяне на допълнителни услуги към Услугата UNYQTV (ако има такива), Спецификация и ценова листа на услугата UNYQTV, настоящите Общи условия за предоставяне на Услугата UNYQTV, като при няколко документа от един и същ вид, предимство ще имат клаузите от документите с по-късна дата.<br>20.5.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; В случай, че някоя от клаузите на настоящите Общи условия се окаже недействителна, това няма да окаже въздействие върху валидността на всички останали.<br>20.6. Всякакви въпроси от страна на Потребителя относно предоставянето на Услугата, цени и условията на плащане, тарифни планове, технически проблеми, прекратяване на правоотношенията, поддръжка и др. могат да се отправят на телефона за Обслужване на клиенти на УНИКОМС СЪРВИСИЗ ЕООД, както и във всеки търговски център на УНИКОМС СЪРВИСИЗ ЕООД.<br><br><br></p>', 'Общи условия за ползване на услугата UnyQTV', 'Общи условия за ползване на услугата UnyQTV', 'main', 0, 1, '2017-09-21 05:34:57', '2017-09-21 05:34:57');

-- --------------------------------------------------------

--
-- Table structure for table `pages_images`
--

CREATE TABLE `pages_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `pages_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pages_images`
--

INSERT INTO `pages_images` (`id`, `pages_id`, `filename`, `is_main`, `created_at`, `updated_at`) VALUES
(8, 1, '13.57.58-7968b4c023cc4a91cb3d22fcdfad5e04.jpeg', 1, '2017-07-19 10:57:58', '2017-09-21 05:33:57');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `channels` text COLLATE utf8mb4_unicode_ci,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `title`, `content`, `metaTitle`, `metaDescription`, `channels`, `active`, `created_at`, `updated_at`) VALUES
(1, 'LIGHT 25+ канали', '<div class=\"paket-name\">LIGHT</div>                     <div class=\"paket-number\">25+</div>                     <div class=\"channel-label\">канали</div>', 'LIGHT 25+ канали', 'LIGHT 25+ канали', '[\"1\",\"2\",\"4\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"18\",\"21\",\"22\",\"26\",\"29\",\"32\",\"34\",\"44\",\"45\",\"46\",\"47\",\"56\",\"63\",\"64\",\"65\",\"66\",\"67\",\"68\"]', 1, '2017-09-04 11:37:11', '2017-09-28 09:55:18'),
(2, 'SMART 35+ канали', '<div class=\"paket-name\">SMART</div>                     <div class=\"paket-number\">35+</div>                     <div class=\"channel-label\">канали</div>', 'SMART 35+ канали', 'SMART 35+ канали', '[\"1\",\"2\",\"4\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"18\",\"21\",\"22\",\"26\",\"29\",\"32\",\"34\",\"35\",\"40\",\"44\",\"45\",\"46\",\"47\",\"49\",\"50\",\"51\",\"52\",\"56\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"68\"]', 1, '2017-09-26 06:25:17', '2017-09-28 09:55:46'),
(3, 'UNYQ 40+ канали', '<div class=\"paket-name\">UNYQ</div>                     <div class=\"paket-number\">40+</div>                     <div class=\"channel-label\">канали</div>', 'UNYQ 40+ канали', 'UNYQ 40+ канали', '[\"1\",\"2\",\"4\",\"6\",\"7\",\"8\",\"9\",\"10\",\"11\",\"18\",\"21\",\"22\",\"26\",\"29\",\"32\",\"34\",\"35\",\"40\",\"44\",\"45\",\"46\",\"47\",\"49\",\"50\",\"51\",\"52\",\"53\",\"54\",\"55\",\"56\",\"58\",\"59\",\"60\",\"61\",\"62\",\"63\",\"64\",\"65\",\"66\",\"67\",\"68\"]', 1, '2017-09-26 06:25:52', '2017-09-28 09:56:15');

-- --------------------------------------------------------

--
-- Table structure for table `products_images`
--

CREATE TABLE `products_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `products_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaTitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metaDescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `title`, `content`, `metaTitle`, `metaDescription`, `active`, `created_at`, `updated_at`) VALUES
(1, 'Какво е UnyQTV?', '<p><font color=\"#FF0000\">UnyQ</font>TV<font color=\"#FF0000\"><span style=\"background-color: rgb(255, 0, 0);\"><span style=\"background-color: rgb(255, 255, 255);\"></span></span></font>&nbsp; е софтуерно приложение за гледане на телевизионни програми на екрана на мобилно устройство: смартфон или таблет – навсякъде и по всяко време.<br></p>', 'Какво е UnyQTV?', 'Какво е UnyQTV?', 1, '2017-09-04 11:05:37', '2017-09-21 11:57:07'),
(2, 'Какви основни функции има приложението UnyQTV?', '<ul><li>Гледане на избран от основното меню телевизионен канал;</li><li>Електронен програмен справочник за всички канали, включени в пакета;</li><li>Гледане на запис на вече излъчени предавания;&nbsp;</li><li>Търсене на предаване по ключова дума от заглавието;&nbsp;</li><li>Връщане на зазад в гледаното в момента предаване;</li><li>Гледане на предаване във формат по избор – „портрет“ или „пейзаж“;</li><li>Показва вградени субтитри; Адаптивно качество на изображението в зависимост от достъпната в момента скорост на интернет свързаността.</li></ul>', 'Какви основни функции има приложението UnyQTV?', 'Какви основни функции има приложението UnyQTV?', 1, '2017-09-05 02:55:13', '2017-09-21 10:10:51'),
(3, 'Как да получа достъп до услугата?', 'За да гледате услугата, моля свържете се с вашия кабелен или интернет оператор.<br>', 'Как да получа достъп до услугата?', 'Как да получа достъп до услугата?', 1, '2017-09-05 02:55:22', '2017-09-21 09:44:15'),
(4, 'Как да заплатя таксата за използване на услугата?', 'Услугата се заплаща на касите на вашия кабелен или интернет оператор, който Ви я предоставя.<br>', 'Как да заплатя таксата за използване на услугата?', 'Как да заплатя таксата за използване на услугата?', 1, '2017-09-05 02:55:32', '2017-09-21 09:45:02'),
(5, 'На какви устройства мога да гледам телевизионните програми?', 'Приложението <font color=\"#FF0000\">UnyQ</font>TV е предназначено за всички устройства с операционни системи: Android с версия 4.4.2 и по-висока, iOS с версия 7.0 и по-висока, включително Android IPTV STB.', 'На какви устройства мога да гледам телевизионните програми?', 'На какви устройства мога да гледам телевизионните програми?', 1, '2017-09-05 02:55:39', '2017-09-25 03:23:32'),
(6, 'Мога ли да гледам телевизия чрез UnyQTV през WiFi?', 'Да, можете - няма ограничения в типа свързване с Интернет мрежата.', 'Мога ли да гледам телевизия чрез UnyQTV през WiFi?', 'Мога ли да гледам телевизия чрез UnyQTV през WiFi?', 1, '2017-09-05 02:55:49', '2017-09-21 09:47:15'),
(7, 'В мрежата на кои мобилни оператори мога да гледам чрез UnyQTV телевизия?', 'Няма ограничения. Можете да използвате мрежата на който и да е мобилен оператор, стига тя да осигурява 3G или 4G свързаност.', 'В мрежата на кои мобилни оператори мога да гледам чрез UnyQTV телевизия?', 'В мрежата на кои мобилни оператори мога да гледам чрез UnyQTV телевизия?', 1, '2017-09-05 02:55:58', '2017-09-21 09:48:09');

-- --------------------------------------------------------

--
-- Table structure for table `questions_images`
--

CREATE TABLE `questions_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `questions_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `title`, `content`, `slogan`, `active`, `created_at`, `updated_at`) VALUES
(2, 'Slide 1', '<p>ТЕЗИ, КОИТО СА ДОСТАТЪЧНО ЛУДИ ДА МИСЛЯТ, ЧЕ МОГАТ ДА ПРОМЕНЯТ СВЕТА, СА ТЕЗИ, КОИТО ГО ПРАВЯТ<br></p>', 'Стив Джобс', 1, '2017-08-30 10:52:54', '2017-09-05 04:52:36'),
(3, 'Slide 2', '<p>ТЕЗИ, КОИТО СА ДОСТАТЪЧНО ЛУДИ ДА МИСЛЯТ, ЧЕ МОГАТ ДА ПРОМЕНЯТ СВЕТА, СА ТЕЗИ, КОИТО ГО ПРАВЯТ<br></p>', 'Стив Джобс', 1, '2017-09-05 01:08:20', '2017-09-05 04:52:52');

-- --------------------------------------------------------

--
-- Table structure for table `sliders_images`
--

CREATE TABLE `sliders_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `sliders_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_main` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders_images`
--

INSERT INTO `sliders_images` (`id`, `sliders_id`, `filename`, `is_main`, `created_at`, `updated_at`) VALUES
(8, 2, 'jSsWjMttLckzgpVWUZHuBrS343NrOblqqm50Z2xB.jpeg', 1, '2017-09-28 09:58:32', '2017-09-28 09:58:33'),
(9, 3, 'PyGmMhPN8pD2zvIxmJe1ZEkMm57KYLXYcw0IZ6Kp.jpeg', 1, '2017-09-28 09:58:48', '2017-09-28 09:58:49');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(10) UNSIGNED NOT NULL,
  `accessToken` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expirationDate` int(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`id`, `accessToken`, `expirationDate`, `created_at`, `updated_at`) VALUES
(3, '3e6c8951-87e4-47be-9cbc-b6fdcea1127f', 1506425719, '2017-09-25 08:42:00', '2017-09-25 08:42:00'),
(4, 'd1992a28-3056-47dd-b096-bf98ebc0a8d2', 1506511724, '2017-09-26 08:35:25', '2017-09-26 08:35:25'),
(5, 'd5b66c0b-f450-438c-a9b6-395fa053eb86', 1506603853, '2017-09-27 10:10:54', '2017-09-27 10:10:54'),
(6, '91dae011-a83f-4702-b0bf-7d1d669076fd', 1506696050, '2017-09-28 11:47:31', '2017-09-28 11:47:31');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Atanas', 'atanasov@webstart.bg', '$2y$10$pVao.InV4d0hMN3Z7dXi5.gSen8D.jAReuDOXWvsWtGu07MSJpcTW', 'Administrator', 'Qb4mZzdHRehCUUMSf3VPmkhQoNEuzVGS0MjkRPHvhOFROFqD33k0rAosTySc', '2017-07-07 17:16:48', '2017-07-07 17:16:48'),
(2, 'Nastya', 'nastya@unicomstrading.com', '$2y$10$pVao.InV4d0hMN3Z7dXi5.gSen8D.jAReuDOXWvsWtGu07MSJpcTW', 'Administrator', 'qg43y5GL8rCKJtsQ6T7zEcK1mQ1ObvlwBCUv4wrtaVSRjdHCOZU62av3lvrr', '2017-07-07 17:16:48', '2017-07-07 17:16:48');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blocks`
--
ALTER TABLE `blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blocks_images`
--
ALTER TABLE `blocks_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageId` (`blocks_id`);

--
-- Indexes for table `channels`
--
ALTER TABLE `channels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `channels_images`
--
ALTER TABLE `channels_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `channelId` (`channels_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_images`
--
ALTER TABLE `news_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `newId` (`news_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages_images`
--
ALTER TABLE `pages_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageId` (`pages_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_images`
--
ALTER TABLE `products_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `productId` (`products_id`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `questions_images`
--
ALTER TABLE `questions_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `questionId` (`questions_id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders_images`
--
ALTER TABLE `sliders_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pageId` (`sliders_id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blocks`
--
ALTER TABLE `blocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `blocks_images`
--
ALTER TABLE `blocks_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `channels`
--
ALTER TABLE `channels`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `channels_images`
--
ALTER TABLE `channels_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `news_images`
--
ALTER TABLE `news_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pages_images`
--
ALTER TABLE `pages_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=105;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `products_images`
--
ALTER TABLE `products_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `questions_images`
--
ALTER TABLE `questions_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `sliders_images`
--
ALTER TABLE `sliders_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `blocks_images`
--
ALTER TABLE `blocks_images`
  ADD CONSTRAINT `sectionId` FOREIGN KEY (`blocks_id`) REFERENCES `blocks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `channels_images`
--
ALTER TABLE `channels_images`
  ADD CONSTRAINT `channelId` FOREIGN KEY (`channels_id`) REFERENCES `channels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `news_images`
--
ALTER TABLE `news_images`
  ADD CONSTRAINT `newId` FOREIGN KEY (`news_id`) REFERENCES `news` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pages_images`
--
ALTER TABLE `pages_images`
  ADD CONSTRAINT `pageId` FOREIGN KEY (`pages_id`) REFERENCES `pages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products_images`
--
ALTER TABLE `products_images`
  ADD CONSTRAINT `productId` FOREIGN KEY (`products_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `questions_images`
--
ALTER TABLE `questions_images`
  ADD CONSTRAINT `questionId` FOREIGN KEY (`questions_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sliders_images`
--
ALTER TABLE `sliders_images`
  ADD CONSTRAINT `sliderId` FOREIGN KEY (`sliders_id`) REFERENCES `sliders` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
