<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collections extends Model
{
 
    public function products()
    {
        return $this->hasMany('App\Products', 'collection_id');
    }

}
