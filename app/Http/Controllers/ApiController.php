<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Products;
use App\Categories;
use App\Products_images;

use DB;
use Input;
use App\Http\Controllers\Controller;

class ApiController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {



    }
 public function category_info()
    {
        
        $category = null;

        if(Input::get('category'))
            $category = Categories::where('slug', Input::get('category'))->first();
        
        if(Input::get('subcategory'))
            $category = Categories::where('slug', Input::get('subcategory'))->where('parent_id', $category->id)->first();

        if(isset($category->attributes)){
            $attributes = $category->attributes;
            foreach($attributes as $a){
                if($a->values)
                    $a->values = $a->values;
            }

            $category->attributes = $attributes;

            if(isset($category->parent))
                $category->parent = $category->parent;
        }
        
        return $category;

}

 public function product($slug)
    {
        
        $product = null;

        if($slug)
            $product = Products::where('slug', $slug)->where('active', 1)->first();

        if($product) {
            if($product->attributes) {
                $attribute_groups = [];
                foreach($product->attributes as $attribute) {
                    $attribute_groups[$attribute->parent->title]['group']   = $attribute->parent;
                    $attribute_groups[$attribute->parent->title]['values'][] = $attribute;
                }
                unset($product->attributes);
                $product->attributes = $attribute_groups;
                    // $attributes->Размер = $attributes->parent->attribute;

            }
            $product->attribute_groups_count = ( count($product->attributes) > 0) ? count($product->attributes) : 0;

            $product->tags = $product->tags;
            $product->category = $product->category;

            if(isset($product->category->parent))
                $product->category->parent = $product->category->parent;

            $images = $product->images;

            if(isset($images)){
                foreach($images as $img) {
                    $img->link = url('/').'/image/1000/?link=public/uploads/products/'.$img->filename;
                    $img->thumb = url('/').'/image/128/?link=public/uploads/products/'.$img->filename;
                    $img->blur = url('/').'/image/16/?link=public/uploads/products/'.$img->filename;
                }
                $product->gallery = $images;
            }


            $image = $product->images->where('is_main','=', 1)->first();
            if(isset($image)):
                $product->main_image = url('/').'/image/1000/?link=public/uploads/products/'.$image->filename;
                $product->main_image_blur = url('/').'/image/16/?link=public/uploads/products/'.$image->filename;
            else:
                $product->main_image = url('/').'/image/1000/?link=public/uploads/no-image.jpg';
                $product->main_image_blur = url('/').'/image/16/?link=public/uploads/no-image.jpg';
            endif;
        }

        return $product;


}
    
    public function category()
    {
        // per page variable
        
        $perPage = 9;

        if(Input::get('category'))
            $category = Categories::where('slug', Input::get('category'))->first();
            // $category = Categories::where('slug', Input::get('category'))->where('parent_id', 0)->first();
 	  
        if(Input::get('subcategory'))
            $category = Categories::where('slug', Input::get('subcategory'))->where('parent_id', $category->id)->first();

        
        $pagination = Products::select('*');



        $pagination = $pagination->where('active', 1);





        if(Input::get('sortAttr')){
            $sortAttr = explode(',', Input::get('sortAttr'));
            $sortAttr = array_filter($sortAttr);


            $pagination = $pagination->join('products_attributes_values', 'products.id', '=', 'products_attributes_values.product_id')
                                     ->whereIn('products_attributes_values.attribute_id', $sortAttr);

        }
        
        if(isset($category->id) && $category->id > 0){
            $pagination = $pagination->where('category_id', $category->id);
        }



        if(Input::get('search')){
            $str = Input::get('search');
            $pagination = $pagination->Where('id', 'LIKE', "%$str%")
                                   ->orWhere('sku', 'LIKE', "%$str%")
                                   ->orWhere('barcode', 'LIKE', "%$str%")
                                   ->orWhere('title', 'LIKE', "%$str%")
                                   ->orWhere('content', 'LIKE', "%$str%")
                                   ->orWhere('intro', 'LIKE', "%$str%");
        }



        if(Input::get('sort') && Input::get('sort_type')) {
            $sort_type = 'ASC';
            $sort = Input::get('sort');
            $sort_type = Input::get('sort_type');

            // discount price order by workaround
            if($sort == 'price')  
                $pagination = $pagination->orderByRaw('IF(discount=0,price,discount) '.$sort_type);
            elseif($sort == 'created_at')  
                $pagination = $pagination->orderByRaw('UNIX_TIMESTAMP(created_at) '.$sort_type);
            else
            $pagination = $pagination->orderBy($sort, $sort_type);

        }


  
        $pagination = $pagination->groupBy('products.id');


        $pagination = $pagination->paginate($perPage);


        foreach($pagination as $p){
            $p->post_time = $this->humanTiming(strtotime($p->created_at));
            if(isset($p->category->title)) {
                $p->subcategoryTitle = $p->category->title;
                $p->subcategorySlug =  $p->category->slug;

                if(isset($p->category->parent->title)) {
                    $p->categoryTitle = $p->category->parent->title;
                    $p->categorySlug =  $p->category->parent->slug;
                }
            }

                $image = $p->images->where('is_main','=', 1)->first();
                
                if(isset($image)):
                    $p->main_image = url('/').'/image/420/?link=public/uploads/products/'.$image->filename;
                    $p->main_image_blur = url('/').'/image/16/?link=public/uploads/products/'.$image->filename;
                else:
                    $p->main_image = url('/').'/image/420/?link=public/uploads/no-image.jpg';
                    $p->main_image_blur = url('/').'/image/16/?link=public/uploads/no-image.jpg';
                endif;

            $p->tags = $p->tags;

        }

        if($pagination) {
            header('Content-Type: application/json');
                return json_encode($pagination);
        }
        else {
            return FALSE;
        }


    }     

    public function humanTiming($time){


    $periods = ['y' => [ 'година', 'години'  ] ,
                'm' => [ 'месец', 'месеца'   ] ,
                'w' => [ 'седмица', 'седмици' ] ,
                'd' => [ 'ден', 'дни'     ] ,
                'h' => [ 'час', 'часа'    ] ,
                'i' => [ 'минута', 'минути'  ] ,
                's' => [ 'секунда', 'секунди'  ] ];


    $time = time() - $time; // to get the time since that moment
    $time = ($time<1)? 1 : $time;
    $tokens = array (
        31536000 => $periods['y'], 
        2592000 => $periods['m'], 
        604800 => $periods['w'], 
        86400 => $periods['d'],
        3600 => $periods['h'],
        60 => $periods['i'], 
        1 => $periods['s'] 
    );

    foreach ($tokens as $unit => $t) {
        if ($time < $unit) continue;
        $numberOfUnits = floor($time / $unit);
        return $numberOfUnits.' '.(($numberOfUnits>1)?$t[0]:$t[1]);
    }

    }







}
