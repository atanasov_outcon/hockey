<?php

namespace App\Http\Controllers;

use Input; 
use Redirect;
use Validator;
use Response;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use App\Pages;
use App\Pages_images;
use App\Blocks;
use App\Blocks_images;
use App\Sliders;
use App\Sliders_images;
use App\News;
use App\News_images;
use App\Questions;
use App\Questions_images;
use App\Products;
use App\Products_images;
use App\Channels;
use App\Channels_images;

class PagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//
    }


    public function view($slug=null) {
       
        $data = array();


        // $data['sliders'] =  Sliders::select('*')
        //                             ->where('sliders.active', 1)
        //                             ->get();

        // $data['questions'] =  Questions::select('*')
        //                             ->where('questions.active', 1)
        //                             ->get();  

        // $data['news'] =  News::select('*')
        //                             ->where('news.active', 1)
        //                             ->orderBy('news.id', 'DESC')
        //                             ->get(2);     

        // $blocks =  Blocks::select('*')
        //                             ->where('blocks.active', 1)
        //                             ->get();      

        // $data['blocks'] = [];
        // foreach($blocks as $block) {
        //     $data['blocks'][$block->position] = clone $block;
        //     if( $block->images()->where('is_main', 1)->first())
        //         $data['blocks'][$block->position]->image = $block->images()->where('is_main', 1)->first();
        // }


        if($slug)
            $data['page'] = Pages::where('slug', $slug)->first();

        if($data['page']){
            $data['image'] = $data['page']->images()->where('is_main', 1)->first(); 
            return view('pages/view', $data);
        }
        else
            return redirect('404')->send();
    }

    




}
