<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use App\Categories;
use App\Categories_images;
use App\Pages;
use App\Pages_images;
use App\ImageThumbnailer;
use Mail;
use Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  


        $data['categories'] =  Categories::select('*')
                                    ->where('categories.parent_id', 0)
                                    ->where('categories.active', 1)
                                    ->get();   

        $data['pages'] =  Pages::select('*')
                                    ->where('pages.active', 1)
                                    ->get();     


    
        $data['controller'] = 'index';

        return view('index', $data);
    }


    public function image($max=300, $thumb_link=null) {


        $link = urldecode(Input::get('link'));

        $linker = explode('.', $link);


        $thumb = $linker[0].'-'.$max.'._thumb.'.$linker[1];

        // set object 
        $img = new ImageThumbnailer; 

        $img->max_dimension = $max;


        if($max >= 900) {
            $img->compression_jpeg = 100;
            $img->compression_png = 3;
        }
        elseif($max < 900 && $max >= 400 ) {
            $img->compression_jpeg = 80;
            $img->compression_png = 3;
        }
        elseif($max < 400 && $max >= 100 ) {
            $img->compression_jpeg = 70;
            $img->compression_png = 3;
        }
        elseif($max < 100) {
            $img->compression_jpeg = 65;
            $img->compression_png = 3;
        }


        // if there isn't a thumb, make one and use it for future
        if(!file_exists($thumb)){
            $res = $img->draw($link, $thumb);
            $this->image($max, $thumb);
        }
        else
            $res =$img->draw($thumb);


        die;


    }

    public function manage()
    {   
        
        $data['controller'] = 'home';

        return view('manage/home', $data);
    }

    public function error404() {
        $data['controller'] = 'home';
        die('error 404. not found.');
    }



    public function register(){
        $this->layout = null;

            $email = Input::get('email');
            $username = Input::get('username');
            $password = Input::get('password');
            $ref_id = 0;

            if( Session::get('ref_id') > 0 )
                $ref_id = Session::get('ref_id');

            $emailCheck = User::where('email', $email)->first();
            $nameCheck = User::where('username', $username)->first();

            if( !isset($emailCheck->email) && !isset($nameCheck->username) ) {

                $user = new User;

                if($email != null && $password != null) {

                    $user->email = $email;
                    $user->username = $username;
                    $user->password = $password;
                    $user->ref_id = $ref_id;
                    $user->ip = $_SERVER['REMOTE_ADDR'];

                    if($user->save())
                            return 1;
                }
                else {
                    return "Please enter all the inputs to complete the register process!";
                }

            }
            else {
                if(isset($emailCheck->email))
                    return "There is an user with the same email in the database! Please use other email!";
                else 
                    return "There is an user with the same username in the database! Please use other username!";
            }

            return "Error. Please try again later!";


           
     }


    public function reset(){
        $this->layout = null;

            $email = Input::get('email');


            $emailCheck = User::where('email', $email)->first();

            if( isset($emailCheck->email) ) {

                $password = mb_substr( md5(rand(1, 10000)), 0, 6);

                $postdata =  array( 'password' => $password );

                // var_dump($password);
                // dd($postdata);

                $userUpdate = User::where('email', $email)->update($postdata);
                
                Mail::send('emails.reset', array('password' => $password), function($message)
                {
                    $message->to(Input::get('email'))->subject('Password reset for MSLP Form Applicatoin');
                });
                return 2;

            }

            return 1;


           
     }



    public function login(){
        $this->layout = null;



            $email = Input::get('email');
            $password =  Input::get('password');


            $emailPasswordCheck = User::where('email', $email)->first();
            if(!isset($emailPasswordCheck->password))
                $emailPasswordCheck = User::where('username', $email)->first();


            if( isset($emailPasswordCheck->password) && $password == $emailPasswordCheck->password ) {
                unset($emailPasswordCheck->password);
                if($emailPasswordCheck->is_admin == 0) {
                    unset($emailPasswordCheck->is_admin);
                }

                $refs = User::where('ref_id', $emailPasswordCheck->id)->get();

                if(isset($refs))
                    $emailPasswordCheck->refs = $refs;

                Session::put('loggedUser', $emailPasswordCheck);
                return json_encode($emailPasswordCheck);

            }

            else {
                return json_encode(1);
            }
           
     }

     public function logout(){
        $this->layout = null;

            Session::flush('loggedUser', null);

            return json_encode(1);
     }

     public function isUserLogged(){
        $this->layout = null;

        $loggedUser = Session::get('loggedUser');

        if($loggedUser != NULL){
            $loggedUserNewInfo = User::where('email', $loggedUser->email)->first();

            $refs = User::where('ref_id', $loggedUserNewInfo->id)->get();

            if(isset($refs))
                $loggedUserNewInfo->refs = $refs;
            return json_encode($loggedUserNewInfo);
        }
        return 1;

     }



}
