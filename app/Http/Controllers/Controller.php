<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Storage;
use App\Categories;
use App\Attributes;
use App\Vendors;
use App\Collections;
use App\Orders;


class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function uploadImage($path, $file) {


        // Get real extension according to mime type
        $ext                = $file->guessClientExtension();  

        // Client file name, including the extension of the client
        $fullname           = $file->getClientOriginalName(); 

        // Hash processed file name, including the real extension
        $hashname           = date('H.i.s').'-'.md5($fullname).'.'.$ext; 
        // $upload_success     = $temporary_image->move($destinationPath, $hashname);


        $store = Storage::disk('uploads')->put($path, $file);


        if($store){
            $uploaded_image = explode('/', $store);
            return $uploaded_image[1];
        }
        else{
            return false;
        }


    }



    public function listAllCategories() {


      return Categories::where('parent_id', 0)->get();



    }

    public function listAllCategoriesWithSubs() {


      return Categories::get();



    }


    public function listAllAttributes() {


      return Attributes::where('active', 1)->get();


    }

    public function listAllVendors() {


      return Vendors::get();

    }    

    public function listAllCollections() {


      return Collections::get();

    }


    public function listAllOrders() {


      return Orders::get();

    }


    public function newOrdersCount() {
    
      return count(Orders::where('status', 0)->get());

    }





}

