<?php

namespace App\Http\Controllers\Manage;

use Auth;
use Input; 
use Redirect;
use Validator;
use Response;
use Storage;
use User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Attributes as ItemModel;
use App\Attributes_values as ItemModel_values;

class AttributesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');

        $this->item = "attributes";
        $this->title = "Атрибути";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {



        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

         $data = array();
         $data['itemTitle'] = $this->title;
         $data['controller'] = $this->item;

        $data['meta'] = $this->title;
         
         return view('manage/'.$this->item.'/index', $data);
    }    

    public function add()
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;
        $data['controller'] = $this->item;

        if(Input::get('_token') != NULL) {
        
            $item = new ItemModel;
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = Input::get('slug');
            $item->active = Input::get('active') ? 1 : 0;
            
            if( $item->save() ) {
                $data['success'] = 'Записано успешно!';
                return Redirect::route($this->item)->send();
            }


        }

        // seo
        $data['meta'] = 'Добави';

        return view('manage/'.$this->item.'/add', $data);
    }

    public function edit($itemId)
    {
        
    if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

     $data = array();

         $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;

        $itemId = (int)$itemId;

        if(Input::get('_token') != NULL) {
        
            $item = ItemModel::find($itemId);
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = Input::get('slug');
            $item->active = Input::get('active') ? 1 : 0;
            
            if( $item->save() )
                $data['success'] = 'Записано успеншо!';

        }


        if($itemId > 0)
            $item = ItemModel::find($itemId);
        else
            return Redirect::route($this->item)->send();

        
        // seo
        $data['meta'] = 'Редакция на '.$item->title;
        $data['item'] = $item;


        return view('manage/'.$this->item.'/edit', $data);
    }  

    public function delete($itemId)
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;
        $itemId = (int)$itemId;

        if($itemId > 0) {
            $item = ItemModel::find($itemId);
            if( $item->delete() ) {
                $data['success'] = 'Записът е изтрит!';
                return Redirect::route($this->item)->send();
            }
        }
        else 
            return Redirect::route($this->item)->send();
    }  


    public function delete_value($valueId){ 
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $valueId = (int)$valueId;

        if( $valueId == 0 )
           return Response::json('error', 400);

       $image = ItemModel_values::find($valueId);


        if($valueId > 0) {
            $image = ItemModel_values::find($valueId);
            if( $image->delete() ) {
                return Response::json('success', 200);
            }
        }
        else 
            return Response::json('error', 400);


    }

    public function add_value($id=0)
    {
        if($id == 0)
            return Response::json('error', 400);

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $value = Input::get('input_value');



        $data['itemTitle'] = $this->title;
        $data['controller'] = $this->item;

        if(Input::get('_token') != NULL) {
        
            $item = new ItemModel_values;
            $item->item_id = $id;
            $item->value = $value;
            
            if( $item->save() ) {
                return Response::json('success', 200);
            }


        }

        return Response::json('error', 400);


    }
    //------------------------------------//

    public function list($isPaginated=0)
    {

        
        $pagination = ItemModel::paginate(10);

        foreach($pagination as $p)
            $p->values = $p->values;

        if($pagination) {
            header('Content-Type: application/json');
           return json_encode($pagination);
        }
        else {
            return FALSE;
        }


    }    

    public function listValues($id=0)
    {

        
        $values = ItemModel_values::where('item_id', $id)->orderBy('value', 'ASC')->get();

        if($values) {

            header('Content-Type: application/json');
           return json_encode( array( 'data' => $values ) ); 
        }
        else {
            return FALSE;
        }



    }   
}
