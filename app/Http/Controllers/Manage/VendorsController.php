<?php

namespace App\Http\Controllers\Manage;

use Auth;
use Input; 
use Redirect;
use Validator;
use Response;
use Storage;
use User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendors as ItemModel;
use App\Vendors_images as ItemModel_images;

class VendorsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');

        $this->item = "vendors";
        $this->title = "Производители";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

         $data = array();
         $data['itemTitle'] = $this->title;
         $data['controller'] = $this->item;
         $data['meta'] = $this->title;
         return view('manage/'.$this->item.'/index', $data);
    }    

    public function add()
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;
        $data['controller'] = $this->item;

        if(Input::get('_token') != NULL) {
        
            $item = new ItemModel;
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = Input::get('slug');

            if( $item->save() ) {
                $data['success'] = 'Записано успешно!';
                return Redirect::route($this->item)->send();
            }


        }

        // seo
        $data['meta'] = 'Добави';

        return view('manage/'.$this->item.'/add', $data);
    }

    public function edit($itemId)
    {
        
    if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

     $data = array();

         $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;


        $itemId = (int)$itemId;

        if(Input::get('_token') != NULL) {
        
            $item = ItemModel::find($itemId);
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = Input::get('slug');

            if( $item->save() )
                $data['success'] = 'Записано успешно!';

        }


        if($itemId > 0)
            $item = ItemModel::find($itemId);
        else
            return Redirect::route($this->item)->send();

        // seo
        $data['meta'] = 'Редакция на '.$item->title;
        $data['item'] = $item;


        return view('manage/'.$this->item.'/edit', $data);
    }  

    public function delete($itemId)
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;
        $itemId = (int)$itemId;

        if($itemId > 0) {
            $item = ItemModel::find($itemId);
            if( $item->delete() ) {
                $data['success'] = 'Записът е изтрит!';
                return Redirect::route($this->item)->send();
            }
        }
        else 
            return Redirect::route($this->item)->send();
    }  


    public function upload($itemId) {
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $itemId = (int)$itemId;
        
        if( $itemId == 0 )
           return Response::json('error', 400);

        $input = Input::all();

        $rules = array(
            'file' => 'image|max:5000',
        );


        $validation = Validator::make($input, $rules);

        if ($validation->fails())
        {
            return Response::make($validation->errors->first(), 400);
        }


        foreach(Input::file('images') as $temporary_image) {


            $hashname = $this->uploadImage($this->item, $temporary_image);
            if( $hashname != false ) {


                $image             = new ItemModel_images;
                $image->filename   = $hashname;
                $image->item_id = $itemId;
                $image->save();
            }
            else {
                 return Response::json('error', 400);
            }



   
        }

           return Response::json('success', 200);


    }  


    public function delete_image($imageId){ 
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $imageId = (int)$imageId;

        if( $imageId == 0 )
           return Response::json('error', 400);

       $image = ItemModel_images::find($imageId);

       

       $destinationPath    = public_path().'/uploads/'.$this->item.'/'.$image->filename;
   
        unlink($destinationPath); 

        if($imageId > 0) {
            $image = ItemModel_Images::find($imageId);
            if( $image->delete() ) {
                return Response::json('success', 200);
            }
        }
        else 
            return Response::json('error', 400);
      




    }

    public function set_main_image($itemId, $imageId){ 
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();
        $imageId = (int)$imageId;
        $itemId = (int)$itemId;

        if( $imageId == 0 || $itemId == 0 )
           return Response::json('error', 400);

        
        $updateAllImages = ItemModel_Images::where('item_id', $itemId)->update(['is_main' => 0]);

        $image = ItemModel_Images::find($imageId);
        $image->is_main = 1;


        if( $image->save() ) {
                return Response::json('success', 200);

        }
        else 
            return Response::json('error', 400);
      

    }

    //------------------------------------//

    public function list()
    {

        $pagination = ItemModel::paginate(10);
        
        foreach($pagination as $p) 
            $p->productsCount = $p->products->count() ? $p->products->count() : 0;
      

        if($pagination) {
            header('Content-Type: application/json');
           return json_encode($pagination);
        }
        else {
            return FALSE;
        }


    }    

    public function listImages($id=0)
    {

        
        $images = ItemModel_images::where('item_id', $id)->orderBy('id', 'DESC')->get();

        if($images) {

            foreach($images as $image)
                $image->fullpath = url('/').'/image/300/?link=public/uploads/'.$this->item.'/'.$image->filename;

            header('Content-Type: application/json');
           return json_encode( array( 'data' => $images ) ); 
        }
        else {
            return FALSE;
        }



    }   
}
