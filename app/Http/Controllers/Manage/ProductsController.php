<?php

namespace App\Http\Controllers\Manage;

use Auth;
use Input; 
use Redirect;
use Validator;
use Response;
use Storage;
use User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Products as ItemModel;
use App\Products_images as ItemModel_images;
use App\Products_attributes_values as ItemModel_attributes_values;
use App\Products_tags as ItemModel_tags;

class ProductsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');

        $this->item = "products";
        $this->title = "Продукти";
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

         $data = array();
         $data['itemTitle'] = $this->title;
         $data['controller'] = $this->item;
         $data['meta'] = $this->title;
         return view('manage/'.$this->item.'/index', $data);
    }    

    public function add()
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;
        $data['controller'] = $this->item;

        $data['categories'] = $this->listAllCategories();
        $data['vendors'] = $this->listAllVendors();
        $data['collections'] = $this->listAllCollections();

        if(Input::get('_token') != NULL) {

            $slug = Input::get('slug') ? Input::get('slug') : 'no-slug';
            $slugCount = ItemModel::where('slug', $slug)->count();

            if($slugCount > 0)
                $slug .= "-new".rand(1,9999);

            $item = new ItemModel;
            $item->category_id = Input::get('category_id');
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = $slug;
            $item->price = Input::get('price');
            $item->discount = Input::get('discount') ? Input::get('discount') : 0;
            $item->is_vat = Input::get('is_vat') ? 1 : 0;
            $item->intro = Input::get('intro');
            $item->vendor_id = Input::get('vendor_id');
            $item->sku = Input::get('sku');
            $item->barcode = Input::get('barcode');
            $item->quantity = Input::get('quantity') ? Input::get('quantity') : 0;
            $item->weight = Input::get('weight') ? Input::get('weight') : 0;
            $item->collection_id = Input::get('collection_id') ? Input::get('collection_id') : 0;
            $item->active = Input::get('active') ? 1 : 0;

            if( $item->save() ) {
                $data['success'] = 'Записано успешно!';


                $tags = explode(',', Input::get('tags'));
                if( is_array($tags) ) {
                    foreach($tags as $tag) {
                        $add_tag = new ItemModel_tags;
                        $add_tag->item_id  = $item->id;
                        $add_tag->tag = trim($tag);
                        $add_tag->save();

                        return Redirect::to('manage/'.$this->item.'/edit/'.$item->id)->send();

                    }
                }
                return Redirect::route($this->item)->send();
            }


        }

        // seo
        $data['meta'] = 'Добави';

        return view('manage/'.$this->item.'/add', $data);
    }

    public function edit($itemId)
    {
        
    if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

     $data = array();

        $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;

        $data['categories'] = $this->listAllCategories();
        // $data['attributes'] = $this->listAllAttributes();
        $data['vendors'] = $this->listAllVendors();
        $data['collections'] = $this->listAllCollections();


        $itemId = (int)$itemId;

        $item = ItemModel::find($itemId);

        if(isset($item->category->attributes))
            $data['attributes'] = $item->category->attributes;


        
        if(Input::get('_token') != NULL) {
 
            $slug = Input::get('slug') ? Input::get('slug') : 'no-slug';
            $slugCount = ItemModel::where('slug', $slug)->count();

            if($slugCount > 0 && $item->slug != $slug)
                $slug .= "-".$item->id;

            $item->category_id = Input::get('category_id');
            $item->title = Input::get('title');
            $item->content = Input::get('content');
            $item->slug = $slug;
            $item->price = Input::get('price');
            $item->discount = Input::get('discount') ? Input::get('discount') : 0;
            $item->is_vat = Input::get('is_vat') ? 1 : 0;
            $item->intro = Input::get('intro');
            $item->vendor_id = Input::get('vendor_id');
            $item->sku = Input::get('sku');
            $item->barcode = Input::get('barcode');
            $item->quantity = Input::get('quantity') ? Input::get('quantity') : 0;
            $item->weight = Input::get('weight') ? Input::get('weight') : 0;
            $item->collection_id = Input::get('collection_id') ? Input::get('collection_id') : 0;
            $item->active = Input::get('active') ? 1 : 0;
            
            $delete_attributes = ItemModel_attributes_values::where('product_id', $itemId)->delete();

            // reassign attributes
            if(Input::get('attributes_values')) {
            foreach(Input::get('attributes_values') as $att) {
                    $attribute = new ItemModel_attributes_values;
                    $attribute->product_id  = $itemId;
                    $attribute->attribute_id = $att;
                    $attribute->save();

                }
            }

            // deleting tags
            $delete_tags = ItemModel_tags::where('item_id', $itemId)->delete();

            // reassign tags
            $tags = explode(',', Input::get('tags'));
            if( is_array($tags) ) {
            foreach($tags as $tag) {
                    $add_tag = new ItemModel_tags;
                    $add_tag->item_id  = $itemId;
                    $add_tag->tag = trim($tag);
                    $add_tag->save();

                }
            }

            if( $item->save() )
                $data['success'] = 'Записано успешно!';


        }


        if($itemId > 0)
            $item = ItemModel::find($itemId);
        else
            return Redirect::route($this->item)->send();

        $data['item_attributes'] = [];
        if( isset($item->attributes) ) {
            foreach($item->attributes as $att) 
                $data['item_attributes'][] = $att->id;
        }

        $tags = [];
        if( isset($item->tags) ) {
            foreach($item->tags as $tag)
                $tags[] = trim($tag->tag);
        }


        if(is_array($tags))
            $item->tagsComma = implode(', ', $tags);
        else 
            $item->tagsComma = null;

        // seo
        $data['meta'] = 'Редакция на '.$item->title;
        $data['item'] = $item;


        return view('manage/'.$this->item.'/edit', $data);
    }  

    public function delete($itemId)
    {

        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $data = array();

        $data['itemTitle'] = $this->title;

        $data['controller'] = $this->item;
        $itemId = (int)$itemId;

        if($itemId > 0) {
            $item = ItemModel::find($itemId);
            if( $item->delete() ) {
                $data['success'] = 'Записът е изтрит!';
                return Redirect::route($this->item)->send();
            }
        }
        else 
            return Redirect::route($this->item)->send();
    }  


    public function upload($itemId) {
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $itemId = (int)$itemId;
        
        if( $itemId == 0 )
           return Response::json('error', 400);

        $input = Input::all();

        $rules = array(
            'file' => 'image|max:5000',
        );


        $validation = Validator::make($input, $rules);

        if ($validation->fails())
        {
            return Response::make($validation->errors->first(), 400);
        }


        foreach(Input::file('images') as $temporary_image) {


            $hashname = $this->uploadImage($this->item, $temporary_image);
            if( $hashname != false ) {


                $image             = new ItemModel_images;
                $image->filename   = $hashname;
                $image->item_id = $itemId;
                $image->save();
            }
            else {
                 return Response::json('error', 400);
            }



   
        }

           return Response::json('success', 200);


    }  


    public function delete_image($imageId){ 
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();

        $imageId = (int)$imageId;

        if( $imageId == 0 )
           return Response::json('error', 400);

       $image = ItemModel_images::find($imageId);

       

       $destinationPath    = public_path().'/uploads/'.$this->item.'/'.$image->filename;
   
        unlink($destinationPath); 

        if($imageId > 0) {
            $image = ItemModel_Images::find($imageId);
            if( $image->delete() ) {
                return Response::json('success', 200);
            }
        }
        else 
            return Response::json('error', 400);
      




    }

    public function set_main_image($itemId, $imageId){ 
        if (Auth::user()->role != 'Administrator')
            return Redirect::route('shop')->send();
        $imageId = (int)$imageId;
        $itemId = (int)$itemId;

        if( $imageId == 0 || $itemId == 0 )
           return Response::json('error', 400);

        
        $updateAllImages = ItemModel_Images::where('item_id', $itemId)->update(['is_main' => 0]);

        $image = ItemModel_Images::find($imageId);
        $image->is_main = 1;


        if( $image->save() ) {
                return Response::json('success', 200);

        }
        else 
            return Response::json('error', 400);
      

    }

    //------------------------------------//

    public function list()
    {
        
        $pagination = ItemModel::paginate(10);

        if(Input::get('search')){
            $str = Input::get('search');
            $pagination = ItemModel::where('id', 'LIKE', "%$str%")
                                   ->orWhere('sku', 'LIKE', "%$str%")
                                   ->orWhere('barcode', 'LIKE', "%$str%")
                                   ->orWhere('title', 'LIKE', "%$str%")
                                   ->orWhere('content', 'LIKE', "%$str%")
                                   ->orWhere('intro', 'LIKE', "%$str%")
                                   ->paginate(10);
        }

        foreach($pagination as $p){
            if(isset($p->category->title))
                $p->categoryTitle = $p->category->title;

            $p->tags = $p->tags;

        }

        if($pagination) {
            header('Content-Type: application/json');
           return json_encode($pagination);
        }
        else {
            return FALSE;
        }


    }    

    public function listImages($id=0)
    {

        
        $images = ItemModel_images::where('item_id', $id)->orderBy('id', 'DESC')->get();

        if($images) {

            foreach($images as $image)
                $image->fullpath = url('/').'/image/300/?link=public/uploads/'.$this->item.'/'.$image->filename;

            header('Content-Type: application/json');
           return json_encode( array( 'data' => $images ) ); 
        }
        else {
            return FALSE;
        }



    }   
}
