<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    public function images()
    {
        return $this->hasMany('App\Products_images', 'item_id');
    }

    public function tags()
    {
        return $this->hasMany('App\Products_tags', 'item_id');
    }

     public function category()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }

    public function attributes()
    {
        return $this->belongsToMany('App\Attributes_values', 'products_attributes_values', 'product_id', 'attribute_id');
    }



    // public function attributes_group()
    // {
    //     return $this->belongsToMany('App\Attributes', 'attributes_values', 'id', 'item_id');
    // }


    // public function attributes_group()
    // {
    //     return $this->belongsToMany('App\Attributes', 'products_attributes_values', 'product_id', 'attribute_id');
    // }

}
