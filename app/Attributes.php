<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attributes extends Model
{
   
    public function values()
    {
        return $this->hasMany('App\Attributes_values', 'item_id');
    }


    public function categories()
    {
        return $this->belongsToMany('App\Categories', 'categories_attributes', 'category_id', 'attribute_id');
    }
}
