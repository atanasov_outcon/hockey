<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Orders;
use App\Categories;
use App\Categories_images;
use View;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
\URL::forceRootUrl(\Config::get('app.url'));    
// And this if you wanna handle https URL scheme
// It's not usefull for http://www.example.com, it's just to make it more independant from the constant value
if (str_contains(\Config::get('app.url'), 'https://')) {
    \URL::forceScheme('https');
    //use \URL:forceSchema('https') if you use laravel < 5.4
}
        
  // Set up global user object for views


         View::share('newOrders', count(Orders::where('status', 0)->get()) );
         View::share('categories', count(Categories::where('active', 1)->get()) );


    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
