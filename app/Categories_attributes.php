<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories_attributes extends Model
{
     public function attribute()
    {
        return $this->belongsTo('App\Attributes', 'attribute_id');
    }

     public function category()
    {
        return $this->belongsTo('App\Categories', 'category_id');
    }

}
