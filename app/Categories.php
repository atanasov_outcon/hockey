<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
   


    public function subcategories()
    {
        return $this->hasMany('App\Categories', 'parent_id');
    }
    
    public function parent()
    {
        return $this->belongsTo('App\Categories', 'parent_id');
    }

    public function attributes()
    {
        return $this->belongsToMany('App\Attributes', 'categories_attributes', 'category_id', 'attribute_id');
    }

    public function images()
    {
        return $this->hasMany('App\Categories_images', 'item_id');
    }


    public function products()
    {
        return $this->hasMany('App\Products', 'category_id');
    }
}



