<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_info extends Model
{

	// protected $table = 'оrders_info';

    public function city()
    {
        return $this->belongTo('App\Cities', 'city_id');
    }

      public function order()
    {
        return $this->belongsTo('App\Orders', 'item_id');
    }

}
