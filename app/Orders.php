<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function info()
    {
        return $this->hasOne('App\Orders_info', 'item_id');
    }


    public function products()
    {
        return $this->belongsToMany('App\Products', 'orders_products', 'item_id', 'product_id');
    }
}
