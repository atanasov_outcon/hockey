<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors extends Model
{
 
    public function products()
    {
        return $this->hasMany('App\Products', 'vendor_id');
    }

    public function images()
    {
        return $this->hasMany('App\Vendors_Images', 'item_id');
    }
}
