<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories_images extends Model
{
     public function category()
    {
        return $this->belongsTo('App\Categories', 'item_id');
    }

}
