<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pages_images extends Model
{
     public function page()
    {
        return $this->belongsTo('App\Pages', 'item_id');
    }

}
