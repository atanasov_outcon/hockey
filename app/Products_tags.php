<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products_tags extends Model
{
     public function product()
    {
        return $this->belongsTo('App\Products', 'item_id');
    }

}
