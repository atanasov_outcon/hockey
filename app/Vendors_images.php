<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendors_images extends Model
{
     public function vendor()
    {
        return $this->belongsTo('App\Vendors', 'item_id');
    }

}
