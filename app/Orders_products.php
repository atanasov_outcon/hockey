<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders_products extends Model
{
     public function product()
    {
        return $this->belongsTo('App\Products', 'product_id');
    }

      public function order()
    {
        return $this->belongsTo('App\Orders', 'item_id');
    }

}
