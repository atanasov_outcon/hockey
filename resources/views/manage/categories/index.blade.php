@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">{{ $itemTitle }}
                        <a href="{{ url('manage/'.$controller.'/add') }}" class="btn btn-primary btn-xs pull-right hidden-xs"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination mx-auto">
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.prev_item_url)"
                                :disabled="!pagination.prev_item_url">
                            <i class="fa fa-caret-left"></i>
                        </button>
                        <span>Страница @{{pagination.current_item}} от @{{pagination.last_item}}</span>
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.next_item_url)"
                                :disabled="!pagination.next_item_url"><i class="fa fa-caret-right"></i>
                        </button>
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th class="hidden-xs">#</th>
                            <th>Име</th>
                            <th ><i class="fa fa-th-list fa-fw"></i><span class="visible-lg-inline visible-md-inline"> Продукти</span></th>
                            <th class="hidden-xs">Подкатегории</th>
                            <th class="text-center"><i class="fa fa-eye fa-lg fa-fw"></i><span class="visible-lg-inline visible-md-inline"> Статус</span></th>
                            <th class="text-center"><i class="fa fa-cog fa-lg fa-rw"></i> <span class="hidden-xs"> Действия</span></th>
                        </tr>
                        <tr v-for="item in items" is="item" :item="item"></tr>
                    </table>

                    <template id="template-item">
                        <tr>
                            <th class="hidden-xs">
                                @{{item.id}}
                            </td>
                            <td>
                                <strong>@{{item.title}}</strong>
                            </td>
                            <td>
                                <span class="visible-lg-inline visible-md-inline">брой </span><span v-bind:class="[ ( item.productsCount > 0 ) ? 'label-primary' : 'label-danger', 'label' ]">@{{item.productsCount}}</span>
                            </td>
                            <td class="hidden-xs">
                                <span v-if="item.attributes != ''">Атрибути:</span> <span  v-for="attribute in item.attributes" class="badge" >@{{ attribute.title }}</span>
                            </td>
                            <td class="text-center">
                                <span v-bind:class="[ ( item.active == 1 ) ? 'label-success' : 'label-danger', 'label' ]">
                                    &nbsp;
                                    <span class="hidden-xs hidden-sm">активен</span>
                                    &nbsp;
                                </span>
                            </td>                           
                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{ url('/manage') }}/{{ $controller }}/edit/@{{item.id}}">
                                   <i class="fa fa-pencil fa-lg fa-fw"></i><span class="hidden-xs"> Редактирай</span>
                                </a>                                
                                <a class="btn btn-danger btn-xs" href="{{ url('/manage') }}/{{ $controller }}/delete/@{{item.id}}" onclick="return confirm('ИЗТРИВАНЕ на този запис?')">
                                   <i class="fa fa-trash-o fa-lg fa-fw"></i><span class="hidden-xs"> Изтрий</span>
                                </a>
                            </td>
                        </tr>

                        <tr v-for="child in item.childs" >
                            <td class="hidden-xs">
                                @{{child.id}}
                            </td>
                            <td>
                               <i class="fa fa-long-arrow-right"></i> @{{child.title}}
                            </td>
                             <td>
                                <span class="visible-lg-inline visible-md-inline">брой </span><span v-bind:class="[ ( child.productsCount > 0 ) ? 'label-primary' : 'label-danger', 'label' ]">@{{child.productsCount}}</span>
                            </td>
                             <td class="hidden-xs">
                                <span  v-for="subchild in child.childs" >
                                    <a class="" href="{{ url('/manage') }}/{{ $controller }}/edit/@{{subchild.id}}">
                                        @{{ subchild.title }}</a> <a class="btn btn-primary btn-xs" href="{{ url('/manage') }}/{{ $controller }}/edit/@{{subchild.id}}"><i class="fa fa-pencil fa-lg fa-fw"></i></a>

                                    <a class="btn btn-danger btn-xs" href="{{ url('/manage') }}/{{ $controller }}/delete/@{{subchild.id}}" onclick="return confirm('ИЗТРИВАНЕ на този запис?')">
                                       <i class="fa fa-trash-o fa-lg fa-fw"></i><span class="hidden-xs"></span>
                                    </a> <br>
                                </span> 
                            </td>
                             <td class="text-center">
                                <span v-bind:class="[ ( child.active == 1 ) ? 'label-success' : 'label-danger', 'label' ]">
                                    &nbsp;
                                    <span class="hidden-xs hidden-sm">активен</span>
                                    &nbsp;
                                </span>
                            </td>                           
                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{ url('/manage') }}/{{ $controller }}/edit/@{{child.id}}">
                                   <i class="fa fa-pencil fa-lg fa-fw"></i><span class="hidden-xs"> Редактирай</span>
                                </a>                                
                                <a class="btn btn-danger btn-xs" href="{{ url('/manage') }}/{{ $controller }}/delete/@{{child.id}}" onclick="return confirm('ИЗТРИВАНЕ на този запис?')">
                                   <i class="fa fa-trash-o fa-lg fa-fw"></i><span class="hidden-xs"> Изтрий</span>
                                </a>
                            </td>


                        </tr>


                    </template>

                    <div class="pagination mx-auto">
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.prev_item_url)"
                                :disabled="!pagination.prev_item_url">
                            <i class="fa fa-caret-left"></i>
                        </button>
                        <span>Страница @{{pagination.current_item}} от @{{pagination.last_item}}</span>
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.next_item_url)"
                                :disabled="!pagination.next_item_url"><i class="fa fa-caret-right"></i>
                        </button>
                    </div>
                    <br> 
                <a href="{{ url('manage/'.$controller.'/add') }}" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('item', {
                        template: '#template-item',
                        props: ['item'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            items: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchItems()
                        },
                        methods: {
                            fetchItems: function(item_url) {
                                let vm = this;
                                item_url =  item_url || '{{ url("/api/".$controller) }}/1'
                                this.$http.get(item_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.items = response.data.data;
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_item: data.current_page,
                                    last_item: data.last_page,
                                    next_item_url: data.next_page_url,
                                    prev_item_url: data.prev_page_url
                                }
                                this.pagination = pagination
                            }
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
@endsection
