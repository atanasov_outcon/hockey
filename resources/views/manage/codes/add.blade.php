@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                  <form method="POST">
                <div class="panel-heading clearfix">Добавяне на нов запис 
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                </div>

                <div class="panel-body">
                   
                    
                     {{ csrf_field() }}

                      @if ( isset($error) )
                          <div class="alert alert-danger">
                            {{ $error }}
                          </div>
                      @endif                    

                      @if ( isset($success) )
                          <div class="alert alert-success">
                            {{ $success }}
                          </div>
                      @endif

                      <div class="form-group">
                        <label for="code">Код <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="code" id="code" placeholder="Код" value="">
                      </div>      
                      <div class="form-group">
                        <label for="discount">Намаление <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i> <small class="text-primary">цената е в проценти</small></label>
                        <input required type="text" class="form-control" name="discount" id="discount" placeholder="Намаление" value="">
                      </div>      
                 
  <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="1" > Активен
                        </label>
                      </div>

                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>

                </div></form>
            </div>
        </div>        

    </div>



@endsection
