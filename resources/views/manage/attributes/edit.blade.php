@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 "> 
            <div class="panel panel-default">
                <form method="POST">
                    <div class="panel-heading clearfix">Редакция на {{ $item->title }}
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                    </div>

                <div class="panel-body">
                   
                    
                     {{ csrf_field() }}

                      @if ( isset($error) )
                          <div class="alert alert-danger">
                            {{ $error }}
                          </div>
                      @endif                    

                      @if ( isset($success) )
                          <div class="alert alert-success">
                            {{ $success }}
                          </div>
                      @endif

                      <div class="form-group">
                        <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="<?=(isset($item->title)) ? $item->title : null;?>">
                      </div>  

                      <div class="form-group">
                        <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="<?=(isset($item->slug)) ? $item->slug : null;?>">
                      </div>                  

                      <div class="form-group">
                        <label for="content">Описание <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <textarea required name="content" id="content_" class="form-control" cols="20" rows="5"><?=(isset($item->content)) ? $item->content : null;?></textarea>
                      </div>       
  <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="<?=(isset($item->active)) ? 1 : 0;?>" <?php if(isset($item->active) && $item->active == true) echo 'checked'; ?> > Активен
                        </label>
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>

                </div></form>
            </div>
        </div>  


    <div class="col-md-4"> 
            <div class="panel panel-default">
                <div class="panel-heading">Добави стойности на атрибута</div>
                <div class="panel-body container-vue">
             
             <style>
            
             </style>       

                     {{ csrf_field() }}

                        <div class="row">
                            <div class="col-md-8">
                                <input v-on:keyup.enter="saveValue()" type="text" v-model="input_value" class="form-control"><br>
                            </div>
                            <div class="col-md-4">
                                <button  v-on:click="saveValue()" class="btn btn-primary">Запиши</button>
                            </div>
                        </div>

                    
                   
                    <hr />

                    <div class="row ">

                            <div class="col-md-12 hover border-bot" v-for="(index, item) in attribute_values"  >
                                  @{{ item.value }}
                                  <a v-on:click="deleteValue(item.id)" href="#" title="Delete" class="btn-item-delete tip" data-dismiss="alert">
                                    <span><i class="fa fa-trash pull-right"></i></span>
                                  </a>
                            </div>

                    </div>
                    <div class="row">
                      <hr>
                      <div class="container">
                        <small><i class="fa fa-trash"></i> - изтрий</small> <br>
                      </div>


                    </div>


                   

                </div>
            </div>
        </div>
    </div>  



<script type="text/javascript">
var token = Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');


const gallery = new Vue({
    el: '.container-vue',
    data: {
        input_value: null,
        attribute_values: []
    },
    ready: function () {
        this.fetchValues()
    },
    methods: {
        fetchValues: function () {
            let vm = this;
            item_url =  "{{ url('/api/'.$controller.'/values/'.$item->id) }}"
            this.$http.get(item_url)
                .then(function (response) {
                    vm.$set('attribute_values', response.data.data)
                });
        },  
        saveValue: function () {
            postData = {
              '_token'      : token,
              'input_value' : this.input_value
            }
            let vm = this;
            item_url =  "{{ url('/manage/'.$controller.'/add_value/'.$item->id) }}"
            this.$http.post(item_url, postData)
                .then(function (response) {
                   vm.fetchValues();
                   this.input_value = null;
                });
        },             
        deleteValue: function (item_id) {

          var doIt = confirm("ИЗТРИЙ този запис?");
          if (doIt == true) {
            let vm = this;
            item_url = "{{ url('/manage/'.$controller.'/delete_value/') }}/"+item_id
            this.$http.get(item_url)
                .then(function (response) {
                    vm.fetchValues();
                });
          }


        }
    }
});   

</script>

@endsection
