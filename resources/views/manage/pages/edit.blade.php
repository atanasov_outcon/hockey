@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-8 "> 
            <div class="panel panel-default">
                <form method="POST">
                    <div class="panel-heading clearfix">Редакция на {{ $item->title }}
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                    </div>
                <div class="panel-body">
                   
                    
                     {{ csrf_field() }}

                      @if ( isset($error) )
                          <div class="alert alert-danger">
                            {{ $error }}
                          </div>
                      @endif                    

                      @if ( isset($success) )
                          <div class="alert alert-success">
                            {{ $success }}
                          </div>
                      @endif

                      <div class="form-group">
                        <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="<?=(isset($item->title)) ? $item->title : null;?>">
                      </div>  

                      <div class="form-group">
                        <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="<?=(isset($item->slug)) ? $item->slug : null;?>">
                      </div>   

                      <div class="form-group">
                        <label for="content">Описание <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <textarea required name="content" id="content" class="form-control" cols="20" rows="5"><?=(isset($item->content)) ? $item->content : null;?></textarea>
                      </div>         

   <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="<?=(isset($item->active)) ? 1 : 0;?>" <?php if(isset($item->active) && $item->active == true) echo 'checked'; ?> > Активен
                        </label>
                      </div>

  <hr>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                     
                </div></form>
            </div>
        </div>        

        <div class="col-md-4"> 
            <div class="panel panel-default">
                <div class="panel-heading">Галерия</div>
                <div class="panel-body container-vue">
             
             <style>
            
             </style>       



                    <form id="ajax-upload" method="POST" enctype="multipart/form-data">
                     {{ csrf_field() }}


                    <div class="alert alert-success alert-upload-success alert-hidden">
                           Картинките са качени успешно!
                    </div>                    

                    <div class="alert alert-danger alert-upload-error alert-hidden">
                          Моля пробвай отново!
                    </div>
                

                      <div class="form-group">
                      <div class="spinLoader alert-hidden pull-right">качване <i class="fa fa-spinner fa-pulse fa-fw "></i> <span class="sr-only">качване...</span></div>
                        <label for="images">Добави изображения</label>
                        <input type="file" class="form-control" name="images[]" id="images" multiple>
                      </div>                  

                    </form>
                    
                   
                    <hr />

                    <div class="row ">

                            <div class="col-md-6 thumb" v-for="(index, image) in images" >
                                <span class="main-image" v-if="image.is_main==1" href="">главна снимка</span>

                                <div class="col-md-12 controls-bottom">
                                    <a v-on:click="setMainImage(image.id)" href="#" title="Set main image" class="btn-image-add tip" data-dismiss="alert">
                                    <span><i class="fa fa-image"></i></span>
                                    </a>  
                                  
                                  <a v-on:click="deleteImage(image.id)" href="#" title="Delete" class="btn-image-delete tip" data-dismiss="alert">
                                    <span><i class="fa fa-trash"></i></span>
                                  </a>

                  
                                </div>                          
                                  <img :src="image.fullpath" class="img-responsive">

                                  <div v-if="index%2!=0" class="clearfix">&nbsp;</div>
                            </div>

                      <div class="clearfix"></div>
                      <hr>
                      <div class="container">
                        <small><i class="fa fa-image"></i> - направи главна картинка</small> <br>
                        <small><i class="fa fa-trash"></i> - изтрий</small> <br>
                      </div>


                    </div>


                   

                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})




const gallery = new Vue({
    el: '.container-vue',
    data: {
        images: []
    },
    ready: function () {
        this.fetchUploads()
    },
    methods: {
        fetchUploads: function () {
            let vm = this;
            item_url =  "{{ url('/api/'.$controller.'/uploads/'.$item->id) }}"
            this.$http.get(item_url)
                .then(function (response) {
                    vm.$set('images', response.data.data)
                });
        },             
        deleteImage: function (image_id) {

          var doIt = confirm("Изтрий това изображение?");
          if (doIt == true) {
            let vm = this;
            item_url = "{{ url('/manage/'.$controller.'/delete_image/') }}/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
          }


        },      
        setMainImage: function (image_id) {
            let vm = this;
            item_url = "{{ url('/manage/'.$controller.'/set_main_image/'.$item->id) }}/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
        },           
        addToContent: function (img_url) {
            editor.summernote("insertImage", img_url);
        },
    }
});   

</script>


<script type="text/javascript">

$(document).ready(function() {

  $( "#ajax-upload" ).on('change', function(e) {
    e.preventDefault();
    $('.spinLoader').show();
    $.ajax({
        method: 'post',
        url: '{{ url("manage/".$controller."/upload/".$item->id) }}',
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
          $('.spinLoader').hide();
            $('.alert-upload-success').show().fadeOut(2000);
            $('#images').val('');
            gallery.fetchUploads(); // re fetch the images
        },
        error: function(data){
          $('.spinLoader').hide();
            $('.alert-upload-error').show().fadeOut(2000);
        },

    });

});




});

</script>
@endsection
