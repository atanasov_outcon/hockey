@extends('layouts.app')

@section('content')
    <div class="row">
       <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix">{{ $itemTitle }}
                        <!-- <a href="{{ url('manage/'.$controller.'/add') }}" class="btn btn-primary btn-xs pull-right hidden-xs"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a> -->
                </div>

                <div class="panel-body">
                
                <div class="container-vue">

                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <div class="form-group">
                                <input v-model="searchStr" v-on:keyup="search()" type="text" class="form-control input-sm" placeholder="търсене" >
                            </div>
                        </div>

                        <div  v-show="!searchStr" class="col-sm-10 col-xs-12 text-primary">
                            търсене по id, име, емейл, телефон
                        </div>                        
                        <div  v-show="searchStr && searchStr.length > 0 && searchCount > 0" class="col-sm-10 col-xs-12">
                            <span class="text-success">@{{ searchCount }}</span> резултата за <i>@{{ searchStr }}</i>
                        </div>
                        <div  v-show="searchStr && searchStr.length > 0 && searchCount == 0" class="col-sm-10 col-xs-12">
                         <span class="text-danger">няма</span> резултати за <i>@{{ searchStr }}</i>
                        </div>
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th class="hidden-xs">#</th>
                            <th>Име</th>
                            <th class="hidden-xs"><i class="fa fa-envelope-o fa-lg fa-rw"></i> <span class="hidden-xs"> Е-мейл</span></th>
                            <th><i class="fa fa-mobile fa-lg fa-rw"></i> <span class="hidden-xs hidden-sm"> Телефон</span></th>
                            <th><i class="fa fa-truck fa-lg fa-rw"></i> <span class="hidden-xs hidden-sm"> Поръчки</span></th>
                            <th class="hidden-xs"><i class="fa fa-calendar fa-lg fa-fw"></i><span class="visible-lg-inline visible-md-inline"> Регистриран на</span></th>
                            <th class="text-center"><i class="fa fa-eye fa-lg fa-fw"></i><span class="visible-lg-inline visible-md-inline"> Статус</span></th>
                            <th class="text-center"><i class="fa fa-cog fa-lg fa-rw"></i> <span class="hidden-xs"> Действия</span></th>
                        </tr>
                        <tr v-for="item in items" is="item" :item="item"></tr>
                    </table>

                    <template id="template-item">
                        <tr>
                            <th class="hidden-xs">
                                @{{item.id}}
                            </td>
                            <td>
                                <span class="label label-danger">&nbsp;<i class="fa fa-user-secret fa-lg fa-fw"></i><i class="hidden-xs hidden-sm">admin</i>&nbsp;</span> @{{item.name}} 
                            </td>
                            <td class="hidden-xs">
                                @{{item.email}}
                            </td>
                            <td>
                                @{{item.phone}}
                            </td>
                            <td>
                                <span v-bind:class="[ ( item.ordersCount > 0 ) ? 'label-primary' : 'label-danger', 'label' ]">@{{item.ordersCount}}</span>
                            </td>
                            <td class="hidden-xs">
                                @{{ item.created_at }} 
                            </td> 
                            <td class="text-center">
                                <span v-bind:class="[ ( item.active == 1 ) ? 'label-success' : 'label-danger', 'label' ]">
                                    &nbsp;
                                    <span class="hidden-xs hidden-sm">активен</span>
                                    &nbsp;
                                </span>
                            </td>  
                            <td class="text-center">
                                <a class="btn btn-primary btn-xs" href="{{ url('/manage') }}/{{ $controller }}/edit/@{{item.id}}">
                                   <i class="fa fa-pencil fa-lg fa-fw"></i><span class="hidden-xs hidden-sm hidden-md"> Редактирай</span>
                                </a>                                
                                <a class="btn btn-danger btn-xs" href="{{ url('/manage') }}/{{ $controller }}/delete/@{{item.id}}" onclick="return confirm('ИЗТРИВАНЕ на този запис?')">
                                   <i class="fa fa-trash-o fa-lg fa-fw"></i><span class="hidden-xs hidden-sm hidden-md"> Изтрий</span>
                                </a>
                            </td>
                        </tr>

                    </template>

                    <div class="pagination mx-auto">
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.prev_item_url)"
                                :disabled="!pagination.prev_item_url">
                            <i class="fa fa-caret-left"></i>
                        </button>
                        <span>Страница @{{pagination.current_item}} от @{{pagination.last_item}}</span>
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.next_item_url)"
                                :disabled="!pagination.next_item_url"><i class="fa fa-caret-right"></i>
                        </button>
                    </div>
                    <br> 
                <!-- <a href="{{ url('manage/'.$controller.'/add') }}" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a> -->
                </div>

                    <script type="text/javascript">
                    Vue.component('item', {
                        template: '#template-item',
                        props: ['item'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            items: [],
                            pagination: {},
                            searchStr: null
                        },
                        ready: function () {
                            this.fetchItems()
                        },
                        computed: {
                            searchCount: function(){
                                if(this.searchStr != null && this.searchStr.length > 0) {
                                    return this.items.length;
                                }
                            }
                        },
                        methods: {
                            search: function(){
                                let vm = this;
                                vm.fetchItems('{{ url("/api/".$controller) }}/?search='+vm.searchStr)
                            },
                            fetchItems: function(item_url) {
                                let vm = this;
                                item_url =  item_url || '{{ url("/api/".$controller) }}'
                                this.$http.get(item_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.items = response.data.data
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_item: data.current_page,
                                    last_item: data.last_page,
                                    next_item_url: data.next_page_url,
                                    prev_item_url: data.prev_page_url
                                }
                                this.pagination = pagination
                            }
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
@endsection
