@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                <form method="POST">
                    <div class="panel-heading clearfix">Редакция на {{ $item->title }}
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                    </div>

                <div class="panel-body">
                   
                    
                     {{ csrf_field() }}

                      @if ( isset($error) )
                          <div class="alert alert-danger">
                            {{ $error }}
                          </div>
                      @endif                    

                      @if ( isset($success) )
                          <div class="alert alert-success">
                            {{ $success }}
                          </div>
                      @endif

                      <div class="form-group">
                        <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="<?=(isset($item->title)) ? $item->title : null;?>">
                      </div>  

                      <div class="form-group">
                        <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="<?=(isset($item->slug)) ? $item->slug : null;?>">
                      </div>   

                      <div class="form-group">
                        <label for="content">Описание <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <textarea required name="content" id="content_" class="form-control" cols="20" rows="5"><?=(isset($item->content)) ? $item->content : null;?></textarea>
                      </div>         

        <hr>
   
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="{{ route($controller) }}" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                     
                </div></form>
            </div>
        </div>        

        
</div>

@endsection
