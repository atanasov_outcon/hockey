<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
<base href="{{ url('/') }}/public/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('public/images/favicon.png') }}">
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="{{ csrf_token() }}">

    <title>@if (isset($meta) == true) {{$meta}} | @endif {{ config('app.name', 'Laravel') }}</title>
    <meta name="description" content="@if (isset($metaDescription) == true) {{$metaDescription}} @endif">

    <title>{{ config('app.name', 'Laravel') }}</title>

        <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    
        <!-- include libraries(jQuery, bootstrap) -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>
    

    <script src="https://use.fontawesome.com/2b915e1407.js"></script>



    <!-- Styles -->
    <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('public/css/upgrade.css') }}" rel="stylesheet">


</head>

<style>
    
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Вход</a></li>
                            <!-- <li><a href="{{ route('register') }}">Регистрация</a></li> -->
                        @else

                            @if (Auth::user()->role = 'Administrator')

                            <li class="dropdown  <?php if(in_array($controller, ['codes', 'categories', 'attributes', 'vendors', 'collections']) == true ) echo "active"; ?> ">
                                <a class="dropdown-toggle " data-toggle="dropdown" role="button" aria-expanded="false">
                                   <i class="fa fa-book"></i> Каталог <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <li class="@if ($controller == 'categories') active @endif"><a href="{{ route('categories') }}">
                                            <i class="fa fa-bars"></i> Категории</a>
                                        </li>
                                        <li class="@if ($controller == 'attributes') active @endif"><a href="{{ route('attributes') }}">
                                            <i class="fa fa-paperclip"></i> Атрибути</a>
                                        </li>
                                        <li class="@if ($controller == 'vendors') active @endif"><a href="{{ route('vendors') }}">
                                            <i class="fa fa-globe"></i> Производители</a>
                                        </li>
                                        <li class="@if ($controller == 'collections') active @endif"><a href="{{ route('collections') }}">
                                            <i class="fa fa-calendar"></i> Колекции</a>
                                        </li>

                                        <li class="@if ($controller == 'codes') active @endif"><a href="{{ route('codes') }}">
                                            <i class="fa fa-key"></i> Промо кодове</a>
                                        </li>
                                </ul>
                            </li>

                            <li class="@if ($controller == 'products') active @endif"><a href="{{ route('products') }}">
                                <i class="fa fa-circle"></i> Продукти</a>
                            </li>

                            <li class="@if ($controller == 'orders') active @endif"><a href="{{ route('orders') }}">
                                <i class="fa fa-shopping-basket"></i> Поръчки
                                @if($newOrders > 0)
                                        <span class="label label-success">{{ $newOrders }}</span> 
                                @endif</a>
                            </li>

                            <li class="@if ($controller == 'users') active @endif"><a href="{{ route('users') }}">
                                <i class="fa fa-users"></i> Клиенти</a>
                            </li>
                            @endif

                            <li class="@if ($controller == 'pages') active @endif"><a href="{{ route('pages') }}">
                                <i class="fa fa-calendar"></i> Страници</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                   <i class="fa fa-user"></i> {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Изход
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
        @yield('content')
        </div>

    </div>

    <script src="{{ asset('public/js/app.js') }}"></script>



</body>
</html>
