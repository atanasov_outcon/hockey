 template: '<div class="row">
 	<div class="pagination mx-auto">
 	<button class="btn btn-default" v-if="pagination.prev_item_url" v-on:click="app.fetchItems(pagination.prev_item_url)" :disabled="!pagination.prev_item_url">
  <i class="fa fa-caret-left"></i>
  предишна 
</button><button class="btn btn-default" v-if="pagination.next_item_url" v-on:click="app.fetchItems(pagination.next_item_url)" :disabled="!pagination.next_item_url"> 
	следваща <i class="fa fa-caret-right"></i></button><small class="pull-right">Страница @{{pagination.current_item}} от @{{pagination.last_item}}</small>
</div></div>',
