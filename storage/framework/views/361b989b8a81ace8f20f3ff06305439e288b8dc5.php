<footer>
    <div class="footer-box ">
        <div class="container">
            <h3>За нас  /  Правила за ползване на интернет сайта  /  Общи условия за ползване на услугата <span style="text-transform: none;">UnyQTV</span>
</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-boxes">
                        <img src="img/icons/phone-icon.png" class="img-responsive  center-block" alt="news-2">
                        <div class="text-contact-box">
                            <div class="contact-text-heading">Contact sales:</div>
                            (678) 999-3002
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-boxes">
                        <img src="img/icons/mail-icon.png" class="img-responsive  center-block" alt="news-2">
                        <div class="text-contact-box">
                            <div class="contact-text-heading">Contact:</div>
                            For comments or questions
                            <br> contact us
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="all-rights ">
        <div class="container">
            <p>© Copyright 2017. All Rights Reserved.</p>
            <ul class="social-network social-circle">
                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin-square"></i></a>
                </li>
                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                </li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</footer>
<script>
    window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


</body>
</html>