<?php $__env->startSection('content'); ?>


<div id="container-vue" class="all-news-page">
    <div class="text-center">
        <h3 class="title-underline">NEWS</h3>
    </div>
    <div class="container">


            <div v-for="new in news" is="new" :new="new"></div>
            <template id="template-new">
            
            <div class="one-news">
            <div class="row">
                <div class="col-md-4">
                    <a href="<?php echo e(url('news')); ?>/{{new.slug}}">
                        <div class="news-image-bg" style="background: url('uploads/news/{{new.image}}');"></div>
                    </a>
                </div>
                <div class="col-md-8">
                    <h1 class="news-title">
                        <a href="<?php echo e(url('news')); ?>/{{new.slug}}">
                            {{new.title}}
                        </a>
                    </h1>
                    <div class="news-info clearfix">
                        <!-- <div class="news-date">14 Feb 2014 | Comments <strong>0</strong></div> -->
                        <!-- <div class="news-author"><span>By <strong>Rumen</strong></span><img src="images/team1.png" width="35" height="35" alt="team"></div> -->
                    </div>
                    <div class="news-text">
                         {{{new.content}}}
                    </div>
                    <a href="<?php echo e(url('news')); ?>/{{new.slug}}" class="link-read-more">Прочети повече</a>
                </div>
            </div>
            </div>
            </template>

            <div class="row">
            <div class="col-md-12">
            <div class="text-center">
            <div class="pagination" v-if="pagination.last_new > 1">
                    <a class="btn btn-pagination" @click="fetchNews(pagination.prev_new_url)"
                            :disabled="!pagination.prev_new_url">
                        Previous
                    </a>
                    <a>&nbsp;&nbsp;&nbsp;Page {{pagination.current_new}} of     {{pagination.last_new}}&nbsp;&nbsp;&nbsp;</a>
                    <a class="btn btn-pagination" @click="fetchNews(pagination.next_new_url)"
                            :disabled="!pagination.next_new_url">Next
                    </a>

                </div>
            </div></div></div>


        
    </div>
</div>




     <a href="#" style="opacity: 0" id="myBtn">Donate</a>

                    <script type="text/javascript">
                    Vue.component('new', {
                        template: '#template-new',
                        props: ['new'],
                    })

                    new Vue({
                        el: '#container-vue',
                        data: {
                            news: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchNews()
                        },
                        methods: {
                            fetchNews: function (new_url) {
                                let vm = this;
                                new_url = new_url || "<?php echo e(url('/api/news')); ?>"
                                this.$http.get(new_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('news', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_new: data.current_page,
                                    last_new: data.last_page,
                                    next_new_url: data.next_page_url,
                                    prev_new_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>