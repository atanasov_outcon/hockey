<?php $__env->startSection('content'); ?>


<div id="container-vue" class="all-sponsors-page">
    <div class="text-center">
       <h3 class="title-underline">PARTNERS</h3>
    </div>

    <div class="container">
        <div class="row">
            
            <div v-for="sponsor in sponsors" is="sponsor" :sponsor="sponsor"></div>

            <template id="template-sponsor">
            <div class="col-sm-3 col-xs-6">
                <a target="_blank" href="{{sponsor.link}}">
                    <figure><img src="<?php echo e(url('/')); ?>/public/uploads/sponsors/{{sponsor.image}}" class="img-responsive" alt="img"></figure>
                </a>
            </div>
            </template>
        </div>
    </div>


    
</div>




     <a href="#" style="opacity: 0" id="myBtn">Donate</a>

                    <script type="text/javascript">
                    Vue.component('sponsor', {
                        template: '#template-sponsor',
                        props: ['sponsor'],
                    })

                    new Vue({
                        el: '#container-vue',
                        data: {
                            sponsors: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchSponsors()
                        },
                        methods: {
                            fetchSponsors: function (sponsor_url) {
                                let vm = this;
                                sponsor_url = sponsor_url || "<?php echo e(url('/api/sponsors')); ?>"
                                this.$http.get(sponsor_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('sponsors', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_sponsor: data.current_page,
                                    last_sponsor: data.last_page,
                                    next_sponsor_url: data.next_page_url,
                                    prev_sponsor_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>