<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sliders
                        <a href="<?php echo e(url('manage/sliders/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add slider</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchSliders(pagination.prev_slider_url)"
                                :disabled="!pagination.prev_slider_url">
                            Previous
                        </button>
                        <span>Page {{pagination.current_slider}} of {{pagination.last_slider}}</span>
                        <button class="btn btn-default" @click="fetchSliders(pagination.next_slider_url)"
                                :disabled="!pagination.next_slider_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="slider in slider" is="slider" :slider="slider"></tr>
                    </table>

                    <template id="template-slider">
                        <tr>
                            <td>
                                {{slider.id}}
                            </td>
                            <td class="col-md-6">
                                {{slider.title}}
                            </td>
                            <td>
                                {{slider.slug}}
                            </td>                              
                            <td>
                                <span v-if="slider.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>                           
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/sliders/edit/{{slider.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/sliders/delete/{{slider.id}}" onclick="return confirm('Are you sure you want to DELETE this slider?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>

                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchSliders(pagination.prev_slider_url)"
                                :disabled="!pagination.prev_slider_url">
                            Previous
                        </button>
                        <span>Page {{pagination.current_slider}} of {{pagination.last_slider}}</span>
                        <button class="btn btn-default" @click="fetchSliders(pagination.next_slider_url)"
                                :disabled="!pagination.next_slider_url">Next
                        </button>
                    </div>
                    <br> 
                <a href="<?php echo e(url('manage/sliders/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add slider</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('slider', {
                        template: '#template-slider',
                        props: ['slider'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            slider: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchSliders()
                        },
                        methods: {
                            fetchSliders: function (slider_url) {
                                let vm = this;
                                slider_url = slider_url || '<?php echo e(url("/api/sliders")); ?>'
                                this.$http.get(slider_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('slider', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_slider: data.current_page,
                                    last_slider: data.last_page,
                                    next_slider_url: data.next_page_url,
                                    prev_slider_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            },
                            viewPage: function (id) {
                                let vm = this;
                                vm.$http.get('/api/sliders/' + id).then(function(response) {
                                    vm.$set('slider', response.data.data)
                                })
                                vm.$els.taskinput.focus()
                                vm.edit = true
                            }
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>