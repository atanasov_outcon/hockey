<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta property="og:title" content="*|MC:SUBJECT|*" />
    <title></title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0" style="font-family: 'Roboto Condensed', sans-serif; background-color: #f5f8fa;">
    <center>
        <table border="0" cellpadding="0" cellspacing="0" width="600px">
            <tr>
                <td border="0" width="50%" style="padding: 20px 0px;">
                	<img src="<?php echo e(asset('public/img/logo.png')); ?>" style="height: 140px;">
                </td>
            </tr>

            <tr>
                <td colspan="2" style="padding: 15px 0px;">
				
				<h3>Моля натиснете линка и се логнете със следните данни, за да потвърдете вашият е-мейл</h3>
<br>
                име &nbsp; &nbsp; &nbsp;: &nbsp; &nbsp; &nbsp; <?php echo e($email); ?> <br>
                парола: &nbsp; &nbsp; &nbsp; <?php echo e($password); ?> <br>
					<br><br>
				<a href="<?php echo e(url( '/verifyMail/'.urlencode(base64_encode($email)) )); ?>" target="_blank" style="font-family: 'Roboto Condensed', sans-serif; color: #3097d1;">Натиснете този линк за да потвърдите вашият е-мейл <?php echo e($email); ?>!</a>
				
                </td>
                <tr>
		
                    <td colspan="2" style="font-family: 'Roboto Condensed', sans-serif; font-size: 13px; text-align: center; padding-top: 20px;">
                        © 2017
                    </td>
                </tr>
            </tr>
        </table>

        
    </center>
</body>
</html>