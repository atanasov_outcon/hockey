<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <base href="http://webstart.dev/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="<?php echo e(csrf_token()); ?>">
    <title><?php if(isset($metaTitle) == true): ?> <?php echo e($metaTitle); ?> | <?php endif; ?> <?php echo e(config('app.name', 'UnicomsGateway')); ?></title>
    <meta name="description" content="<?php if(isset($metaDescription) == true): ?> <?php echo e($metaDescription); ?> <?php endif; ?>">
    <title><?php echo e(config('app.name', 'Laravel')); ?></title>
    <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    <!-- include libraries(jQuery, bootstrap) -->
    <script src="<?php echo e(asset('public/js/vendor/modernizr.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/vendor/jquery.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/bootstrap.min.js')); ?>"></script>
    <script src="<?php echo e(asset('public/js/skrollr.min.js')); ?>"></script>
    <!-- Styles -->
    <link href="<?php echo e(asset('public/css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/normalize.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/main.css')); ?>" rel="stylesheet">
</head>
<body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <img alt="unyqtv" src="<?php echo e(asset('public/img/logo.png')); ?>">
                      </a>
                </div>
                <div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="<?php echo e(url('/')); ?>/<?=(Route::currentRouteName() == 'index') ? null : 'index'; ?>#section4">Цени и пакети</a></li>
                            <li><a href="<?php echo e(url('/')); ?>/<?=(Route::currentRouteName() == 'index') ? null : 'index'; ?>#section6">Помощ</a></li>
                            <li><a href="<?php echo e(url('/')); ?>/<?=(Route::currentRouteName() == 'index') ? null : 'index'; ?>#section3">Активация</a></li>
                            <li><a href="<?php echo e(url('/')); ?>/<?=(Route::currentRouteName() == 'index') ? null : 'index'; ?>#section7">Новини</a></li>
                            <!--  <li><a href="<?php echo e(url('/')); ?>/<?=(Route::currentRouteName() == 'index') ? null : 'index'; ?>#">Вход &nbsp;<i class="fa fa-user-o" aria-hidden="true"></i></a></li> -->
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle dropdown-link-login" data-toggle="dropdown"><i class="fa fa-user-o" aria-hidden="true"></i> Вход <span class="caret"></span></a>
                                <ul class="dropdown-menu dropdown-lr" role="menu">
                                    <div class="login-form">
                                        <div class="col-lg-12">
                                            <div class="text-center">
                                                <h3><b>Вход</b></h3></div>
                                            <form id="ajax-login-form" action="<?php echo e(url('/')); ?>" method="post" role="form" autocomplete="off">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="form-group">
                                                    <label for="username">Имейл:</label>
                                                    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Имейл" value="" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">Парола:</label>
                                                    <input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Парола" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <!--                                        <div class="col-xs-7">
                                                        <input type="checkbox" tabindex="3" name="remember" id="remember">
                                                        <label for="remember"> Запомни ме</label>
                                                    </div> -->
                                                        <div class="col-xs-12">
                                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-red" value="Влез">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <div class="text-center">
                                                                <a class="forgot-password">Забравена парола?</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="forgot-form">
                                        <div class="col-lg-12">
                                            <div class="text-center">
                                                <h3><b>Забравена парола</b></h3></div>
                                            <form id="ajax-login-form" action="<?php echo e(url('/')); ?>" method="post" role="form" autocomplete="off">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="form-group">
                                                    <label for="username">Имейл:</label>
                                                    <input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Имейл" value="" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-xs-7">
                                                            <a class="forgot-password-back">Обратно</a>
                                                        </div>
                                                        <div class="col-xs-5 pull-right">
                                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-red" value="Влез">
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>
    <?php echo $__env->yieldContent('content'); ?>
    <footer>
        <div class="footer-box ">
            <div class="container">
                <h3>

                    <h3>
            <a href="<?php echo e(url('/')); ?>/za-nas">ЗА НАС</a> / <a href="<?php echo e(url('/')); ?>/obschi-usloviya-za-polzvane-na-uslugata-unyqtv">ОБЩИ УСЛОВИЯ ЗА ПОЛЗВАНЕ НА УСЛУГАТА UnyQTV</a>
            
            <!-- <?php echo e(strip_tags($blocks['footer-slogan']->content)); ?> -->
            </h3>
                <div class="row">
                    <div class="col-md-6">
                        <div class="contact-boxes">
                            <img src="img/icons/phone-icon.png" class="img-responsive  center-block" alt="news-2">
                            <div class="text-contact-box">
                                <div class="contact-text-heading">Contact sales:</div>
                                <a href="tel:0700 3 10 20">0700 3 10 20</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-boxes">
                            <img src="img/icons/mail-icon.png" class="img-responsive  center-block" alt="news-2">
                            <div class="text-contact-box email-footer-text">
                                <div class="contact-text-heading">Contact:</div>
                                <a href="mailto:support@unicomsservices.com" target="_top">support@unicomsservices.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="all-rights ">
            <div class="container">
                <p>© Copyright 2017. All Rights Reserved.</p>
                <ul class="social-network social-circle">
                    <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin-square"></i></a>
                    </li>
                    <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
        </div>
    </footer>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Регистрация</h4>
                </div>
                <div class="modal-body">
                    <div class="modal-register-code">
                        <div class="margin10"></div>
                        <h3>Инструкции за регистрация</h3>
                        <div class="alert alert-danger alert-hidden alert-contract-code">
                           Моля, въведете правилен код за регистрация!
                        </div>
                        <p>
Ползването на услугата UnyQTV изисква регистрация. За да можете да се регистрирате е необходимо да изберете името на оператора, при когото сте подписали договор за ползване на услугата UnyQTV.  Необходимо е също да въведете номера на договора за ползване на услугата. Системата ще Ви даде възможност да си изберете потребителско име и паролата за достъп до UnyQTV приложението. Те ще са Ви необходими за да стартирате приложението UnyQTV, което сте свалили на Вашето мобилно устройство от Play Store или Apple Store. На мейл адреса Ви ще бъде поискано да потвърдите регистрацията. За по-подробна информация можете да се свържете с оператора или с центъра ни за обслужване на клиенти на тел 0700 3 1020.
                        </p>
                        <div class="margin30"></div>
                        <form class="form-horizontal">
                            <fieldset>
                                <!-- Select Basic -->
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="selectbasic">Избери твоя оператор <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title='задължително'></i></label>

                                    <div class="col-md-6">
                                        <select id="selectbasic" name="selectbasic" class="form-control">
                                            <option value="" disabled selected></option>
                                            <?php if(isset($operators)): ?>
                                                <?php $__currentLoopData = $operators; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $operator): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                                    <?php if($operator->enabled == true): ?>
                                                <option value="<?php echo e($operator->operatorId); ?>" data-identifier="<?php echo e($operator->identifierName); ?>"><?php echo e($operator->name); ?></option>
                                                    <?php endif; ?>
                                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            <?php endif; ?>
                                        </select>
                                        
                                    </div>
                                </div>
                                <div class="form-group contract_code">
                                    <label class="col-md-4 control-label" for="contract_codes"><span class="identifier-text">   </span> <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title='задължително'></i></label>
                                    <div class="col-md-6">
                                        <input onkeyup="this.value=this.value.replace(/[^\d]/,'')" type="text" name="contract_codes" id="contract_codes" tabindex="1" class="form-control " placeholder="" value="" autocomplete="off">
                                        
                                    </div>
                                </div>

                            <div class="modal-footer">
                                <button type="button" class="modal-register-btn btn btn-red">РЕГИСТРИРАЙ СЕ <i class="spinner fa fa-spinner fa-spin fa-fw"></i></button>
                            </div>
                            </fieldset>
                        </form>
                    </div>
                    <div class="modal-register-login">
                        <div class="margin10"></div>
                        <div class="alert alert-danger alert-hidden alert-email">
                           Моля, въведете правилен е-мейл за регистрация!
                        </div>                        
                        <div class="alert alert-danger alert-hidden alert-email-duplicate">
                           Избраният от вас е-мейл адрес е зает!
                        </div>                        
                        <div class="alert alert-danger alert-hidden alert-password">
                           Моля, въведете парола с повече от 6 символа!
                        </div>                        
                        <div class="alert alert-danger alert-hidden alert-password-duplicate">
                           Въведените пароли не съвпадат!
                        </div>                        
                        <div class="alert alert-danger alert-hidden alert-tos">
                           Моля приемете условията за ползване!
                        </div>
                        <p>За да продължите вашата регистрация, моля попълнете полетата: </p>
                        <div class="margin30"></div>
                        <hr>
                        <form class="form-horizontal">
                            <fieldset>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="name">Име</label>
                                    <div class="col-md-6">
                                        <input id="name" name="name" type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="family">Фамилия</label>
                                    <div class="col-md-6">
                                        <input id="family" name="family" type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="phone">Телефон</label>
                                    <div class="col-md-6">
                                        <input id="phone" name="phone" type="text" placeholder="" class="form-control input-md">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="email-register">Имейл <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title='задължително'></i></label>
                                    <div class="col-md-6">
                                        <input required id="email-register" name="email-register" type="text" placeholder="" class="form-control input-md">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="password-register">Парола <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title='задължително'></i></label>
                                    <div class="col-md-6">
                                        <input required id="password-register" name="password-register" type="password" placeholder="" class="form-control input-md">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label" for="password-register-again">Повтори парола <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title='задължително'></i></label>
                                    <div class="col-md-6">
                                        <input required id="password-register2" name="password-register-again" type="password" placeholder="" class="form-control input-md">
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <input id="tos" name="tos" type="checkbox" placeholder=""> Потвърдете условията за ползване
                                    </div>
                                </div>
                <div class="modal-footer">
                    <button type="button" class="modal-register-btn2 btn btn-red">РЕГИСТРИРАЙ СЕ <i class="spinner fa fa-spinner fa-spin fa-fw"></i></button>
                </div>

                            </fieldset>
                        </form>
                    </div>


                    <div class="modal-register-finnish">
                        <div class="margin10"></div>
                        <p>За да завършите вашата регистрация, моля потвърдете вашият е-мейл!</p>
                        <div class="margin30"></div>
                       

                            </fieldset>
                        </form>
                    </div>

                    </div>

            </div>
        </div>
    </div>

    <script src="<?php echo e(asset('public/js/plugins.js')); ?>"></script>
    <?php if(Route::currentRouteName() == 'index'): ?>
    <script src="<?php echo e(asset('public/js/main.js')); ?>"></script>
    <?php endif; ?>
    <script>
    $(document).ready(function() {
        // navbar fix
        $('.navbar').affix({
            offset: {
                top: 5
            }
        });

        // popover
        $('[data-toggle="popover"]').popover();
    });
    </script>
</body>

</html>