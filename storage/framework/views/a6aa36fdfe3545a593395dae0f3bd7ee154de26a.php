<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 "> 
            <div class="panel panel-default">
                <div class="panel-heading">Edit channel</div>


                <div class="panel-body">
                   
                    <form method="POST">
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="title">Title <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?=(isset($channel->title)) ? $channel->title : null;?>">
                      </div>                  

                      <div class="form-group">
                        <label for="content">Content <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <textarea required name="content" id="content" class="form-control" cols="30" rows="10"><?=(isset($channel->content)) ? $channel->content : null;?></textarea>
                      </div>                  

                      <div class="form-group">
                        <label for="metaTitle">Meta title <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="metaTitle"  id="metaTitle" placeholder="Meta Title" value="<?=(isset($channel->metaTitle)) ? $channel->metaTitle : null;?>">
                      </div>    

                      <div class="form-group">
                        <label for="metaDescription">Meta Description <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="metaDescription"  id="metaDescription" placeholder="Meta Description" value="<?=(isset($channel->metaDescription)) ? $channel->metaDescription : null;?>">
                      </div>    

                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="<?=(isset($channel->active)) ? 1 : 0;?>" <?php if(isset($channel->active) && $channel->active == true) echo 'checked'; ?> > Active
                        </label>
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
                      <a href="<?php echo e(route('channels')); ?>" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Cancel</a>

                    </form>

                </div>
            </div>
        </div>        

        <div class="col-md-4"> 
            <div class="panel panel-default">
                <div class="panel-heading">Gallery</div>
                <div class="panel-body container-vue">
             
             <style>
            
             </style>       



                    <form id="ajax-upload" method="POST" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>



                    <div class="alert alert-success alert-upload-success alert-hidden">
                           Images were uploaded succesfully!
                    </div>                    

                    <div class="alert alert-success alert-upload-error alert-hidden">
                           Please try again later!
                    </div>
                

                      <div class="form-group">
                        <label for="images">Add images</label>
                        <input type="file" class="form-control" name="images[]" id="images" multiple>
                      </div>                  

                      <!-- <button type="submit" class="btn btn-primary btn-upload"><i class="fa fa-floppy-o" aria-hidden="true"></i> Upload</button> -->
                    </form>

                    <hr />

                    <div class="row ">

                            <div class="col-md-6 thumb" v-for="(index, image) in images" >
                                <span class="main-image" v-if="image.is_main==1" href="">главна снимка</span>

                                <div class="col-md-12 controls-bottom">
                                  <a @click="addToContent(image.fullpath)" href="#" title="Add to the content" class="btn-image-add tip" data-dismiss="alert">
                                    <span aria-hidden="true"><i class="fa fa-plus" aria-hidden="true"></i></span>
                                  </a>

           
                                  
                                  <a @click="deleteImage(image.id)" href="#" title="Delete" class="btn-image-delete tip" data-dismiss="alert">
                                    <span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span>
                                  </a>


                                  <a @click="setMainImage(image.id)" href="#" title="Set main image" class="btn-image-delete tip" data-dismiss="alert">
                                    <span aria-hidden="true"><i class="fa fa-image" aria-hidden="true"></i></span>
                                  </a>  
                                </div>                          
                                  <img :src="image.fullpath" class="img-responsive">

                                  <div v-if="index%2!=0" class="clearfix">&nbsp;</div>
                            </div>


                     <template id="template-images">
                        
                     </template>

                    </div>


                   

                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})

Vue.component('images', {
    template: '#template-images',
    props: ['images'],
})

const gallery = new Vue({
    el: '.container-vue',
    data: {
        images: []
    },
    ready: function () {
        this.fetchUploads()
    },
    methods: {
        fetchUploads: function (channel_url) {
            let vm = this;
            channel_url = channel_url || '/api/channels/uploads/<?php echo e($channel->id); ?>'
            this.$http.get(channel_url)
                .then(function (response) {
                    vm.$set('images', response.data.data)
                });
        },             
        deleteImage: function (image_id) {

          var doIt = confirm("Delete this image?");
          if (doIt == true) {
            let vm = this;
            channel_url = '/manage/channels/delete_image/'+image_id
            this.$http.get(channel_url)
                .then(function (response) {
                    this.fetchUploads();
                });
          }


        },      
        setMainImage: function (image_id) {
            let vm = this;
            channel_url = '/manage/channels/set_main_image/<?php echo e($channel->id); ?>/'+image_id
            this.$http.get(channel_url)
                .then(function (response) {
                    this.fetchUploads();
                });
        },           
        addToContent: function (img_url) {
            editor.summernote("insertImage", img_url);
        },
    }
});   

</script>


<script type="text/javascript">

$(document).ready(function() {

  $( "#ajax-upload" ).on('change', function(e) {
    e.preventDefault();

    $.ajax({
        method: 'post',
        url: '/manage/channels/upload/<?php echo e($channel->id); ?>',
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
            $('.alert-upload-success').show().fadeOut(2000);
            $('#images').val('');
            gallery.fetchUploads(); // re fetch the images
        },
        error: function(data){
            $('.alert-upload-error').show().fadeOut(2000);
        },

    });

});




});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>