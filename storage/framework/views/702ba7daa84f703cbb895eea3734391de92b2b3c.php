<?php $__env->startSection('content'); ?>

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
       <br>
<br>
<br>
<br>
<br>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Начало</a></li>
  <li class="breadcrumb-item"><a href="<?php echo e(url('news')); ?>">Новини</a></li>
  <li class="breadcrumb-item active"><?php echo e($new->title); ?></li>
</ol>


                    <h1><?php echo e($new->title); ?></h1>

                <div class="panel-body">
                <img class="img-responsive pull-left" src="<?php echo e(url('/')); ?>/uploads/news/<?php echo e(isset($image->filename) ? $image->filename : 'no-image.jpg'); ?> ">
                    <div class="pull-right">
                    <?php echo $new->content; ?>

                    </div>
                </div>



        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>