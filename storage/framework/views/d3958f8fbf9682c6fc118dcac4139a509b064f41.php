<?php $__env->startSection('content'); ?>

<script>
  $(document).ready(function(){
  $('.news-slider').slick({
  slidesToShow: 5,
  slidesToScroll: 5,
  autoplay: true,
  autoplaySpeed: 7000,
  dots: true,
  arrows: false,
  responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
          slidesToScroll: 3
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    },
    {
      breakpoint: 450,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});
  });
</script> 

  
<div class="news-page">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <a href="<?php echo e(url('news')); ?>" class="back-link"><i class="icon ion-ios-arrow-left"></i> Back</a>
                <h1 class="news-title">
      <?php echo e($new->title); ?>

    </h1>
                <hr>
                <div class="news-info clearfix">
                    <div class="news-date"><?php echo e(date('m.d.Y', strtotime($new->created_at))); ?> <!-- | Comments <strong>0</strong> --></div>
                    <!-- <div class="news-author"><span>By <strong>Rumen</strong></span><img src="images/team1.png" width="35" height="35" alt="team"></div> -->
                </div>

                <?php if(isset($image->filename)): ?>
                <img src="<?php echo e(url('public/uploads/news/'.$image->filename)); ?>" class="img-responsive img-news" alt="<?php echo e($new->title); ?>">
                <?php endif; ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="news-text">
                   <?php echo $new->content; ?>

                </div>
            </div>
            <div class="col-md-4">
                <div class="news-donate-banner">
                    <h2>Make it<br> happen!</h2>
                    <p>Become part of this
                        <br>extraordinary project,
                        <br>and change football
                        <br><strong>FOREVER!</strong></p>
                    <a id="myBtn" class="news-donate-btn">Donate now!</a>
                </div>
            </div>
        </div>
    </div>
    <div class="news-slider-bg">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="slider-title">Related news</h2>
                    <div class="news-slider">

                      <?php $__currentLoopData = $last_news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $lnew): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

                        <div class="news-box">
                            <a href="<?php echo e(url('news')); ?>/<?php echo e($lnew->slug); ?>">
                                <figure><img src="<?php echo e(url('public/uploads/news/'.$lnew->filename)); ?>" class="img-responsive img-max-height" alt="<?php echo e($lnew->title); ?>"></figure>
                                <span><?php echo e($lnew->title); ?></span>
                            </a>
                        </div>

                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-center">
        <h3 class="title-underline">PARTNERS</h3>
    </div>
    <div class="partners-logo">
        <div class="container-fluid">
            <div class="row">

              <?php $__currentLoopData = $sponsors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sponsor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="col-sm-3 col-xs-6">
                    <a href="<?php echo e($sponsor->link); ?>">
                        <figure><img src="<?php echo e(url('public/uploads/sponsors/'.$sponsor->filename)); ?>" class="img-responsive" alt="img"></figure>
                    </a>
                </div>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
        <div class="margin70">
        </div>
        <div class="text-center">
            <h3 class="title-underline">AMBASSADORS</h3>
        </div>
        <div class="ambassadors-images">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/team1.png" class="img-responsive" alt="team">
                                <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Rumen Vatkov</h6>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/team2.png" class="img-responsive" alt="team">
                                <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Megi Slavova</h6>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/Outsource-Consulting-2.png" class="img-responsive" alt="team">
                                <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Outsource Consulting</h6>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/team4.png" class="img-responsive" alt="team">
                                <span><img src="images/flag19.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Alex Gorbanescu</h6>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/team1.png" class="img-responsive" alt="team">
                                <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Rumen Vatkov</h6>
                        </a>
                    </div>
                    <div class="col-sm-6 col-md-3 col-lg-2">
                        <a href="" class="text-center">
                            <figure>
                                <img src="images/team2.png" class="img-responsive" alt="team">
                                <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                            </figure>
                            <h6>Megi Slavova</h6>
                        </a>
                    </div>
                </div>
            </div>
            <div class="margin70">
            </div>
        </div>




<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>