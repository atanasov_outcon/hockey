<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                
                  <form method="POST">
                <div class="panel-heading clearfix">Добавяне на нов запис 
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                </div>


                <div class="panel-body">
                   
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                              <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                              <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие">
                            </div>  
                            <div class="form-group">
                              <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                              <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" >
                            </div> 

                            <div class="form-group">
                              <label for="price">Цена <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i> <small class="text-primary">в формат 49.00</small></label>
                              <input required onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');" type="text" class="form-control" name="price" id="price" placeholder="Цена">
                            </div>                     
        
                            <div class="form-group">
                              <label for="discount">Намалена цена <small class="text-primary">в формат 49.00</small></label>
                              <input onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');" type="text" class="form-control" name="discount" id="discount" placeholder="Промоционална цена">
                            </div>                     
      

                        </div>

                        <div class="col-md-4">
                          <div class="form-group">
                            <label for="title">Категория <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                            <select required type="text" class="form-control" name="category_id" id="category_id" placeholder="Категория">
                              <option value="0" selected>избери</option>
                              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($cat->id); ?>" ><?php echo e($cat->title); ?></option>
                                  <?php if($cat->subcategories() == true): ?>
                                    <?php $__currentLoopData = $cat->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option value="<?php echo e($sub->id); ?>" >- <?php echo e($sub->title); ?></option>
                                      <?php if($sub->subcategories() == true): ?>
                                        <?php $__currentLoopData = $sub->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($sub->id); ?>" >-- <?php echo e($sub->title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      <?php endif; ?>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                        </div> 

                        <div class="form-group">
                          <label for="title">Производител</label>
                          <select type="text" class="form-control" name="vendor_id" id="vendor_id" placeholder="Производител">
                             <option value="0" selected disabled>избери</option>
                            <?php $__currentLoopData = $vendors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($vendor->id); ?>"><?php echo e($vendor->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                        </div> 
                       
                        <div class="form-group">
                          <label for="title">Колекция</label>
                          <select type="text" class="form-control" name="collection_id" id="collection_id" placeholder="Колекция">
                             <option value="0" selected disabled>избери</option>
                            <?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option value="<?php echo e($collection->id); ?>"><?php echo e($collection->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                        </div> 

                            <div class="form-group">
                              <label for="tags">Тагове <small class="text-primary">разделени със запетайка</small></label>
                              <input type="text" class="form-control" name="tags" id="tags" placeholder="Тагове" >
                            </div> 
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                              <label for="sku">SKU</label>
                              <input type="text" class="form-control" name="sku" id="sku" placeholder="SKU" >
                            </div>                     
                            <div class="form-group">
                              <label for="barcode">Barcode</label>
                              <input type="text" class="form-control" name="barcode" id="barcode" placeholder="Barcode">
                            </div>                     
                            <div class="form-group">
                              <label for="quantity">Бройка</label>
                              <input onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" type="text" class="form-control" name="quantity" id="quantity" placeholder="Бройка">
                            </div>                     
                            <div class="form-group">
                              <label for="weight">Тегло</label>
                              <input onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" type="text" class="form-control" name="weight" id="weight" placeholder="Тегло">
                            </div>                     
                        </div>

                      </div>                


<!-- 
                        <div class="col-md-12">
                            <div class="checkbox">
                              <label for="is_vat">
                              <input type="checkbox" name="is_vat" id="is_vat" value="1">
                              Цената е <strong>с</strong> ДДС</label>
                            </div>                     
                        </div> -->

<hr>
                      <div class="form-group">
                        <label for="intro">Кратко описание</label>
                        <textarea name="intro" id="intro" class="form-control" cols="20" rows="2"></textarea>
                      </div>   

                      <div class="form-group">
                        <label for="content">Описание</label>
                        <textarea name="content" id="content_" class="form-control" cols="30" rows="5"></textarea>
                      </div>  

  <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="1" > Активен
                        </label>
                      </div>

              <hr>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>


                </div>

            </form>
            </div>

        </div>        

    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>