<?php $__env->startSection('content'); ?>
    <div class="row">
        

        <div class="col-md-8 "> 
            <div class="panel panel-default">
                <form method="POST">
                <div class="panel-heading">Редакция на <?php echo e($item->title); ?> 
                  <a onclick="deleteItem(<?php echo e($item->id); ?>)" class="text-danger margin-left"><i class="fa fa-trash"></i> изтрий</a>
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                </div>


                <div class="panel-body">
                   
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>


                      
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                              <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                              <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="<?=(isset($item->title)) ? $item->title : null;?>">
                            </div>  
                            <div class="form-group">
                              <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                              <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="<?=(isset($item->slug)) ? $item->slug : null;?>">
                            </div>    

                           <div class="form-group">
                              <label for="price">Цена <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i> <small class="text-primary">в формат 49.00</small></label>
                              <input required required onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');" type="text" class="form-control" name="price" id="price" placeholder="Цена" value="<?=(isset($item->price)) ? $item->price : null;?>">
                            </div>                     
                            <div class="form-group">
                              <label for="discount">Намалена цена <small class="text-primary">в формат 49.00</small></label>
                              <input required onkeyup="this.value=this.value.replace(/[^0-9.]/g,'');" type="text" class="form-control" name="discount" id="discount" placeholder="Намалена цена" value="<?=(isset($item->discount)) ? $item->discount : null;?>">
                            </div> 
                                                                    
                        </div>
            

                      <div class="col-md-4">
                          <div class="form-group">
                            <label for="title">Категория <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                            <select required type="text" class="form-control" name="category_id" id="category_id" placeholder="Категория">
                              <option value="0">избери</option>
                              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $cat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php if($item->category_id == $cat->id ): ?> selected <?php endif; ?> value="<?php echo e($cat->id); ?>" ><?php echo e($cat->title); ?></option>
                                  <?php if($cat->subcategories() == true): ?>
                                    <?php $__currentLoopData = $cat->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <option <?php if($item->category_id == $sub->id ): ?> selected <?php endif; ?> value="<?php echo e($sub->id); ?>" >- <?php echo e($sub->title); ?></option>
                                    <?php if($sub->subcategories() == true): ?>
                                        <?php $__currentLoopData = $sub->subcategories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $sub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                        <option value="<?php echo e($sub->id); ?>" >-- <?php echo e($sub->title); ?></option>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    <?php endif; ?>

                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                  <?php endif; ?>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </select>
                          </div> 
             

                        <div class="form-group">
                          <label for="title">Производител</label>
                          <select type="text" class="form-control" name="vendor_id" id="vendor_id" placeholder="Производител">
                             <option value="0" selected disabled>избери</option>
                            <?php $__currentLoopData = $vendors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $vendor): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php if($item->vendor_id == $vendor->id ): ?> selected <?php endif; ?> value="<?php echo e($vendor->id); ?>"><?php echo e($vendor->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                        </div> 
                        
                        <div class="form-group">
                          <label for="title">Колекция</label>
                          <select type="text" class="form-control" name="collection_id" id="collection_id" placeholder="Колекция">
                             <option value="0" selected disabled>избери</option>
                            <?php $__currentLoopData = $collections; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $collection): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <option <?php if($item->collection_id == $collection->id ): ?> selected <?php endif; ?> value="<?php echo e($collection->id); ?>"><?php echo e($collection->title); ?></option>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </select>
                        </div> 

                        <div class="form-group">
                          <label for="tags">Тагове <small class="text-primary">разделени със запетайка</small></label>
                          <input type="text" class="form-control" name="tags" value="<?=$item->tagsComma;?>" placeholder="Тагове">
                        </div>                     
                      </div>

                      <div class="col-md-4">

                            <div class="form-group">
                                  <label for="sku">SKU</label>
                                  <input type="text" class="form-control" name="sku" value="<?=$item->sku;?>" placeholder="SKU" >
                            </div>                     
                            <div class="form-group">
                              <label for="barcode">Barcode</label>
                              <input type="text" class="form-control" name="barcode" value="<?=$item->barcode;?>" placeholder="Barcode">
                            </div>                     
                            <div class="form-group">
                              <label for="quantity">Бройка</label>
                              <input onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" type="text" class="form-control" name="quantity" value="<?=$item->quantity;?>" placeholder="Бройка">
                            </div>                     
                            <div class="form-group">
                              <label for="weight">Тегло</label>
                              <input onkeyup="this.value=this.value.replace(/[^0-9]/g,'');" type="text" class="form-control" name="weight" value="<?=$item->weight;?>" placeholder="Тегло">
                            </div>    


                        </div>

                      </div>
                        
                        
<!--                         <div class="col-md-12">
                            <div class="checkbox">
                              <label for="is_vat"> 
                              <input <?php if($item->is_vat == 1):?> checked <?php endif; ?>  type="checkbox" name="is_vat"  id="is_vat" value="1" >
                              Цената е <strong>с</strong> ДДС</label>
                            </div>                     
                        </div> -->


<hr>

  

                      <div class="form-group">
                        <label for="intro">Кратко описание</label>
                        <textarea name="intro" id="intro" class="form-control" cols="20" rows="2"><?=(isset($item->intro)) ? $item->intro : null;?></textarea>
                      </div>   

                      <div class="form-group">
                        <label for="content">Описание</label>
                        <textarea name="content" id="content_" class="form-control" cols="20" rows="5"><?=(isset($item->content)) ? $item->content : null;?></textarea>
                      </div>   
                          
<hr>
                      <?php if( isset($attributes) ): ?>
                        <div class="form-group">
                          <div class="row">
                          <?php $__currentLoopData = $attributes; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $att): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <div class="col-md-3">
                                <h4><?php echo e($att->title); ?></h4>
                              <?php $__currentLoopData = $att->values; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <label class="hover "> 
                                <input name="attributes_values[]" <?php if( in_array($value->id, $item_attributes) == true ):?> checked <?php endif; ?> value="<?php echo e($value->id); ?>" type="checkbox"> <?php echo e($value->value); ?>

                              </label>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </div>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>

                            <a href="">Генерирай продуктови варианти</a>     

                      </div>  
                      <?php endif; ?>

   <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="<?=(isset($item->active)) ? 1 : 0;?>" <?php if(isset($item->active) && $item->active == true) echo 'checked'; ?> > Активен
                        </label>
                      </div>

  <hr>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>


                </div>
            </div>
      </form>
      </div>        


        <div class="col-md-4"> 
            <div class="panel panel-default">
                <div class="panel-heading">Галерия</div>
                <div class="panel-body container-vue">
             
             <style>
            
             </style>       



                    <form id="ajax-upload" method="POST" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>



                    <div class="alert alert-success alert-upload-success alert-hidden">
                           Картинките са качени успешно!
                    </div>                    

                    <div class="alert alert-danger alert-upload-error alert-hidden">
                          Моля пробвай отново!
                    </div>
                

                      <div class="form-group">
                      <div class="spinLoader alert-hidden pull-right">качване <i class="fa fa-spinner fa-pulse fa-fw "></i> <span class="sr-only">качване...</span></div>
                        <label for="images">Добави изображения</label>
                        <input type="file" class="form-control" name="images[]" id="images" multiple>
                      </div>                  

                    </form>
                    
                   
                    <hr />

                    <div class="row ">

                            <div class="col-md-6 thumb" v-for="(index, image) in images" >
                                <span class="main-image" v-if="image.is_main==1" href="">главна снимка</span>

                                <div class="col-md-12 controls-bottom">
                                    <a v-on:click="setMainImage(image.id)" href="#" title="Set main image" class="btn-image-add tip" data-dismiss="alert">
                                    <span><i class="fa fa-image"></i></span>
                                    </a>  
                                  
                                  <a v-on:click="deleteImage(image.id)" href="#" title="Delete" class="btn-image-delete tip" data-dismiss="alert">
                                    <span><i class="fa fa-trash"></i></span>
                                  </a>
                  
                                </div>                          
                                  <img :src="image.fullpath" class="img-responsive">

                                  <div v-if="index%2!=0" class="clearfix">&nbsp;</div>
                            </div>

                      <div class="clearfix"></div>
                      <hr>
                      <div class="container">
                        <small><i class="fa fa-image"></i> - направи главна картинка</small> <br>
                        <small><i class="fa fa-trash"></i> - изтрий</small> <br>
                      </div>


                    </div>


                   

                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})


var deleteItem = function(item_id) {
    r = confirm('Изтрий този запис?');
    if(r == true) {
    item_url =  '<?php echo e(url("/manage/".$controller)); ?>/delete/'+item_id
    gallery.$http.get(item_url)
        .then(function (response) {
            window.location.href = '<?php echo e(url("/manage/".$controller)); ?>';
        }, function() {
            alert('gre6ka')
        });
    }
};

const gallery = new Vue({
    el: '.container-vue',
    data: {
        images: []
    },
    ready: function () {
        this.fetchUploads()
    },
    methods: {
        fetchUploads: function () {
            let vm = this;
            item_url =  "<?php echo e(url('/api/'.$controller.'/uploads/'.$item->id)); ?>"
            this.$http.get(item_url)
                .then(function (response) {
                    vm.$set('images', response.data.data)
                });
        },             
        deleteImage: function (image_id) {

          var doIt = confirm("Изтрий това изображение?");
          if (doIt == true) {
            let vm = this;
            item_url = "<?php echo e(url('/manage/'.$controller.'/delete_image/')); ?>/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
          }


        },      
        setMainImage: function (image_id) {
            let vm = this;
            item_url = "<?php echo e(url('/manage/'.$controller.'/set_main_image/'.$item->id)); ?>/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
        },           
        addToContent: function (img_url) {
            editor.summernote("insertImage", img_url);
        },
    }
});   

</script>


<script type="text/javascript">

$(document).ready(function() {

  $( "#ajax-upload" ).on('change', function(e) {
    e.preventDefault();
    $('.spinLoader').show();
    $.ajax({
        method: 'post',
        url: '<?php echo e(url("manage/".$controller."/upload/".$item->id)); ?>',
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
          $('.spinLoader').hide();
            $('.alert-upload-success').show().fadeOut(2000);
            $('#images').val('');
            gallery.fetchUploads(); // re fetch the images
        },
        error: function(data){
          $('.spinLoader').hide();
            $('.alert-upload-error').show().fadeOut(2000);
        },

    });

});




});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>