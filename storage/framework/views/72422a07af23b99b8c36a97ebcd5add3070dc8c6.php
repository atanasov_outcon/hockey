<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Blocks
                        <a href="<?php echo e(url('manage/blocks/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add block</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchBlocks(pagination.prev_block_url)"
                                :disabled="!pagination.prev_block_url">
                            Previous
                        </button>
                        <span>Block {{pagination.current_block}} of {{pagination.last_block}}</span>
                        <button class="btn btn-default" @click="fetchBlocks(pagination.next_block_url)"
                                :disabled="!pagination.next_block_url">Next
                        </button>
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Position</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="block in blocks" is="block" :block="block"></tr>
                    </table>

                    <template id="template-block">
                        <tr>
                            <td>
                                {{block.id}}
                            </td>
                            <td class="col-md-6">
                                {{block.title}}
                            </td>
                            <td>
                                {{block.position}}
                            </td>                           
                            <td>
                                <span v-if="block.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/blocks/edit/{{block.id}}">
                                   Edit
                                </a>                                
                            </td>
                        </tr>
                    </template>
                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchBlocks(pagination.prev_block_url)"
                                :disabled="!pagination.prev_block_url">
                            Previous
                        </button>
                        <span>Block {{pagination.current_block}} of {{pagination.last_block}}</span>
                        <button class="btn btn-default" @click="fetchBlocks(pagination.next_block_url)"
                                :disabled="!pagination.next_block_url">Next
                        </button>
                    </div>
                    <br>
                <a href="<?php echo e(url('manage/blocks/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add block</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('block', {
                        template: '#template-block',
                        props: ['block'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            blocks: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchBlocks()
                        },
                        methods: {
                            fetchBlocks: function (block_url) {
                                let vm = this;
                                block_url = block_url || '<?php echo e(url("/api/blocks")); ?>'
                                this.$http.get(block_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('blocks', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_block: data.current_page,
                                    last_block: data.last_page,
                                    next_block_url: data.next_page_url,
                                    prev_block_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            }
                        }
                    });   
                </script>


                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>