 <?php $__env->startSection('content'); ?>
<script src="<?php echo e(asset('public/js/main.js')); ?>"></script>
<div class="row">
        <div class="col-md-8 col-md-offset-2">
       <br>
<br>
<br>
<br>
<br>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="http://145.255.200.121/unyqtv/index.php">Начало</a></li>
  <li class="breadcrumb-item active">Активация на акаунт</li>
</ol>


                    <h1>Активация на акаунт</h1>

                <div class="panel-body">
                <div class="margin20"></div>
                    <div class="activation-form">
                        <div class="alert alert-danger alert-hidden alert-activation">
                           Моля, попълнете полетата с праилни стойности!
                        </div>
                    <form id="ajax-login-form" action="<?php echo e(url('/')); ?>" method="post" role="form" autocomplete="off">
                                                <?php echo e(csrf_field()); ?>

                                                <div class="form-group">
                                                    <label for="email">Имейл:</label>
                                                    <input type="text" name="email" id="email-activation" tabindex="1" class="form-control" placeholder="Имейл" value="" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <label for="password">Парола:</label>
                                                    <input type="password" name="password" id="password-activation" tabindex="2" class="form-control" placeholder="Парола" autocomplete="off">
                                                </div>
                                                <div class="form-group">
                                                    <div class="row">
                                                        
                                                        <div class="col-xs-12">
                                                       
                                                       <button type="button" class="activation-btn btn btn-red">АКТИВИРАЙ АКАУНТА СИ<i class="spinner fa fa-spinner fa-spin fa-fw"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                    </form>  

                    </div>

                    <div class="activation-form-finnish">
                       Вие успешно активирахте вашият акаунт!

                    </div>

                </div>



        </div>
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>