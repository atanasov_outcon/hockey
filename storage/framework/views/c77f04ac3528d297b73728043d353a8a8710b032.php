<?php $__env->startSection('content'); ?>
    <div class="row">
       <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading clearfix"><?php echo e($itemTitle); ?>

                        <!-- <a href="<?php echo e(url('manage/'.$controller.'/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Добавяне на нов запис</a> -->
                </div>

                <div class="panel-body">
                
                <div class="container-vue">

                    <div class="row">
                        <div class="col-sm-2 col-xs-12">
                            <div class="form-group">
                                <input v-model="searchStr" v-on:keyup="search()" type="text" class="form-control input-sm" placeholder="търсене" >
                            </div>
                        </div>

                        <div  v-show="!searchStr" class="col-sm-10 col-xs-12 text-primary">
                            търсене по id, име, емейл, телефон
                        </div>                        
                        <div  v-show="searchStr && searchStr.length > 0 && searchCount > 0" class="col-sm-10 col-xs-12">
                            <span class="text-success">{{ searchCount }}</span> резултата за <i>{{ searchStr }}</i>
                        </div>
                        <div  v-show="searchStr && searchStr.length > 0 && searchCount == 0" class="col-sm-10 col-xs-12">
                         <span class="text-danger">няма</span> резултати за <i>{{ searchStr }}</i>
                        </div>
                    </div>

                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Име</th>
                            <th>Е-мейл</th>
                            <th>Телефон</th>
                            <th>Роля</th>
                            <th>Поръчки</th>
                            <th>Действия</th>
                        </tr>
                        <tr v-for="item in items" is="item" :item="item"></tr>
                    </table>

                    <template id="template-item">
                        <tr>
                            <td>
                                {{item.id}}
                            </td>
                            <td>
                                {{item.name}}
                            </td>
                            <td>
                                {{item.email}}
                            </td>
                            <td>
                                {{item.phone}}
                            </td>
                            <td>
                                {{item.role}}
                            </td>
                            <td>
                                {{item.ordersCount}}
                            </td>
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/<?php echo e($controller); ?>/edit/{{item.id}}">
                                   Редактирай
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/<?php echo e($controller); ?>/delete/{{item.id}}" onclick="return confirm('ИЗТРИВАНЕ на този запис?')">
                                   Изтрий
                                </a>
                            </td>
                        </tr>

                    </template>

                    <div class="pagination mx-auto">
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.prev_item_url)"
                                :disabled="!pagination.prev_item_url">
                            <i class="fa fa-caret-left"></i>
                        </button>
                        <span>Страница {{pagination.current_item}} от {{pagination.last_item}}</span>
                        <button class="btn btn-default" v-on:click="fetchItems(pagination.next_item_url)"
                                :disabled="!pagination.next_item_url"><i class="fa fa-caret-right"></i>
                        </button>
                    </div>
                    <br> 
                <!-- <a href="<?php echo e(url('manage/'.$controller.'/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Добавяне на нов запис</a> -->
                </div>

                    <script type="text/javascript">
                    Vue.component('item', {
                        template: '#template-item',
                        props: ['item'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            items: [],
                            pagination: {},
                            searchStr: null
                        },
                        ready: function () {
                            this.fetchItems()
                        },
                        computed: {
                            searchCount: function(){
                                if(this.searchStr != null && this.searchStr.length > 0) {
                                    return this.items.length;
                                }
                            }
                        },
                        methods: {
                            search: function(){
                                let vm = this;
                                vm.fetchItems('<?php echo e(url("/api/".$controller)); ?>/?search='+vm.searchStr)
                            },
                            fetchItems: function(item_url) {
                                let vm = this;
                                item_url =  item_url || '<?php echo e(url("/api/".$controller)); ?>'
                                this.$http.get(item_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('items', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_item: data.current_page,
                                    last_item: data.last_page,
                                    next_item_url: data.next_page_url,
                                    prev_item_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            }
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>