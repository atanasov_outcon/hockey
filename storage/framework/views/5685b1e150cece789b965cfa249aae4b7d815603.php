<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">News
                        <a href="news/add" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add new</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchNews(pagination.prev_new_url)"
                                :disabled="!pagination.prev_new_url">
                            Previous
                        </button>
                        <span>New {{pagination.current_new}} of {{pagination.last_new}}</span>
                        <button class="btn btn-default" @click="fetchNews(pagination.next_new_url)"
                                :disabled="!pagination.next_new_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Slug</th>
                            <th>Menu</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="new in news" is="new" :new="new"></tr>
                    </table>

                    <template id="template-new">
                        <tr>
                            <td>
                                {{new.id}}
                            </td>
                            <td class="col-md-6">
                                {{new.title}}
                            </td>
                            <td>
                                {{new.slug}}
                            </td>                           
                            <td>
                                {{new.menu}}
                            </td>
                            <td>
                                <span v-if="new.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="news/edit/{{new.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="news/delete/{{new.id}}" onclick="return confirm('Are you sure you want to DELETE this new?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>

        
                <a href="news/add" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add new</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('new', {
                        template: '#template-new',
                        props: ['new'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            news: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchNews()
                        },
                        methods: {
                            fetchNews: function (new_url) {
                                let vm = this;
                                new_url = new_url || '/api/news'
                                this.$http.get(new_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('news', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_slider: data.current_page,
                                    last_slider: data.last_page,
                                    next_slider_url: data.next_page_url,
                                    prev_slider_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>