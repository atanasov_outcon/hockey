    

    <?php $__env->startSection('content'); ?>
        <div class="row">
           <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix"><?php echo e($itemTitle); ?>

                            <a href="<?php echo e(url('manage/'.$controller.'/add')); ?>" class="btn btn-primary btn-xs pull-right hidden-xs"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a>
                    </div>

                    <div class="panel-body">
                    
                    <div class="container-vue">
                        

                        <div class="row">
                            <div class="col-sm-2 col-xs-12">
                                <div class="form-group">
                                    <input v-model="searchStr" v-on:keyup="search()" type="text" class="form-control input-sm" placeholder="търсене" >
                                </div>
                            </div>

                            <div  v-show="!searchStr" class="col-sm-10 col-xs-12 text-primary">
                                търсене по id, sku, barcode, име, описание
                            </div>                        
                            <div  v-show="searchStr && searchStr.length > 0 && searchCount > 0" class="col-sm-10 col-xs-12">
                                <span class="text-success">{{ searchCount }}</span> резултата за <i>{{ searchStr }}</i>
                            </div>
                            <div  v-show="searchStr && searchStr.length > 0 && searchCount == 0" class="col-sm-10 col-xs-12">
                             <span class="text-danger">няма</span> резултати за <i>{{ searchStr }}</i>
                            </div>
                        </div>
                            
                        <table class="table table-striped">
                            <tr>
                                <th class="hidden-xs">#</th>
                                <th class="hidden-xs">SKU</th>
                                <th>Име</th>
                                <th><i class="fa fa-money fa-lg fa-fw"></i><span class="hidden-xs"> Цена</span></th>
                                <th class="visible-lg visible-md">slug</th>
                                <th class="visible-lg">Категория</th>
                                <th class="text-right visible-md visible-lg">Брой</th>
                                <th class="text-center"><i class="fa fa-eye fa-lg fa-fw"></i><span class="visible-lg-inline visible-md-inline"> Статус</span></th>
                                <th class="text-center"><i class="fa fa-cog fa-lg fa-fw"></i><span class="hidden-xs"> Действия</span></th>
                            </tr>
                            <tr v-for="item in items" is="item" :item="item"></tr>
                        </table>

                        <template id="template-item">
                            <tr>
                                <th class="hidden-xs">
                                {{item.id}}
                                </td>                            
                                <td class="hidden-xs">
                                    {{ ( item.sku ? item.sku : '-' ) }}
                                </td>
                                <td>
                                    {{item.title}}
                                </td>
                                <td>
                                    <span v-if="item.discount > 0">{{ formatBalance(item.discount) }} лв.
                                        <span class="linethr hidden-xs">{{ formatBalance(item.price) }} лв.</span>
                                    </span>
                                    <span v-else="item.discount == 0">{{ formatBalance(item.price) }} лв.</span>
                                    

                                </td>
                                <td class="visible-lg visible-md small">
                                    {{item.slug}}
                                </td>
                                <td class="visible-lg">
                                    {{item.categoryTitle}}
                                </td>
                                <td class="visible-md visible-lg text-right">
                                    {{item.quantity}}
                                </td>
                                <td class="text-center">
                                    <span v-bind:class="[ ( item.active == 1 ) ? 'label-success' : 'label-danger', 'label' ]">
                                        &nbsp;
                                        <span class="hidden-xs hidden-sm">активен</span>
                                        &nbsp;
                                    </span>
                                </td>                           
                                <td class="text-center">
                                    <a class="btn btn-primary btn-xs" href="<?php echo e(url('/manage')); ?>/<?php echo e($controller); ?>/edit/{{item.id}}">
                                       <i class="fa fa-pencil fa-lg fa-fw"></i><span class="hidden-xs"> Редактирай</span>
                                    </a>                                
                                    <a class="btn btn-danger btn-xs" v-on:click="deleteItem(item)" >
                                       <i class="fa fa-trash-o fa-lg fa-fw"></i><span class="hidden-xs"> Изтрий</span>
                                    </a>
                                </td>
                            </tr>
                        </template>

                        <div class="pagination mx-auto">
                            <button class="btn btn-default" v-on:click="fetchItems(pagination.prev_item_url)"
                                    :disabled="!pagination.prev_item_url">
                                <i class="fa fa-caret-left"></i>
                            </button>
                            <span>Страница {{pagination.current_item}} от {{pagination.last_item}}</span>
                            <button class="btn btn-default" v-on:click="fetchItems(pagination.next_item_url)"
                                    :disabled="!pagination.next_item_url"><i class="fa fa-caret-right"></i>
                            </button>
                        </div>
                        <br> 
                    <a href="<?php echo e(url('manage/'.$controller.'/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-circle fa-fw"></i> Добавяне на нов запис</a>
                    </div>

                        <script type="text/javascript">
                        Vue.component('item', {
                            template: '#template-item',
                            props: ['item'],
                            methods: {
                                formatBalance: function(value) {
                                    let val = (value/1).toFixed(2).replace('.', ',')
                                    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                                },                            
                                deleteItem: function(item) {
                                    let vm = this;
                                    let item_id = item.id;


                                    console.log(app.items);
                                    item_url =  '<?php echo e(url("/manage/".$controller)); ?>/delete/'+item_id
                                    this.$http.get(item_url)
                                        .then(function (response) {
                                            app.items.$remove(item);
                                        }, function() {
                                            alert('gre6ka')
                                        });
                                },
                            }
                        })

                        const app = new Vue({
                            el: '.container-vue',
                            data: {
                                items: [],
                                pagination: {},
                                searchStr: null
                            },
                            ready: function () {
                                this.fetchItems()
                            },
                            computed: {
                                searchCount: function(){
                                    if(this.searchStr != null && this.searchStr.length > 0) {
                                        return this.items.length;
                                    }
                                }
                            },
                            methods: {
                                search: function(){
                                    let vm = this;
                                    vm.fetchItems('<?php echo e(url("/api/".$controller)); ?>/?search='+vm.searchStr)
                                },
                                fetchItems: function(item_url) {
                                    let vm = this;
                                    item_url =  item_url || '<?php echo e(url("/api/".$controller)); ?>'
                                    this.$http.get(item_url)
                                        .then(function (response) {
                                            vm.makePagination(response.data)
                                            vm.items = response.data.data;
                                        });
                                },
                                makePagination: function(data){
                                    let pagination = {
                                        current_item: data.current_page,
                                        last_item: data.last_page,
                                        next_item_url: data.next_page_url,
                                        prev_item_url: data.prev_page_url
                                    }
                                    this.pagination = pagination
                                },                            
                                formatBalance: function(value) {
                                    let val = (value/1).toFixed(2).replace('.', ',')
                                    return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                                }
                            }
                        });   

                    </script>





                    </div>
                </div>
            </div>
        </div>
    <?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>