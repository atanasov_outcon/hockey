<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php if(isset($metaTitle) == true): ?> <?php echo e($metaTitle); ?> | <?php endif; ?> <?php echo e(config('app.name', 'UnicomsGateway')); ?></title>
    <meta name="description" content="<?php if(isset($metaDescription) == true): ?> <?php echo e($metaDescription); ?> <?php endif; ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>


        <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    

        <!-- include libraries(jQuery, bootstrap) -->
    <script src="js/vendor/modernizr.min.js"></script> 
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
    <script src="js/skrollr.min.js"></script> 



    <!-- Styles -->
    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/normalize.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">


</head>


<body>



    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <header>
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">
                        <img alt="Brand" src="img/logo.png">
                      </a>
                </div>
                <div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#section4">Цени и пакети</a></li>
                            <li><a href="#section5">Функции</a></li>
                            <li><a href="#section6">Помощ</a></li>
                            <li><a href="#section3">Активация</a></li>
                            <li><a href="#">Вход &nbsp;<i class="fa fa-user-o" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </header>
<div id="section1">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="20000" data-pause="false">
        <div class="carousel-inner" role="listbox">
            <div class="item active" style="background: url('img/slide-1.jpg');">
                <div class="carousel-caption">
                    <div class="carousel-text-bg">
                    <h2>ТЕЗИ, КОИТО СА ДОСТАТЪЧНО ЛУДИ <br> ДА МИСЛЯТ, ЧЕ МОГАТ ДА ПРОМЕНЯТ СВЕТА,<br> СА ТЕЗИ, КОИТО ГО ПРАВЯТ</h2>
                    <h3>Стив Джобс</h3>
                    <br>
                    <a href="#" class="btn btn-red btn-lg" role="button">РЕГИСТРИРАЙ СЕ</a></div>
                </div>
            </div>
            <div class="item" style="background: url('img/slide-2.jpg');">
                <div class="carousel-caption">
                    <div class="carousel-text-bg">
                    <h2>ТЕЗИ, КОИТО СА ДОСТАТЪЧНО ЛУДИ <br> ДА МИСЛЯТ, ЧЕ МОГАТ ДА ПРОМЕНЯТ СВЕТА,<br> СА ТЕЗИ, КОИТО ГО ПРАВЯТ</h2>
                    <h3>Стив Джобс</h3>
                    <br>
                    <a href="#" class="btn btn-red btn-lg" role="button">РЕГИСТРИРАЙ СЕ</a></div>
                </div>
            </div>
        </div>
        <div class="carousel-label">
            <h2><span style="text-transform: none;">UnyQ</span><strong>TV</strong> - платформа за Мултискрийн Телевизия</h2>
        </div>
    </div>
</div>

    <div id="section2" class="container text-center">
        <h2 class="section-title">Какво е <span style="color: #E3313B; text-transform: none;">UnyQ</span>TV?</h2>
        <p class="section-p"><span>UnyQ</span><strong>TV</strong> e мобилно приложение за телевизия на живо за iOS и Android устройства в OTT (Over-the-top) среда</p>
        <h3 class="section-subtitle">Разпространение:</h3>
        <p class="section-p"><strong>ЧРЕЗ КАБЕЛНИТЕ И ИНТЕРНЕТ ОПЕРАТОРИ</strong> – Услугата <span>UnyQ</span><strong>TV</strong> се разпространява единствено чрез операторите на телевизионна и интернет услуга. </p>
        <p class="section-p">Услугата не е възможна за директна онлайн продажба!</p>
        <p class="section-p">За да гледате услугата, моля свържете се с вашия кабелен или интернет оператор!</p>
        <h3 class="section-subtitle">Устройства: </h3>
        <p class="section-p">Приложението <span>UnyQ</span><strong>TV</strong> е предназначено за всички устройства с операционни системи: Android с версия 4.2 и по-висока, iOS с версия 7.0 и по-висока, включително Android IPTV STB</p>
        <div class="margin50"></div>    
        
        <div class="download">
            <h2 class="section-title">Изтегли от:</h2>
            <div style="text-align: center;">
            &nbsp;
            <a href="https://itunes.apple.com/bg/app/unyqtv/id1189135460?mt=8" target="_blank"><img src="img/apple-grey.png"></a>
            &nbsp;
            <a href="https://play.google.com/store/apps/details?id=com.unyqtv.live&hl=en" target="_blank"><img src="img/google-grey.png"></a>
            </div>
        </div>
            <div class="margin50"></div>
          </div>
        <div class="l-white-bg">    
        <div class="container text-center">
        <div class="margin50"></div>
        <div class="row hover12">
            <div class="col-md-4">
                <figure><img src="img/circle-1.png" class="img-responsive img-circle  center-block" alt="Cinque Terre"></figure>
                <h3 class="circle-h3-s2"><span style="color: #E3313B !important; font-family: 'LatoBlack'; text-transform: none;">UnyQ</span><strong>TV</strong></h3>
                <p class="circle-text-s2">Гледайте телевизия навсякъде, по всяко време и на всички устройства</p>
            </div>
            <div class="col-md-4">
                <figure><img src="img/circle-2.png" class="img-responsive  center-block" alt="Cinque Terre"></figure>
                <h3 class="circle-h3-s2">До 7 дни назад</h3>
                <p class="circle-text-s2">Може да гледате Вашите любими предавания до 7 дни назад</p>
            </div>
            <div class="col-md-4">
                <figure><img src="img/circle-3.png" class="img-responsive  center-block" alt="Cinque Terre"></figure>
                <h3 class="circle-h3-s2">ТВ Програма</h3>
                <p class="circle-text-s2">Имате инфомрация за текущото предаване или такива, които са били вече излъчени</p>
            </div>
        </div>
        <div class="margin30"></div>
        </div>
        </div>
<div class="parallax">
    <div class="h2-holder">
        <h2><span style="text-transform: none;">UnyQ</span><strong>TV</strong><br>
КОГАТО КЛАСИКАТА СРЕЩА МОДЕРНОТО</h2>
    </div>
</div>
<div class="section2-2 text-center container">
    <h2 class="section-title"><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV мобилна телевизия!</h2>
    <h3 class="section-subtitle-2">Разчупи традицията!</h3>
    <p class="section-p">Гледай любимите си канали и филми навсякъде и по всяко време от своя мобилен телефон или таблет! Вече не само на традиционния ТВ приемник, а защо не и на дивана, на балкона, в двора, на път, на плажа, в планината, в парка! Вече няма значение къде си – с <span>UnyQ</span><strong> TV</strong>получаваш достъп над ТВ съдържанието за което плащаш и гледаш когато искаш и където искаш!</p>
    <br>
    <img src="img/platform-3.png" class="img-responsive center-block" alt="Cinque Terre">
</div>
<div id="skrollr-body">
    <div id="section3" class="container activation-section">
        <h2 class="section-title text-center">Активация</h2>
        <h3 class="text-center">Активирай услугата <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong>? като следваш инструкциите по-долу!
</h3>
        <div id="scrollr-section-1">
        <div class="row">
            <div class="col-md-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-1">
                    <img src="img/activate-1.jpg" class="img-responsive img-circle center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6">
                <div class="activate-desc">
                    <div class="desc-number">
                        <span>1</span>
                    </div>
                    <p>Свържи се с вашия оператор и заяви услугата <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong> – самостоятелно или в пакета, предлаган от вашия оператор!</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;"  data-anchor-target="#scrollr-section-1">
                    <img src="img/activate-2.jpg" class="img-responsive img-circle  center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="activate-desc text-right pull-right">
                    <div class="desc-number pull-right">
                        <span>2</span>
                    </div>
                    <div class="clearfix"></div>
                    <p class="pull-right">Иди в офиса на вашия оператор и сключи договор за услугата <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong>.</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-1">
                    <img src="img/activate-3.jpg" class="img-responsive img-circle  center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6">
                <div class="activate-desc">
                    <div class="desc-number">
                        <span>3</span>
                    </div>
                    <p>Регистрирай се в портала <a href="index.php">www.unyqtv.com</a> (използвай абонатния си номер или кода за активация, предоставени от вашия оператор)</p>
                    <a href="#" class="btn btn-red" role="button">РЕГИСТРИРАЙ СЕ</a>
                </div>
            </div>
        </div>
        </div>
        <div id="scrollr-section-2">
        <div class="row">
            <div class="col-md-6 col-md-push-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;" data-anchor-target="#scrollr-section-2">
                    <img src="img/activate-4.jpg" class="img-responsive img-circle  center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="activate-desc text-right pull-right">
                    <div class="desc-number pull-right">
                        <span>4</span>
                    </div>
                    <div class="clearfix"></div>
                    <p class="pull-right">Изтегли приложението <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong> за мобилни телефони и таблети от Google Play (за Android устройства) и от Apple App Store (за iOS устройства)</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-2">
                    <img src="img/activate-5.jpg" class="img-responsive img-circle  center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6">
                <div class="activate-desc">
                    <div class="desc-number">
                        <span>5</span>
                    </div>
                    <p>Вход - влез в приложението <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong> и въведи еднократно потребителското си име и парола от портала unyqtv.com </p>
                    <a href="#" class="btn btn-red" role="button">ВХОД</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-push-6 hover13">
                <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;" data-anchor-target="#scrollr-section-2">
                    <img src="img/activate-6.jpg" class="img-responsive img-circle  center-block" alt="Cinque Terre">
                </figure>
            </div>
            <div class="col-md-6 col-md-pull-6">
                <div class="activate-desc text-right pull-right">
                    <div class="desc-number pull-right">
                        <span>6</span>
                    </div>
                    <div class="clearfix"></div>
                    <p class="pull-right">Гледай любимите си предавания и канали навсякъде и по всяко време!</p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        </div>
    </div>
</div>
<div id="section4" class="container">
    <h2 class="section-title text-center"><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV Цени и пакети</h2>
    <p class="section-p">С <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong> получавате достъп до най-гледаните канали на вашия мобилен екран!
        <br>Light – 30+ канала, Smart – 40+ канала, UnyQ – 50+ канала + HBO + HBO Go.
    </p>
    <div class="margin50"></div>
    <h2 class="section-title text-center">ПРОГРАМИ</h2>
    <div class="row">
        <div class="col-md-4">
            <div class="paket-heading">
                <div class="inner-p-h">
                    <div class="paket-name">LIGHT</div>
                    <div class="paket-number">30+</div>
                    <div class="channel-label">канали</div>
                </div>
            </div>
            <div class="program-box">
                <div class="programs-section">
                    <div class="programs-title">
                        ПОЛИТЕМАТИЧНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt1-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kanal3.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bloomberg_tv_bg.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-2.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-world.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/evrokom.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ФИЛМИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-comedy.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-cinema.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kino-nova.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fox-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxlife-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxcrime-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-family.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/filmboxplus.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СВОБОДНО ВРЕМЕ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-lady.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/24kitchen-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tlc.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ohota-ribalka.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/wness.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        МУЗИКА
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/city-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/the-voice.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/planeta.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fentv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/magic-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/dm-sat.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СПОРТ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btvaction-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/edgesport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-sport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ringtv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/e1.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hd-bnt.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/automotor.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fightbox.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДОКУМЕНТАЛНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ngc-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/discovery_channel-logo.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/da-vinci.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/travel-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДЕТСКИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/cn-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/neokelodeon.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ducktv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ekids.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        НОВИНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/on-air-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tv-europa.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bit.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        18+
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hustler-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="paket-heading">
                <div class="inner-p-h">
                    <div class="paket-name">SMART</div>
                    <div class="paket-number">40+</div>
                    <div class="channel-label">канали</div>
                </div>
            </div>
            <div class="program-box">
                <div class="programs-section">
                    <div class="programs-title">
                        ПОЛИТЕМАТИЧНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt1-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kanal3.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bloomberg_tv_bg.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-2.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-world.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/evrokom.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ФИЛМИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-comedy.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-cinema.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kino-nova.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fox-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxlife-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxcrime-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-family.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/filmboxplus.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СВОБОДНО ВРЕМЕ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-lady.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/24kitchen-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tlc.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ohota-ribalka.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/wness.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        МУЗИКА
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/city-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/the-voice.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/planeta.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fentv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/magic-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/dm-sat.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СПОРТ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btvaction-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/edgesport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-sport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ringtv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/e1.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hd-bnt.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/automotor.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fightbox.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДОКУМЕНТАЛНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ngc-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/discovery_channel-logo.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/da-vinci.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/travel-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДЕТСКИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/cn-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/neokelodeon.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ducktv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ekids.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        НОВИНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/on-air-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tv-europa.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bit.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        18+
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hustler-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="paket-heading">
                <div class="inner-p-h">
                    <div class="paket-name">UNYQ</div>
                    <div class="paket-number">50+</div>
                    <div class="channel-label">канали</div>
                    <div class="more-channel"> +HBO
                        <br>+HBO GO</div>
                </div>
            </div>
            <div class="program-box">
                <div class="programs-section">
                    <div class="programs-title">
                        ПОЛИТЕМАТИЧНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt1-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kanal3.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bloomberg_tv_bg.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-2.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bnt-world.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/evrokom.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ФИЛМИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-comedy.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-cinema.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/kino-nova.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fox-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxlife-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/foxcrime-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/diema-family.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/filmboxplus.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СВОБОДНО ВРЕМЕ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btv-lady.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/24kitchen-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tlc.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ohota-ribalka.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/wness.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        МУЗИКА
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/city-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/the-voice.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/planeta.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fentv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/magic-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/dm-sat.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        СПОРТ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/btvaction-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/edgesport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/nova-sport.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ringtv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/e1.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hd-bnt.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/automotor.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/fightbox.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДОКУМЕНТАЛНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ngc-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/discovery_channel-logo.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/da-vinci.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/travel-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        ДЕТСКИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/cn-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/neokelodeon.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ducktv-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/ekids.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        НОВИНИ
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/on-air-hover.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/tv-europa.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/bit.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="programs-title">
                        18+
                        <br>
                    </div>
                    <div class="the-programs">
                        <div class="row">
                            <div class="col-xs-3">
                                <div class="tvLogo">
                                    <img src="img/programs/hustler-tv.png" class="img-responsive  center-block" alt="БНТ 1" data-cutv="1" data-stovr="1" data-npvr="1">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section5" class="container functions">
    <h2 class="section-title text-center">КАКВИ ФУНКЦИИ ИМА <span style="color: #E3313B; text-transform: none;">UnyQ</span>TV?</h2>
    <h3 class="red-subtitle text-center">КАКВО ПРЕДЛАГА ПЛАТФОРМАТА ЗА ПОТРЕБИТЕЛЯ?</h3>
    <br>
    <div class="upper-grey-text text-center">ПОДДЪРЖА УСТРОЙСТВА НА ОС: <strong>IOS (> 7.0) И ANDROID (> 4.2) ВКЛЮЧИТЕЛНО ANDROID IPTV STB</strong></div>
    <br>
    <h3 class="red-text text-center"><strong>Live-TV</strong></h3>
    <br>
    <p class="section-p-lg">Tелeвизия на живо - с <strong><span style="color: #E3313B; text-transform: none;">UnyQ</span>TV</strong> получаваш достъп до над 60 от най- гледаните български и чужди канали!</p>
    <br>
    <img src="img/screen-shots/screen-shot-1.png" class="img-responsive  center-block" alt="screen-shot-1">
    <br>
    <h3 class="red-subtitle text-center"><strong>СТРИЙМИНГ КЪМ ТВ ПРИЕМНИК ЧРЕЗ CHROMECAST И AIRPLAY</strong></h3>
    <br>
    <ul class="functions-ul text-center">
        <li><i class="fa fa-pause" aria-hidden="true"></i> Пауза</li>
        <li><i class="fa fa-forward" aria-hidden="true"></i> Връщане назад и превъртане напред; </li>
        <li><i class="fa fa-fast-backward" aria-hidden="true"></i> Връщане от началото на текущото предаване; </li>
        <li><i class="fa fa-play-circle-o" aria-hidden="true"></i> Връщане назад до точен момент от гледаното предаване; </li>
    </ul>
    <br>
    <img src="img/screen-shots/screen-shot-2.png" class="img-responsive  center-block" alt="screen-shot-2">
    <br>
    <h3 class="red-subtitle text-center"><strong>START-OVER/ TIME SHIFT</strong></h3>
    <br>
    <img src="img/screen-shots/screen-shot-3.png" class="img-responsive  center-block" alt="screen-shot-3">
    <br>
    <h3 class="red-subtitle text-center"><strong>CATCH-UP</strong></h3>
    <br>
    <p class="section-p-lg text-center">Гледай избрани предавания и канали на запис до 7 дни назад!</p>
    <br>
    <h3 class="red-subtitle text-center"><strong>SEARCH</strong></h3>
    <br>
    <p class="section-p-lg text-center">Търси любимо предаване и гледай на запис до 7 дни назад!</p>
    <br>
    <h3 class="red-subtitle text-center"><strong>EPG (ELECTRONIC PROGRAM GUIDE)</strong></h3>
    <br>
    <p class="section-p-lg text-center">Програмен справочник</p>
    <img src="img/screen-shots/screen-shot-4.png" class="img-responsive  center-block" alt="screen-shot-4">
    <br>
    <h3 class="red-subtitle text-center"><strong>USER GUIDE</strong></h3>
    <br>
    <img src="img/screen-shots/screen-shot-5.png" class="img-responsive  center-block" alt="screen-shot-5">
    <br>
</div>
<div id="section6" class="container">
    <h2 class="section-title text-center">ВЪПРОСИ И ОТГОВОРИ</h2>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Какви функции има <span style=" text-transform: none;">UnyQ</span>TV? </a><span class="glyphicon glyphicon-chevron-up pull-right"></span>
        </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Как да получа достъп до услугата и да се регистрирам? </a> <span class="glyphicon glyphicon-chevron-down pull-right"></span>
        </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Ще се отрази ли ползването на UNYQTV на трафика в <br class="visible-lg"> мобилния ми план?  </a><span class="glyphicon glyphicon-chevron-down pull-right"></span>
        </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">Какво да направя ако срещна проблем с приложението? </a> <span class="glyphicon glyphicon-chevron-down pull-right"></span>
        </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse5">Трябва ли да плащам допълнително за да гледам телевизия на<br class="visible-lg"> различни устройства?
  </a><span class="glyphicon glyphicon-chevron-down pull-right"></span>
        </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse6">Как да търся конкретен филм, предаване или новини?  </a><span class="glyphicon glyphicon-chevron-down pull-right"></span>
        </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#collapse7">Как мога да прекратя абонамента си? </a><span class="glyphicon glyphicon-chevron-down pull-right"></span>

        </h4>
            </div>
            <div id="collapse7" class="panel-collapse collapse">
                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum</div>
            </div>
        </div>
    </div>
</div>
<div id="section7" class="container news-section">
    <h2 class="section-title text-center">НОВИНИ</h2>
    <br>
    <div class="news-box">
        <div class="row">
            <div class="col-md-4">
                <img src="img/slide-2.jpg" class="img-responsive  center-block" alt="news-1">
            </div>
            <div class="col-md-8">
                <h3 class="red-subtitle">НОВИНА 1</h3>
                <p class="section-p-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p>
                <br>
                <a href="#" class="btn btn-red" role="button">ВИЖ ОЩЕ</a>
            </div>
        </div>
    </div>
    <div class="news-box">
        <div class="row">
            <div class="col-md-4">
                <img src="img/slide-1.jpg" class="img-responsive  center-block" alt="news-2">
            </div>
            <div class="col-md-8">
                <h3 class="red-subtitle">НОВИНА 2</h3>
                <p class="section-p-lg">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate</p>
                <br>
                <a href="#" class="btn btn-red" role="button">ВИЖ ОЩЕ</a>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="footer-box ">
        <div class="container">
            <h3>За нас  /  Правила за ползване на интернет сайта  /  Общи условия за ползване на услугата <span style="text-transform: none;">UnyQTV</span>
</h3>
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-boxes">
                        <img src="img/icons/phone-icon.png" class="img-responsive  center-block" alt="news-2">
                        <div class="text-contact-box">
                            <div class="contact-text-heading">Contact sales:</div>
                            (678) 999-3002
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="contact-boxes">
                        <img src="img/icons/mail-icon.png" class="img-responsive  center-block" alt="news-2">
                        <div class="text-contact-box">
                            <div class="contact-text-heading">Contact:</div>
                            For comments or questions
                            <br> contact us
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="all-rights ">
        <div class="container">
            <p>© Copyright 2017. All Rights Reserved.</p>
            <ul class="social-network social-circle">
                <li><a href="#" class="icoLinkedin" title="Linkedin"><i class="fa fa-linkedin-square"></i></a></li>
                <li><a href="#" class="icoFacebook" title="Facebook"><i class="fa fa-facebook"></i></a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</footer>
<script>
window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')
</script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>


</body>
</html>
