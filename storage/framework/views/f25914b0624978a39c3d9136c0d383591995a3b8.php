<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Questions
                        <a href="questions/add" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add question</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchQuestions(pagination.prev_question_url)"
                                :disabled="!pagination.prev_question_url">
                            Previous
                        </button>
                        <span>Question {{pagination.current_question}} of {{pagination.last_question}}</span>
                        <button class="btn btn-default" @click="fetchQuestions(pagination.next_question_url)"
                                :disabled="!pagination.next_question_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Menu</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="question in questions" is="question" :question="question"></tr>
                    </table>

                    <template id="template-question">
                        <tr>
                            <td>
                                {{question.id}}
                            </td>
                            <td class="col-md-6">
                                {{question.title}}
                            </td>
                            <td>
                                {{question.menu}}
                            </td>
                            <td>
                                <span v-if="question.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="questions/edit/{{question.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="questions/delete/{{question.id}}" onclick="return confirm('Are you sure you want to DELETE this question?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>

        
                <a href="questions/add" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add question</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('question', {
                        template: '#template-question',
                        props: ['question'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            questions: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchQuestions()
                        },
                        methods: {
                            fetchQuestions: function (question_url) {
                                let vm = this;
                                question_url = question_url || '/api/questions'
                                this.$http.get(question_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('questions', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_slider: data.current_page,
                                    last_slider: data.last_page,
                                    next_slider_url: data.next_page_url,
                                    prev_slider_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>