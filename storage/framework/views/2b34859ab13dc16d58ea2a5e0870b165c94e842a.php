<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sponsors
                        <a href="<?php echo e(url('manage/sponsors/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add sponsor</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchSponsors(pagination.prev_sponsor_url)"
                                :disabled="!pagination.prev_sponsor_url">
                            Previous
                        </button>
                        <span>Sponsor {{pagination.current_sponsor}} of {{pagination.last_sponsor}}</span>
                        <button class="btn btn-default" @click="fetchSponsors(pagination.next_sponsor_url)"
                                :disabled="!pagination.next_sponsor_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="sponsor in sponsors" is="sponsor" :sponsor="sponsor"></tr>
                    </table>

                    <template id="template-sponsor">
                        <tr>
                            <td>
                                {{sponsor.id}}
                            </td>
                            <td class="col-md-6">
                                {{sponsor.title}}
                            </td>
                            <td>
                                <span v-if="sponsor.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/sponsors/edit/{{sponsor.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/sponsors/delete/{{sponsor.id}}" onclick="return confirm('Are you sure you want to DELETE this sponsor?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>
                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchSponsors(pagination.prev_sponsor_url)"
                                :disabled="!pagination.prev_sponsor_url">
                            Previous
                        </button>
                        <span>Sponsor {{pagination.current_sponsor}} of {{pagination.last_sponsor}}</span>
                        <button class="btn btn-default" @click="fetchSponsors(pagination.next_sponsor_url)"
                                :disabled="!pagination.next_sponsor_url">Next
                        </button>
                    </div>

                    <br>
        
                <a href="<?php echo e(url('manage/sponsors/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add sponsor</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('sponsor', {
                        template: '#template-sponsor',
                        props: ['sponsor'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            sponsors: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchSponsors()
                        },
                        methods: {
                            fetchSponsors: function (sponsor_url) {
                                let vm = this;
                                sponsor_url = sponsor_url || '<?php echo e(url("/api/sponsors")); ?>'
                                this.$http.get(sponsor_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('sponsors', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_sponsor: data.current_page,
                                    last_sponsor: data.last_page,
                                    next_sponsor_url: data.next_page_url,
                                    prev_sponsor_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>