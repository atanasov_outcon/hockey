<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
<base href="<?php echo e(url('/')); ?>/public/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
        
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo e(asset('public/images/favicon.png')); ?>">
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php if(isset($meta) == true): ?> <?php echo e($meta); ?> | <?php endif; ?> <?php echo e(config('app.name', 'Laravel')); ?></title>
    <meta name="description" content="<?php if(isset($metaDescription) == true): ?> <?php echo e($metaDescription); ?> <?php endif; ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>

        <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    
        <!-- include libraries(jQuery, bootstrap) -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>
    

    <script src="https://use.fontawesome.com/2b915e1407.js"></script>



    <!-- Styles -->
    <link href="<?php echo e(asset('public/css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/upgrade.css')); ?>" rel="stylesheet">


</head>

<style>
    
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container-fluid">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <?php echo e(config('app.name', 'Laravel')); ?>

                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        <?php if(Auth::guest()): ?>
                            <li><a href="<?php echo e(route('login')); ?>">Вход</a></li>
                            <!-- <li><a href="<?php echo e(route('register')); ?>">Регистрация</a></li> -->
                        <?php else: ?>

                            <?php if(Auth::user()->role = 'Administrator'): ?>

                            <li class="dropdown  <?php if(in_array($controller, ['codes', 'categories', 'attributes', 'vendors', 'collections']) == true ) echo "active"; ?> ">
                                <a class="dropdown-toggle " data-toggle="dropdown" role="button" aria-expanded="false">
                                   <i class="fa fa-book"></i> Каталог <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <li class="<?php if($controller == 'categories'): ?> active <?php endif; ?>"><a href="<?php echo e(route('categories')); ?>">
                                            <i class="fa fa-bars"></i> Категории</a>
                                        </li>
                                        <li class="<?php if($controller == 'attributes'): ?> active <?php endif; ?>"><a href="<?php echo e(route('attributes')); ?>">
                                            <i class="fa fa-paperclip"></i> Атрибути</a>
                                        </li>
                                        <li class="<?php if($controller == 'vendors'): ?> active <?php endif; ?>"><a href="<?php echo e(route('vendors')); ?>">
                                            <i class="fa fa-globe"></i> Производители</a>
                                        </li>
                                        <li class="<?php if($controller == 'collections'): ?> active <?php endif; ?>"><a href="<?php echo e(route('collections')); ?>">
                                            <i class="fa fa-calendar"></i> Колекции</a>
                                        </li>

                                        <li class="<?php if($controller == 'codes'): ?> active <?php endif; ?>"><a href="<?php echo e(route('codes')); ?>">
                                            <i class="fa fa-key"></i> Промо кодове</a>
                                        </li>
                                </ul>
                            </li>

                            <li class="<?php if($controller == 'products'): ?> active <?php endif; ?>"><a href="<?php echo e(route('products')); ?>">
                                <i class="fa fa-circle"></i> Продукти</a>
                            </li>

                            <li class="<?php if($controller == 'orders'): ?> active <?php endif; ?>"><a href="<?php echo e(route('orders')); ?>">
                                <i class="fa fa-shopping-basket"></i> Поръчки
                                <?php if($newOrders > 0): ?>
                                        <span class="label label-success"><?php echo e($newOrders); ?></span> 
                                <?php endif; ?></a>
                            </li>

                            <li class="<?php if($controller == 'users'): ?> active <?php endif; ?>"><a href="<?php echo e(route('users')); ?>">
                                <i class="fa fa-users"></i> Клиенти</a>
                            </li>
                            <?php endif; ?>

                            <li class="<?php if($controller == 'pages'): ?> active <?php endif; ?>"><a href="<?php echo e(route('pages')); ?>">
                                <i class="fa fa-calendar"></i> Страници</a>
                            </li>
                            
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                   <i class="fa fa-user"></i> <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Изход
                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container-fluid">
        <?php echo $__env->yieldContent('content'); ?>
        </div>

    </div>

    <script src="<?php echo e(asset('public/js/app.js')); ?>"></script>



</body>
</html>
