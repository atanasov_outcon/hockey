 <?php $__env->startSection('content'); ?>
<div id="section1">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="20000" data-pause="false">
        <div class="carousel-inner" role="listbox">
            <?php $__currentLoopData = $sliders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$slider): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php 
                if( $slider->images()->where('is_main', 1)->get() ){
                    $image = $slider->images()->where('is_main', 1)->first()->filename; 
                    $imagePath = 'uploads/sliders/'.$image;
                }
                else {
                    $imagePath = 'img/no-slider-image.jpg';
                }
            ?>
            <div class="item <?php if($i == 0) echo 'active'; ?>" style="background: url('<?php echo e($imagePath); ?>')">
                <div class="carousel-caption">
                    <div class="carousel-text-bg">
                        <h2><?php echo $slider->content; ?></h2>
                        <h3><?php echo $slider->slogan; ?></h3>
                        <br>
                        <a type="button" data-toggle="modal" data-target="#myModal" class="btn btn-red btn-lg">РЕГИСТРИРАЙ СЕ</a></div>
                </div>
            </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
        <div class="carousel-label">
            <h2><?php echo e($blocks['slogan']->title); ?></h2>
        </div>
    </div>
</div>
<div id="section2" class="">
    <div class="l-white-bg">
        <div class="container text-center">
            <div class="margin30"></div>
            <h2 class="section-title"><?php echo e($blocks['kakvo-e-unyqtv']->title); ?></h2>
            <div class="section-p"><?php echo $blocks['kakvo-e-unyqtv']->content; ?></div>
            <div class="margin50"></div>
            <div class="row">
                <div class="col-md-6">
                    <h3 class="section-subtitle-2"><?php echo $blocks['mobile-tv']->content; ?></h3>
                </div>
                <div class="col-md-6">
                    <img src="uploads/blocks/<?php echo e(isset($blocks['mobile-tv']->image->filename) ? $blocks['mobile-tv']->image->filename : 'no-image.jpg'); ?>" class="img-responsive center-block" alt="">
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center">
        <div class="margin50"></div>
        <h3 class="section-subtitle"><?php echo e($blocks['razprostranenie']->title); ?></h3>
        <div class="section-p"><?php echo $blocks['razprostranenie']->content; ?></div>
        <h3 class="section-subtitle"><?php echo e($blocks['ustroistva']->title); ?> </h3>
        <div class="section-p"><?php echo $blocks['ustroistva']->content; ?></div>
        <div class="margin50"></div>
        <div class="download">
            <h2 class="section-title">Изтегли от:</h2>
            <div style="text-align: center;">
                &nbsp;
                <a href="https://itunes.apple.com/bg/app/unyqtv/id1189135460?mt=8" target="_blank"><img src="img/apple-grey.png">
            </a> &nbsp;
                <a href="https://play.google.com/store/apps/details?id=com.unyqtv.live&hl=en" target="_blank"><img src="img/google-grey.png">
            </a>
            </div>
        </div>
    </div>
    <div class="margin50"></div>
</div>
<div class="l-white-bg">
    <div class="container text-center">
        <div class="margin50"></div>
        <h2 class="section-title text-center"><?php echo e($blocks['functions']->title); ?></h2>
    <h3 class="red-subtitle text-center"><?php echo $blocks['functions']->content; ?></h3>
        <div class="row hover12">
            <div class="col-md-4">
                <figure class="snip1205 red"><img src="uploads/blocks/<?php echo e(isset($blocks['unyqtv']->image->filename) ? $blocks['unyqtv']->image->filename : 'no-image.png'); ?>" class="img-responsive img-circle  center-block" alt="">
                <i class="fa fa-pause" aria-hidden="true"></i>
                </figure>
                <h3 class="circle-h3-s2"><?php echo e($blocks['unyqtv']->title); ?></h3>
                <p class="circle-text-s2"><?php echo $blocks['unyqtv']->content; ?></p>
            </div>
            <div class="col-md-4">
                <figure class="snip1205 red"><img src="uploads/blocks/<?php echo e(isset($blocks['do-7-dni-nazad']->image->filename) ? $blocks['do-7-dni-nazad']->image->filename : 'no-image.png'); ?>" class="img-responsive img-circle  center-block" alt="">
                <i class="fa fa-fast-backward" aria-hidden="true"></i>
                </figure>
                <h3 class="circle-h3-s2"><?php echo e($blocks['do-7-dni-nazad']->title); ?></h3>
                <p class="circle-text-s2"><?php echo $blocks['do-7-dni-nazad']->content; ?></p>
            </div>
            <div class="col-md-4">
                <figure class="snip1205 red"><img src="uploads/blocks/<?php echo e(isset($blocks['tv-programa']->image->filename) ? $blocks['tv-programa']->image->filename : 'no-image.png'); ?>" class="img-responsive img-circle  center-block" alt="">
                <i class="fa fa-television" aria-hidden="true"></i>
                </figure>
                <h3 class="circle-h3-s2"><?php echo e($blocks['tv-programa']->title); ?></h3>
                <p class="circle-text-s2"><?php echo $blocks['tv-programa']->content; ?></p>
            </div>
        </div>
        <div class="margin30"></div>
    </div>
</div>
<div class="parallax">
    <div class="h2-holder">
        <h2>UnyQTV <br> <?php echo $blocks['parallax']->title; ?></h2>
    </div>
</div>
<div id="skrollr-body">
    <div id="section3" class="container activation-section">
        <h2 class="section-title text-center"><?php echo e($blocks['activation']->title); ?></h2>
        <h3 class="text-center">
        <?php echo $blocks['activation']->content; ?>

        </h3>
        <div id="scrollr-section-1">
            <div class="row">
                <div class="col-md-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-1">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation1']->image->filename) ? $blocks['activation1']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6">
                    <div class="activate-desc">
                        <div class="desc-number">
                            <span>1</span>
                        </div>
                        <p>
                            <?php echo $blocks['activation1']->content; ?>

                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;" data-anchor-target="#scrollr-section-1">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation2']->image->filename) ? $blocks['activation2']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle  center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <div class="activate-desc text-right pull-right">
                        <div class="desc-number pull-right">
                            <span>2</span>
                        </div>
                        <div class="clearfix"></div>
                        <p class="pull-right">
                            <?php echo $blocks['activation2']->content; ?>

                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-1">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation3']->image->filename) ? $blocks['activation3']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle  center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6">
                    <div class="activate-desc">
                        <div class="desc-number">
                            <span>3</span>
                        </div>
                        <p>
                            <?php echo $blocks['activation3']->content; ?>

                        </p>
                        <a type="button" data-toggle="modal" data-target="#myModal" class="btn btn-red" role="button">РЕГИСТРИРАЙ СЕ</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="scrollr-section-2">
            <div class="row">
                <div class="col-md-6 col-md-push-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;" data-anchor-target="#scrollr-section-2">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation4']->image->filename) ? $blocks['activation4']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle  center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <div class="activate-desc text-right pull-right">
                        <div class="desc-number pull-right">
                            <span>4</span>
                        </div>
                        <div class="clearfix"></div>
                        <p class="pull-right">
                            <?php echo $blocks['activation4']->content; ?>

                        </p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-right: 250px;" data-center="margin-right: 5px;" data-anchor-target="#scrollr-section-2">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation5']->image->filename) ? $blocks['activation5']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle  center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6">
                    <div class="activate-desc">
                        <div class="desc-number">
                            <span>5</span>
                        </div>
                        <p>
                            <?php echo $blocks['activation5']->content; ?>

                        </p>
                        <a href="#" class="btn btn-red" role="button">ВХОД</a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-md-push-6 hover13">
                    <figure class="skrollable skrollable-after" data-250-top="margin-left: 250px;" data-center="margin-left: 5px;" data-anchor-target="#scrollr-section-2">
                        <img src="uploads/blocks/<?php echo e(isset($blocks['activation6']->image->filename) ? $blocks['activation6']->image->filename : 'no-image.jpg'); ?>" class="img-responsive img-circle  center-block" alt="">
                    </figure>
                </div>
                <div class="col-md-6 col-md-pull-6">
                    <div class="activate-desc text-right pull-right">
                        <div class="desc-number pull-right">
                            <span>6</span>
                        </div>
                        <div class="clearfix"></div>
                        <p class="pull-right"><?php echo $blocks['activation6']->content; ?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="section4" class="container">
    <h2 class="section-title text-center"><?php echo e($blocks['ceni-i-paketi']->title); ?></h2>
    <p class="section-p">
        <?php echo $blocks['ceni-i-paketi']->content; ?>

    </p>
    <div class="margin50"></div>
    <h2 class="section-title text-center">ПРОГРАМИ</h2>
    <div class="row">
    <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <div class="col-md-4">
                <div class="paket-heading">
                    <div class="inner-p-h">
                        <?php echo $product->content; ?>

                    </div>
                </div>
                <div class="program-box">
                    <?php $__currentLoopData = $product->channels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$channels): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                    <div class="programs-section">
                        <div class="programs-title">
                            <?php echo e($key); ?>

                            <br>
                        </div>
                        <div class="the-programs">
                            <div class="row">
                                <?php $__currentLoopData = $channels; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$channel): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <?php

                                    if( $channel->images()->where('is_main', 1)->first() ){
                                        $image = $channel->images()->where('is_main', 1)->first()->filename; 
                                        $imagePath = 'uploads/channels/'.$image;
                                    }
                                    else {
                                        $imagePath = 'uploads/channels/no-image.jpg';
                                    }

                                    $i++;
                                ?>
                                <div class="col-xs-3">
                                    <div class="tvLogo">
                                        <img src="<?php echo e($imagePath); ?>" class="img-responsive  center-block" alt="<?php echo e($channel->title); ?>" 
                                        <?php if($channel->func1 == 1): ?> data-cutv="1" <?php endif; ?>
                                        <?php if($channel->func2 == 1): ?> data-stovr="1" <?php endif; ?> 
                                        <?php if($channel->func3 == 1): ?> data-npvr="1" <?php endif; ?> 
                                        >
                                    </div>
                                </div>

                                    <?php if($i%4==0): ?> <div class="clearfix"></div> <?php endif; ?>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
      
    </div>
</div>

<!-- <div id="section5" class="container functions">
<div class="row">
	<div class="col-md-3">
		<img src="img/step-1.png" class="img-responsive  center-block" alt="step-one">
	</div>
</div>
</div> -->
<!-- <div id="section5" class="container functions">
    <h2 class="section-title text-center"><?php echo e($blocks['functions']->title); ?></h2>
    <h3 class="red-subtitle text-center"><?php echo $blocks['functions']->content; ?></h3>
    <br>
    <h3 class="red-text text-center"><strong><?php echo e($blocks['live-tv']->title); ?></strong></h3>
    <br>
    <div class="section-p-lg"><?php echo $blocks['live-tv']->content; ?></div>
    <br>
    <img src="uploads/blocks/<?php echo e(isset($blocks['live-tv']->image->filename) ? $blocks['live-tv']->image->filename : 'no-image.jpg'); ?>" class="img-responsive  center-block" alt="screen-shot-1">
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['stream']->title); ?></strong></h3>
    <br> <?php echo $blocks['stream']->content; ?>

    <br>
    <img src="uploads/blocks/<?php echo e(isset($blocks['stream']->image->filename) ? $blocks['stream']->image->filename : 'no-image.jpg'); ?>" class="img-responsive  center-block" alt="screen-shot-2">
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['start-over']->title); ?></strong></h3>
    <br>
    <img src="uploads/blocks/<?php echo e(isset($blocks['start-over']->image->filename) ? $blocks['start-over']->image->filename : 'no-image.jpg'); ?>" class="img-responsive  center-block" alt="screen-shot-3">
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['catch-up']->title); ?></strong></h3>
    <br>
    <div class="section-p-lg text-center"><?php echo $blocks['catch-up']->content; ?></div>
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['search']->title); ?></strong></h3>
    <br>
    <div class="section-p-lg text-center"><?php echo $blocks['search']->content; ?></div>
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['epg']->title); ?></strong></h3>
    <br>
    <div class="section-p-lg text-center"><?php echo $blocks['epg']->content; ?></div>
    <img src="uploads/blocks/<?php echo e(isset($blocks['epg']->image->filename) ? $blocks['epg']->image->filename : 'no-image.jpg'); ?>" class="img-responsive  center-block" alt="screen-shot-4">
    <br>
    <h3 class="red-subtitle text-center"><strong><?php echo e($blocks['user-guide']->title); ?></strong></h3>
    <br>
    <img src="uploads/blocks/<?php echo e(isset($blocks['user-guide']->image->filename) ? $blocks['user-guide']->image->filename : 'no-image.jpg'); ?>" class="img-responsive  center-block" alt="screen-shot-5">
    <br>
</div> -->
<div id="section6" class="container">
    <h2 class="section-title text-center">ВЪПРОСИ И ОТГОВОРИ</h2>
    <div class="panel-group" id="accordion">
        <?php $__currentLoopData = $questions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i=>$question): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo e($i); ?>"><?php echo e($question->title); ?></a><span class="glyphicon  <?php if($i==0): ?> glyphicon-chevron-up <?php else: ?> glyphicon-chevron-down <?php endif; ?>  pull-right"></span>
                </h4>
            </div>

            <div id="collapse<?php echo e($i); ?>" class="panel-collapse collapse <?php if($i==0): ?> in <?php endif; ?>">
                <div class="panel-body"><?php echo ($question->content); ?></div>
            </div>
        </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
</div>
<div id="section7" class="container news-section">
    <h2 class="section-title text-center">НОВИНИ</h2>
    <br> <?php $__currentLoopData = $news; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $new): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>

    <?php 
        if( $new->images()->where('is_main', 1)->first() ){
            $image = $new->images()->where('is_main', 1)->first()->filename; 
            $imagePath = 'uploads/news/'.$image;
        }
        else {
            $imagePath = 'uploads/news/no-image.jpg';
        }
    ?>
    <div class="news-box">
        <div class="row">
            <div class="col-md-4">
                <a href="<?php echo e(url('news')); ?>/<?php echo e($new->slug); ?>"><img src="<?php echo e($imagePath); ?>" class="img-responsive  center-block" alt="news-1"></a>
            </div>
            <div class="col-md-8">
                <a href="<?php echo e(url('news')); ?>/<?php echo e($new->slug); ?>"><h3 class="red-subtitle"><?php echo e($new->title); ?></h3></a>
                <p class="section-p-lg"><?php echo e(str_limit(strip_tags($new->content), 360, '...')); ?></p>
                <br> <?php if( strlen($new->content) > 360 ): ?>
                <a href="<?php echo e(url('news')); ?>/<?php echo e($new->slug); ?>" class="btn btn-red red-font-h" role="button">ВИЖ ОЩЕ</a> <?php endif; ?>
            </div>
        </div>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>