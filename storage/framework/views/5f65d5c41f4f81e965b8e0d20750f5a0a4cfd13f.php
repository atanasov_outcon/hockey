<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Channels
                        <a href="<?php echo e(url('manage/channels/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add channel</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchChannels(pagination.prev_channel_url)"
                                :disabled="!pagination.prev_channel_url">
                            Previous
                        </button>
                        <span>Channel {{pagination.current_channel}} of {{pagination.last_channel}}</span>
                        <button class="btn btn-default" @click="fetchChannels(pagination.next_channel_url)"
                                :disabled="!pagination.next_channel_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="channel in channels" is="channel" :channel="channel"></tr>
                    </table>

                    <template id="template-channel">
                        <tr>
                            <td>
                                {{channel.id}}
                            </td>
                            <td class="col-md-6">
                                {{channel.title}}
                            </td>
                            <td>
                                <span v-if="channel.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/channels/edit/{{channel.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/channels/delete/{{channel.id}}" onclick="return confirm('Are you sure you want to DELETE this channel?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>
                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchChannels(pagination.prev_channel_url)"
                                :disabled="!pagination.prev_channel_url">
                            Previous
                        </button>
                        <span>Channel {{pagination.current_channel}} of {{pagination.last_channel}}</span>
                        <button class="btn btn-default" @click="fetchChannels(pagination.next_channel_url)"
                                :disabled="!pagination.next_channel_url">Next
                        </button>
                    </div>

                    <br>
        
                <a href="<?php echo e(url('manage/channels/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add channel</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('channel', {
                        template: '#template-channel',
                        props: ['channel'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            channels: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchChannels()
                        },
                        methods: {
                            fetchChannels: function (channel_url) {
                                let vm = this;
                                channel_url = channel_url || '<?php echo e(url("/api/channels")); ?>'
                                this.$http.get(channel_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('channels', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_channel: data.current_page,
                                    last_channel: data.last_page,
                                    next_channel_url: data.next_page_url,
                                    prev_channel_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>