<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Products
                        <a href="<?php echo e(url('manage/products/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add product</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchProducts(pagination.prev_product_url)"
                                :disabled="!pagination.prev_product_url">
                            Previous
                        </button>
                        <span>Product {{pagination.current_product}} of {{pagination.last_product}}</span>
                        <button class="btn btn-default" @click="fetchProducts(pagination.next_product_url)"
                                :disabled="!pagination.next_product_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="product in products" is="product" :product="product"></tr>
                    </table>

                    <template id="template-product">
                        <tr>
                            <td>
                                {{product.id}}
                            </td>
                            <td class="col-md-6">
                                {{product.title}}
                            </td>
                            <td>
                                <span v-if="product.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>
                            </td>   
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/products/edit/{{product.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/products/delete/{{product.id}}" onclick="return confirm('Are you sure you want to DELETE this product?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>
                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchProducts(pagination.prev_product_url)"
                                :disabled="!pagination.prev_product_url">
                            Previous
                        </button>
                        <span>Product {{pagination.current_product}} of {{pagination.last_product}}</span>
                        <button class="btn btn-default" @click="fetchProducts(pagination.next_product_url)"
                                :disabled="!pagination.next_product_url">Next
                        </button>
                    </div>

                    <br>
        
                <a href="<?php echo e(url('manage/products/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add product</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('product', {
                        template: '#template-product',
                        props: ['product'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            products: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchProducts()
                        },
                        methods: {
                            fetchProducts: function (product_url) {
                                let vm = this;
                                product_url = product_url || '<?php echo e(url("/api/products")); ?>'
                                this.$http.get(product_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('products', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_slider: data.current_page,
                                    last_slider: data.last_page,
                                    next_slider_url: data.next_page_url,
                                    prev_slider_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>