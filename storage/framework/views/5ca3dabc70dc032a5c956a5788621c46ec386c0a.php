<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                <div class="panel-heading">Add channel</div>


                <div class="panel-body">
                   
                    <form method="POST">
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="title">Title <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Title" value="">
                      </div>                  

                      <div class="form-group">
                        <label for="slug">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug"  id="slug" placeholder="Slug" value="">
                      </div>                  

                      <div class="form-group">
                        <label for="content">Content <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <textarea required name="content" id="content" class="form-control" cols="30" rows="10"></textarea>
                      </div>                  

                      <div class="form-group">
                        <label for="metaTitle">Meta title <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="metaTitle"  id="metaTitle" placeholder="Meta title" value="">
                      </div>    

                      <div class="form-group">
                        <label for="metaDescription">Meta Description <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="metaDescription"  id="metaDescription" placeholder="Meta description" value="">
                      </div>    

                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="1" > Active
                        </label>
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                      <a href="<?php echo e(route('channels')); ?>" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Cancel</a>

                    </form>

                </div>
            </div>
        </div>        

    </div>
</div>



<script type="text/javascript">
const editor = $('#content').summernote();

$(document).ready(function() {
var editor;

// editor

   
  
// generate slug

  $( "#title" ).on('keyup', function() {
    var title = $(this).val();
    $('#slug').val(convertToSlug(title));
});



});


// convert text to slug
function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}
</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>