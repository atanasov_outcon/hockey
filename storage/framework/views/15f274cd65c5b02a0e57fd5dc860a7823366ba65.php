<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                <div class="panel-heading">Добавяне на нов запис</div>


                <div class="panel-body">
                   
                    <form method="POST">
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="code">Код <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="code" id="code" placeholder="Код" value="">
                      </div>      
                      <div class="form-group">
                        <label for="discount">Намаление <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i> <small class="text-info">цената е в проценти</small></label>
                        <input required type="text" class="form-control" name="discount" id="discount" placeholder="Намаление" value="">
                      </div>      
                 
  <hr>

                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Отказ</a>

                    </form>

                </div>
            </div>
        </div>        

    </div>
</div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>