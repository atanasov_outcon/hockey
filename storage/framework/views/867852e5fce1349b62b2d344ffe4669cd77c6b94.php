<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Pages
                        <a href="<?php echo e(url('manage/pages/add')); ?>" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add page</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchPages(pagination.prev_page_url)"
                                :disabled="!pagination.prev_page_url">
                            Previous
                        </button>
                        <span>Page {{pagination.current_page}} of {{pagination.last_page}}</span>
                        <button class="btn btn-default" @click="fetchPages(pagination.next_page_url)"
                                :disabled="!pagination.next_page_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Position</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="page in pages" is="page" :page="page"></tr>
                    </table>

                    <template id="template-page">
                        <tr>
                            <td>
                                {{page.id}}
                            </td>
                            <td class="col-md-6">
                                {{page.title}}
                            </td>
                            <td>
                                {{page.position}}
                            </td>  
                            <td>
                                <span v-if="page.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>    
                            </td>                     
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="<?php echo e(url('/manage')); ?>/pages/edit/{{page.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="<?php echo e(url('/manage')); ?>/pages/delete/{{page.id}}" onclick="return confirm('Are you sure you want to DELETE this page?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>

                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchPages(pagination.prev_page_url)"
                                :disabled="!pagination.prev_page_url">
                            Previous
                        </button>
                        <span>Page {{pagination.current_page}} of {{pagination.last_page}}</span>
                        <button class="btn btn-default" @click="fetchPages(pagination.next_page_url)"
                                :disabled="!pagination.next_page_url">Next
                        </button>
                    </div>

                    <br>
        
                <a href="<?php echo e(url('manage/pages/add')); ?>" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add page</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('page', {
                        template: '#template-page',
                        props: ['page'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            pages: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchPages()
                        },
                        methods: {
                            fetchPages: function (page_url) {
                                let vm = this;
                                page_url = page_url || '<?php echo e(url("/api/pages")); ?>'
                                this.$http.get(page_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('pages', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_page: data.current_page,
                                    last_page: data.last_page,
                                    next_page_url: data.next_page_url,
                                    prev_page_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            },
                            viewPage: function (id) {
                                let vm = this;
                                vm.$http.get('/api/page/' + id).then(function(response) {
                                    vm.$set('pages', response.data.data)
                                })
                                vm.$els.taskinput.focus()
                                vm.edit = true
                            }
                        }
                    });   

                </script>



                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>