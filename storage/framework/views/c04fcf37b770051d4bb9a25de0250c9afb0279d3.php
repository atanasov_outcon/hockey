<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-8 "> 
            <div class="panel panel-default">
                <div class="panel-heading">Редакция на <?php echo e($item->title); ?></div>


                <div class="panel-body">
                   
                    <form method="POST">
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="name">E-mail <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="email" id="email" placeholder="E-mail" value="<?=(isset($item->email)) ? $item->email : null;?>">
                      </div>  

                      <div class="form-group">
                        <label for="name">Име <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="name" id="name" placeholder="Име" value="<?=(isset($item->name)) ? $item->name : null;?>">
                      </div>   

        <hr>
   
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Отказ</a>
                     
                    </form>

                </div>
            </div>
        </div>        

        <div class="col-md-4"> 
            <div class="panel panel-default">
                <div class="panel-heading">Галерия</div>
                <div class="panel-body container-vue">
             
             <style>
            
             </style>       



                    <form id="ajax-upload" method="POST" enctype="multipart/form-data">
                     <?php echo e(csrf_field()); ?>



                    <div class="alert alert-success alert-upload-success alert-hidden">
                           Картинките са качени успешно!
                    </div>                    

                    <div class="alert alert-danger alert-upload-error alert-hidden">
                          Моля пробвай отново!
                    </div>
                

                      <div class="form-group">
                      <div class="spinLoader alert-hidden pull-right">качване <i class="fa fa-spinner fa-pulse fa-fw "></i> <span class="sr-only">качване...</span></div>
                        <label for="images">Добави изображения</label>
                        <input type="file" class="form-control" name="images[]" id="images" multiple>
                      </div>                  

                    </form>
                    
                   
                    <hr />

                    <div class="row ">

                            <div class="col-md-6 thumb" v-for="(index, image) in images" >
                                <span class="main-image" v-if="image.is_main==1" href="">главна снимка</span>

                                <div class="col-md-12 controls-bottom">
                                    <a v-on:click="setMainImage(image.id)" href="#" title="Set main image" class="btn-image-add tip" data-dismiss="alert">
                                    <span aria-hidden="true"><i class="fa fa-image" aria-hidden="true"></i></span>
                                    </a>  
                                  
                                  <a v-on:click="deleteImage(image.id)" href="#" title="Delete" class="btn-image-delete tip" data-dismiss="alert">
                                    <span aria-hidden="true"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                  </a>

                  
                                </div>                          
                                  <img :src="image.fullpath" class="img-responsive">

                                  <div v-if="index%2!=0" class="clearfix">&nbsp;</div>
                            </div>

                      <div class="clearfix"></div>
                      <hr>
                      <div class="container">
                        <small><i class="fa fa-image" aria-hidden="true"></i> - направи главна картинка</small> <br>
                        <small><i class="fa fa-trash" aria-hidden="true"></i> - изтрий</small> <br>
                      </div>


                    </div>


                   

                </div>
            </div>
        </div>
    </div>

<script type="text/javascript">
Vue.http.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
})




const gallery = new Vue({
    el: '.container-vue',
    data: {
        images: []
    },
    ready: function () {
        this.fetchUploads()
    },
    methods: {
        fetchUploads: function () {
            let vm = this;
            item_url =  "<?php echo e(url('/api/'.$controller.'/uploads/'.$item->id)); ?>"
            this.$http.get(item_url)
                .then(function (response) {
                    vm.$set('images', response.data.data)
                });
        },             
        deleteImage: function (image_id) {

          var doIt = confirm("Изтрий това изображение?");
          if (doIt == true) {
            let vm = this;
            item_url = "<?php echo e(url('/manage/'.$controller.'/delete_image/')); ?>/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
          }


        },      
        setMainImage: function (image_id) {
            let vm = this;
            item_url = "<?php echo e(url('/manage/'.$controller.'/set_main_image/'.$item->id)); ?>/"+image_id
            this.$http.get(item_url)
                .then(function (response) {
                    this.fetchUploads();
                });
        },           
        addToContent: function (img_url) {
            editor.summernote("insertImage", img_url);
        },
    }
});   

</script>


<script type="text/javascript">

$(document).ready(function() {

  $( "#ajax-upload" ).on('change', function(e) {
    e.preventDefault();
    $('.spinLoader').show();
    $.ajax({
        method: 'post',
        url: '<?php echo e(url("manage/".$controller."/upload/".$item->id)); ?>',
        data:  new FormData(this),
        contentType: false,
        cache: false,
        processData:false,
        success: function(response){
          $('.spinLoader').hide();
            $('.alert-upload-success').show().fadeOut(2000);
            $('#images').val('');
            gallery.fetchUploads(); // re fetch the images
        },
        error: function(data){
          $('.spinLoader').hide();
            $('.alert-upload-error').show().fadeOut(2000);
        },

    });

});




});

</script>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>