<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
<base href="http://webstart.dev/" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php if(isset($metaTitle) == true): ?> <?php echo e($metaTitle); ?> | <?php endif; ?> <?php echo e(config('app.name', 'UnicomsGateway')); ?></title>
    <meta name="description" content="<?php if(isset($metaDescription) == true): ?> <?php echo e($metaDescription); ?> <?php endif; ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>


        <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    
        <!-- include libraries(jQuery, bootstrap) -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.js"></script> 
    <script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.js"></script> 

    <!-- include summernote css/js-->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.6/summernote.js"></script>
    
    <!-- import CSS -->
    <link rel="stylesheet" href="https://unpkg.com/element-ui/lib/theme-default/index.css">

    <script src="https://use.fontawesome.com/2b915e1407.js"></script>



    <!-- Styles -->
    <link href="<?php echo e(asset('public/css/app.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/style.css')); ?>" rel="stylesheet">


</head>

<style>
    
</style>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="<?php echo e(url('/')); ?>">
                        <?php echo e(config('app.name', 'Laravel')); ?>

                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">


                        <!-- Authentication Links -->
                        <?php if(Auth::guest()): ?>
                            <li><a href="<?php echo e(route('login')); ?>">Login</a></li>
                            <li><a href="<?php echo e(route('register')); ?>">Register</a></li>
                        <?php else: ?>
                            <li class="<?php if($controller == 'sliders'): ?> active <?php endif; ?>"><a href="<?php echo e(route('sliders')); ?>">Slider</a></li>
                            <li class="<?php if($controller == 'blocks'): ?> active <?php endif; ?>"><a href="<?php echo e(route('blocks')); ?>">Homepage blocks</a></li>
                            <li class="<?php if($controller == 'pages'): ?> active <?php endif; ?>"><a href="<?php echo e(route('pages')); ?>">Pages</a></li>
                            <li class="<?php if($controller == 'news'): ?> active <?php endif; ?>"><a href="<?php echo e(route('news')); ?>">News</a></li>
                            <li class="<?php if($controller == 'channels'): ?> active <?php endif; ?>"><a href="<?php echo e(route('channels')); ?>">Channels</a></li>
                            <li class="<?php if($controller == 'products'): ?> active <?php endif; ?>"><a href="<?php echo e(route('products')); ?>">Products</a></li>
                            <li class="<?php if($controller == 'questions'): ?> active <?php endif; ?>"><a href="<?php echo e(route('questions')); ?>">Friendly Asked Questions</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    <?php echo e(Auth::user()->name); ?> <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="<?php echo e(route('logout')); ?>"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="<?php echo e(route('logout')); ?>" method="POST" style="display: none;">
                                            <?php echo e(csrf_field()); ?>

                                        </form>
                                    </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="container">
        <?php echo $__env->yieldContent('content'); ?>
        </div>

    </div>

    <script src="<?php echo e(asset('public/js/app.js')); ?>"></script>



</body>
</html>
