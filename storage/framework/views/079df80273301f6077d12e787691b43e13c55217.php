<?php $__env->startSection('content'); ?>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">


<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Начало</a></li>
  <li class="breadcrumb-item active">Новини</li>
</ol>


                <div class="container-vue">


                        <div v-for="new in news" is="new" :new="new"></div>

                    <template id="template-new">
                        <div class="row" >
                                
                                <a href="news/{{new.slug}}">
                                <img class="img-responsive pull-left col-xs-4" src="uploads/news/{{new.image}}" alt="{{new.title}}">
                                </a>

                                <div class="pull-right col-xs-8">
                                  <a href="news/{{new.slug}}">{{new.title}}</a>
                                  {{{new.content}}}
                                </div>

                            </div>


                            <hr>
                    </template>

        
                <div class="pagination" v-if="pagination.last_new > 1">
                    <button class="btn btn-pagination" @click="fetchNews(pagination.prev_new_url)"
                            :disabled="!pagination.prev_new_url">
                        Предишна
                    </button>
                    <span>&nbsp;&nbsp;&nbsp;Страница {{pagination.current_new}} от {{pagination.last_new}}&nbsp;&nbsp;&nbsp;</span>
                    <button class="btn btn-pagination" @click="fetchNews(pagination.next_new_url)"
                            :disabled="!pagination.next_new_url">Следваща
                    </button>
                </div>


                </div>

                    <script type="text/javascript">
                    Vue.component('new', {
                        template: '#template-new',
                        props: ['new'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            news: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchNews()
                        },
                        methods: {
                            fetchNews: function (new_url) {
                                let vm = this;
                                new_url = new_url || '/api/news'
                                this.$http.get(new_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('news', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_new: data.current_page,
                                    last_new: data.last_page,
                                    next_new_url: data.next_page_url,
                                    prev_new_url: data.prev_page_url
                                }
                                this.$set('pagination', pagination)
                            } 
                        }
                    });   

                </script>





        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>