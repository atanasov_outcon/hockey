<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                <div class="panel-heading clearfix">Добавяне на нов запис 
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-xs btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Отказ</a>
                    </span>
                </div>
                <div class="panel-body">
                   
                    <form method="POST">
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="">
                      </div>      

                      <div class="form-group">
                        <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="">
                      </div>     

                      <div class="form-group">
                        <label for="title">Подкатегория на <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <select type="text" class="form-control" name="parent_id" id="parent_id" placeholder="Категория">
                           <option value="0" selected>главна категория</option>
                          <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($category->id); ?>"><?php echo e($category->title); ?></option>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </select>
                      </div> 
                      
                      <div class="form-group">
                        <label for="content">Описание</label>
                        <textarea name="content" id="content_" class="form-control" cols="30" rows="5"></textarea>
                      </div>                  
  <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="1" > Активен
                        </label>
                      </div>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-floppy-o" aria-hidden="true"></i> Отказ</a>

                    </form>

                </div>
            </div>
        </div>        

    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>