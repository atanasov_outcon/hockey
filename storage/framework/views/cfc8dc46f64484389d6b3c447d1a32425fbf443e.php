<?php $__env->startSection('content'); ?>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sections
                        <a href="sections/add" class="btn btn-primary btn-xs pull-right"><i class="fa fa-plus-o" aria-hidden="true"></i> Add section</a>
                </div>

                <div class="panel-body">
                
                <div class="container-vue">


                    <div class="pagination">
                        <button class="btn btn-default" @click="fetchSections(pagination.prev_section_url)"
                                :disabled="!pagination.prev_section_url">
                            Previous
                        </button>
                        <span>Page {{pagination.current_section}} of {{pagination.last_section}}</span>
                        <button class="btn btn-default" @click="fetchSections(pagination.next_section_url)"
                                :disabled="!pagination.next_section_url">Next
                        </button>
                    </div>



                    <table class="table table-striped">
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Position</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        <tr v-for="sectioncomp in sections" is="section" :section="section"></tr>
                    </table>

                    <template id="template-section">
                        <tr>
                            <td>
                                {{section.id}}
                            </td>
                            <td class="col-md-6">
                                {{section.title}}
                            </td>
                            <td>
                                {{section.position}}
                            </td>  
                            <td>
                                <span v-if="section.active == 1" class="label label-success">active</span>
                                <span v-else class="label label-danger">active</span>    
                            </td>                     
                            <td>
                                                                   
                                <a class="btn btn-xs btn-primary" href="sections/edit/{{section.id}}">
                                   Edit
                                </a>                                
                                <a class="btn btn-xs btn-danger" href="sections/delete/{{section.id}}" onclick="return confirm('Are you sure you want to DELETE this section?')">
                                   Delete
                                </a>
                            </td>
                        </tr>
                    </template>

        
                <a href="sections/add" class="btn btn-primary"><i class="fa fa-plus-o" aria-hidden="true"></i> Add section</a>
                </div>

                    <script type="text/javascript">
                    Vue.component('sectioncomp', {
                        template: '#template-section',
                        props: ['sectioncomp'],
                    })

                    new Vue({
                        el: '.container-vue',
                        data: {
                            sections: [],
                            pagination: {}
                        },
                        ready: function () {
                            this.fetchSections()
                        },
                        methods: {
                            fetchSections: function (section_url) {
                                let vm = this;
                                section_url = section_url || '/api/sections'
                                this.$http.get(section_url)
                                    .then(function (response) {
                                        vm.makePagination(response.data)
                                        vm.$set('sections', response.data.data)
                                    });
                            },
                            makePagination: function(data){
                                let pagination = {
                                    current_section: data.current_section,
                                    last_section: data.last_section,
                                    next_section_url: data.next_section_url,
                                    prev_section_url: data.prev_section_url
                                }
                                this.$set('pagination', pagination)
                            },
                            viewPage: function (id) {
                                let vm = this;
                                vm.$http.get('/api/section/' + id).then(function(response) {
                                    vm.$set('sections', response.data.data)
                                })
                                vm.$els.taskinput.focus()
                                vm.edit = true
                            }
                        }
                    });   

                </script>


                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>