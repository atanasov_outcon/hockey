<?php $__env->startSection('content'); ?>

<div class="container one-news-page">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
       <br>
<br>
<br>
<br>
<br>

<ol class="breadcrumb">
  <li class="breadcrumb-item"><a href="<?php echo e(url('/')); ?>">Начало</a></li>
  <li class="breadcrumb-item"><a href="<?php echo e(url('news')); ?>">Новини</a></li>
  <li class="breadcrumb-item active"><?php echo e($new->title); ?></li>
</ol>


                    <h1><?php echo e($new->title); ?></h1>

                <div class="panel-body">
                <?php if(isset($image->filename)): ?>
                <img class="img-responsive center-block" src="<?php echo e('uploads/news/'.$image->filename); ?>">
                <div class="margin20"></div>
                <?php endif; ?>
                    <div class="">
                    <?php echo $new->content; ?>

                    </div>
                </div>



        </div>
    </div>
</div>
<div class="margin50"></div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>