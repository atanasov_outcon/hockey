<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12 "> 
            <div class="panel panel-default">
                  <form method="POST">
                <div class="panel-heading clearfix">Добавяне на нов запис 
                    <span class="pull-right">
                      <button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-xs btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>
                    </span>
                </div>
                
                <div class="panel-body">
                   
                    
                     <?php echo e(csrf_field()); ?>


                      <?php if( isset($error) ): ?>
                          <div class="alert alert-danger">
                            <?php echo e($error); ?>

                          </div>
                      <?php endif; ?>                    

                      <?php if( isset($success) ): ?>
                          <div class="alert alert-success">
                            <?php echo e($success); ?>

                          </div>
                      <?php endif; ?>

                      <div class="form-group">
                        <label for="title">Заглавие <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="title" id="title" placeholder="Заглавие" value="">
                      </div>      

                      <div class="form-group">
                        <label for="title">Slug <i title="" class="fa fa-exclamation-circle text-danger tip" data-original-title="required"></i></label>
                        <input required type="text" class="form-control" name="slug" id="slug" placeholder="Slug" value="">
                      </div>     
                      
                      <div class="form-group">
                        <label for="content">Описание</label>
                        <textarea name="content" id="content" class="form-control" cols="30" rows="5"></textarea>
                      </div>                  
   <hr>
                      <div class="checkbox">
                        <label>
                          <input name="active" type="checkbox" value="1" > Активен
                        </label>
                      </div>

  <hr>
                      <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Запиши</button>
                      <a href="<?php echo e(route($controller)); ?>" class="btn btn-danger"><i class="fa fa-ban fa-fw"></i> Отказ</a>

                </div></form>
            </div>
        </div>        

    </div>



<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>