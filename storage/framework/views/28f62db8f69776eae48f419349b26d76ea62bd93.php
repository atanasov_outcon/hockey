<!DOCTYPE html>
<html lang="<?php echo e(app()->getLocale()); ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    
    <!-- CSRF Token -->
    <meta id="token" name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title><?php if(isset($metaTitle) == true): ?> <?php echo e($metaTitle); ?> | <?php endif; ?> <?php echo e(config('app.name', 'UnicomsGateway')); ?></title>
    <meta name="description" content="<?php if(isset($metaDescription) == true): ?> <?php echo e($metaDescription); ?> <?php endif; ?>">

    <title><?php echo e(config('app.name', 'Laravel')); ?></title>


        <!-- vue js -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
    

        <!-- include libraries(jQuery, bootstrap) -->
    <script src="js/vendor/modernizr.min.js"></script> 
    <script src="js/vendor/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script> 
    <script src="js/skrollr.min.js"></script> 



    <!-- Styles -->
    <link href="<?php echo e(asset('css/bootstrap.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/normalize.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/font-awesome.min.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet">


</head>