 <?php $__env->startSection('content'); ?>



    
<!--header end-->
<!--banner-->
    <section class="banner">
        <div class="banner-in">
            <h1>WE CAN CHANGE footBALL togETHER</h1>
            <p><span>Mega League</span> – the football league to unite the top clubs in Southeastern Europe!</p>
            <a id="myBtn" class="btn">You can make it happen now!</a>
            <div class="down-arrow"><a href="#down"><img src="images/down-arrow.png" width="25" height="25" alt="down-arrow"></a></div>
        </div>
    </section>  
<!--banner end-->
<!--project-->

<div class="project-block">
  <div id="project"></div>
    <div id="down"></div>
  <div class="project-left">
      <div class="title">stronger <br>together</div>
        <h2><span>01</span> Project</h2>
        <ul>
          <li>
              <span>Population:</span>
                <h3>90+ Million</h3>
            </li>
            <li>
              <h3>17</h3>
              <span>National Associations</span>
            </li>
            <li>
                <h3>2</h3>
                <span>Levels</span>
            </li>
            <li>
                <h3>42</h3>
                <span>Teams</span>
            </li>
        </ul>
    </div>
    <div class="map-section">
      <figure><img src="images/map.png" width="965" height="572" alt="map"></figure>
    </div>
    <div class="clear"></div>
    
    <div class="usps">
      <ul>
          <li>
              <figure><img src="images/icon4.png" width="34" height="41" alt="icon"></figure>
                <h3>The Problem </h3>
                <p><span>In the modern globalised </span> world, clubs from smaller countries in Southeastern Europe have become football dwarfs. The limited financial resources they posses impact negatively their competitiveness, as homegrown talent constantly pursues better financial propositions.  </p>
            </li>
            <li>
              <figure><img src="images/icon5.png" width="39" height="41" alt="icon"></figure>
                <h3>The SOLUTION </h3>
                <p>We offer a viable, long-term solution with the formation of a <span>unified league</span> for the top clubs in the region. This will create a level, contested environment, renew historic rivalries, and attract lucrative broadcasting and sponsorship deals.  </p>
            </li>
            <li>
              <figure><img src="images/icon6.png" width="57" height="41" alt="icon"></figure>
                <h3>Mission & VISION</h3>
                <p>Our mission is to promote this concept and to establish and administer <span>Mega League</span>. By utilizing the support of the millions of football fans in the region, we firmly believe Mega League could be formed by Season 2019/2020.  </p>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</div>
<!--project end-->
<!--economic section-->
  <div class="economic-section">
      
      <div class="title1">
          <h2>ECONOMICS</h2>
            <h3>ECONOMIC EFFECT</h3>
        </div>
        
        <div class="block">
          <div class="block-left">
              <h2>Broadcasting Revenues</h2>
                <p><span>Mega League</span> aspires to be the most watched sports event in the region, and our projection for annual Broadcasting Revenues forecasts over 200 million EUR per season.</p>
            </div>
            <div class="block-right">
              <figure><img src="images/img1.png" width="709" height="277" alt="img1"></figure>
            </div>
            <div class="clear"></div>
        </div>
        <div class="block">
          <div class="block-left">
              <h2>Matchday Income </h2>
                <p><span>Mega League</span> will set the stage for some of the fiercest rivalries in football, aiming to bring the fans back to the stadiums. At the same time, the clubs will acquire the resources to invest in bigger and better facilities.</p>
            </div>
            <div class="block-right1">
              <ul>
                  <li>
                        <figure><img src="images/icon7.png" width="63" height="54" alt="icon"></figure>
                        <p>Higher Attendance</p>
                        <span><img src="images/arrow4.png" width="19" height="19" alt="arrow"></span>
                    </li>
                    <li style="background:#005b90;">
                        <figure><img src="images/icon8.png" width="56" height="56" alt="icon"></figure>
                        <p>Higher Revenues</p>
                        <span><img src="images/arrow4.png" width="19" height="19" alt="arrow"></span>
                    </li>
                    <li>
                        <figure><img src="images/icon9.png" width="72" height="54" alt="icon"></figure>
                        <p>Better Stadiums</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
        <div class="block">
          <div class="block-left">
              <h2>Commercial Activities </h2>
                <p><span>Mega League</span> will allow football clubs from the region to finally attract lucrative sponsorship contracts and enhance their merchandising business.</p>
            </div>
            <div class="block-right1">
              <ul>
                  <li>
                        <figure><img src="images/icon10.png" width="62" height="55" alt="icon"></figure>
                        <p>Higher Viewership</p>
                        <span><img src="images/arrow4.png" width="19" height="19" alt="arrow"></span>
                    </li>
                    <li style="background:#005b90;">
                        <figure><img src="images/icon11.png" width="56" height="56" alt="icon"></figure>
                        <p>Lucrative Sponsorships</p>
                        <span><img src="images/arrow4.png" width="19" height="19" alt="arrow"></span>
                    </li>
                    <li>
                        <figure><img src="images/icon12.png" width="56" height="56" alt="icon"></figure>
                        <p>Improved Merchandising</p>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
<!--economic section end-->
<!--FQA SECTION-->
  <div class="fqa-section">
      <div class="title2">
          <h2>FAQ</h2>
            <h3>Frequently asked questions</h3>
        </div>
        
        <ul>
          <li>
              <h4><span>1</span>What type of organization are you?</h4>
                <p><span>A</span>We are a non-profit organization that relies on voluntary donations to realize a project. We are called <strong>Mega League Foundation.</strong></p>
            </li>
            <li>
              <h4><span>2</span>What is the project?</h4>
                <p><span>A</span>We want to establish a unified league for the top football clubs of Southeastern Europe. We call it <strong>Mega League</strong>.</p>
            </li>
            <li>
              <h4><span>3</span>Why is establishing Mega League important?</h4>
                <p><span>A</span>It will create one of the most interesting and contested sports competitions in the world. It will improve the financial well-being of the participating clubs and increase the quality of football in the region</strong>.</p>
            </li>
            <li class="flt_rt right-fqa">
              <h4><span>4</span>How will you spend the donations?</h4>
                <p><span>A</span>We will spend the donations to:</p>
                
                <div class="block1">
                  <ul>
                      <li>
                          Popularize the <strong>Mega League</strong> concept among football fans in the region
                        </li>
                        <li>
                          Organize seminars, workshops, and meetings between the stakeholders in <strong>Mega League</strong> (football clubs, national and international football organizations, media companies, sponsors, fan organizations, etc.)
                        </li>
                        <li>
                          Conduct research and collect data, so we can establish, quantify and present the benefits of <strong>Mega League</strong> to the stakeholders
                        </li>
                        <li>
                          Establish an operational apparatus and structure to administer and supervise <strong>Mega League</strong>
                        </li>
                        <li>
                          Finance the operational expenses of the foundation
                        </li>
                    </ul>
                </div>
            </li>
            <li class="no_bor">
              <h4><span>5</span>When is Mega League expected to fully commence?</h4>
                <p><span>A</span>We expect to hold the pilon season of <strong>Mega League</strong> in <strong>2019/20</strong>.</p>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
<!--FQA SECTION END-->
<!--teams section-->
  <div class="teams-section">
      <div id="format"></div>        
        <div class="teams-left">
        <div class="title3">
          <h2><span>02</span> teams & format</h2>
        </div>
          <p>To ensure the most lucrative broadcasting deals and a gripping competition, the clubs in <strong>Mega League</strong>’s pilot season are pre-selected based on popularity, traditional rivalries, and other factors. Afterwards, <strong>Mega League</strong> will employ a fair promotion and relegation system.</p>
            <ul>
              <li class="bor1">At the end of each season the 3 worst-performing clubs from <strong>ML1</strong> will be relegated to <strong>ML2</strong>, while the top 3 clubs in <strong>ML2</strong> will receive promotion to <strong>ML1</strong>.</li>
                <li>At the end of each season the bottom 5 clubs from <strong>ML2</strong> will be relegated to their respective top domestic league, and the top 5 clubs from the <strong>ML Qualification Tournament</strong> will be promoted to <strong>ML2</strong>.</li>
                <li>At the end of each season the top 40 clubs from the top domestic leagues will play in the <strong>ML Qualification Tournament</strong> to determine the 5 clubs that will be promoted to <strong>ML2</strong> for the following season.</li>
            </ul>
        </div>
        <div class="teams-right">
            <div class="teams-right-in">
                <div class="ml flt_lt"><figure><img src="images/ml-1.png" width="447" height="749" alt="ml"></figure></div>
                <div class="arrows">
                    <span>
                        3
                        <img src="images/arrow1.png" width="44" height="17" alt="arrow">
                    </span>
                    <span>
                        3
                        <img src="images/arrow2.png" width="44" height="17" alt="arrow">
                    </span>
                </div>
                <div class="ml flt_rt"><figure><img src="images/ml-2.png" width="447" height="819" alt="ml"></figure>
                  <span>
                      <img src="images/arrow3.png" width="77" height="44" alt="arrow3">
                        5 teams promoted to Mega League 2
                    </span>
                </div>
                <div class="clear"></div>
            </div>
            <div class="flags">
              <h3>Qualification tournament</h3>
              <ul>
                  <li><a href="#"><img src="images/flag1.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag2.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag3.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag4.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag5.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag6.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag7.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag8.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag9.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag10.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag11.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag12.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag13.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag14.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag15.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag16.png" width="42" height="42" alt="flag"></a></li>
                    <li><a href="#"><img src="images/flag17.png" width="42" height="42" alt="flag"></a></li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>
        <div class="clear"></div>
        
        <div class="bottom-text-team-format">
        <p>The best example to illustrate the proposed format for <strong>Mega League</strong> is the Italian League System. <strong>Mega League 1</strong> will be just like Serie A in Italy. During a season (from July to May) each club will play the others twice (a double round-robin system), once at their home stadium and once at that of their opponents', for a total of 38 games. The 3 lowest placed teams are relegated into <strong>Mega League 2</strong> (like Serie B in Italy), and the 3 top teams from <strong>Mega League 2</strong> are promoted in their place. The format of <strong>Mega League 2</strong> is the same, but with 22 teams and 42 rounds.</p>
        <p>The current first-tier national championships (like Superleague Greece, First Professional Football League in Bulgaria, Liga I in Romania, Serbian Superliga, etc.) will become the third football level in the <strong>Mega League System</strong>, like Serie C in Italy. The <strong>ML Qualification Tournament</strong> between the 40 best-placed teams from the top national championships will determine the 5 clubs to be promoted to <strong>Mega League 2</strong> in place of the 5 relegated.</p>
        </div>
        
    </div>
<!--teams section end-->
<!--news section-->
  <div class="news-section">
      <div id="news"></div>
      <div class="title3">
          <h2><span>03</span> news</h2>
        </div>
    
    <div class="title3 text-center">
          <h2>Coming soon!</h2>
      <br>
      <br>
      <br>
        </div>


        <?php
        foreach( $lastNews as $new ):

    
        if( $new->images()->where('is_main', 1)->first() ){
            $image = $new->images()->where('is_main', 1)->first()->filename; 
            $imagePath = 'uploads/news/'.$image;
        }
        else {
            $imagePath = 'uploads/news/no-image.jpg';
        }
        ?>
                <a href="<?php echo e(url('news')); ?>/<?php echo e($new->slug); ?>">
                  <div class="news-block">
                    <figure><img src="<?php echo e($imagePath); ?>" width="709" height="472" alt="img2"></figure>
                      <div class="news-cont"><p><span><?php echo e($new->title); ?></span></p></div>
                  </div>
                </a>
        

        <?php endforeach; ?>




        <div class="clear"></div>
        
        <ul>

        <?php
        foreach( $lastSixNews as $new ):

    
        if( $new->images()->where('is_main', 1)->first() ){
            $image = $new->images()->where('is_main', 1)->first()->filename; 
            $imagePath = 'uploads/news/'.$image;
        }
        else {
            $imagePath = 'uploads/news/no-image.jpg';
        }
        ?>
        
            <li>
              <a href="<?php echo e(url('news')); ?>/<?php echo e($new->slug); ?>">
                  <figure><img src="<?php echo e($imagePath); ?>" width="223" height="167" alt="img"></figure>
                    <span><?php echo e($new->title); ?></span>
                </a>
            </li>
            
        <?php endforeach; ?>
         
        </ul>
        <div class="clear"></div>
    </div>
<!--news section end-->
<!--partners section-->
  <div class="partners">
      <div id="partners"></div>
      <div class="title3">
          <h2><span>04</span> partners</h2>
        </div>
    <div class="title3 text-center">
          <h2>Coming soon!</h2>
        </div>
        <div class="partner-logos">
          <ul>
              
            </ul>
          <div class="clear"></div>
        </div>
        <div class="ambassadors">
          <h3>AMBASSADORS</h3>
                      <div class="clear"></div>
            <h3>Coming soon!</h3>
        </div>
    </div>
<!--partners section end-->
<!--CONTACTS section-->
  <div class="contact-section">
      <div id="contact"></div>
      <div class="contact-block">
        <div class="contact-left">
            <div class="title3">
                <h2 id="contacts"><span>05</span> CONTACTS</h2>
            </div>
            <p>For questions and comments <a href="mailto:info@megaleague.org">info@megaleague.org</a></p>
           
            <p>For press materials <a href="/files/MegaLeaguePressPackage.rar">download our press package</a> </p>
        </div>
        <div class="form-section container-vue">
          <h6>Contact Form</h6>

          <form id="" method="post" action="index.php#contacts" />
                            <p>Please leave a message and we will get back to you as soon as possible</p>
            
            <ul class="flt_lt">
              <li>

                <input required  name="name" type="text" value="Name" class="text-fld" onblur="//if (this.value == '') {this.value = 'Name';}"
 onfocus="if (this.value == 'Name') {this.value = '';}"></li>
                <li>

                  <input required name="email" type="email" value="Email" class="text-fld" onblur="//if (this.value == '') {this.value = 'Email';}"
 onfocus="if (this.value == 'Email') {this.value = '';}"></li>
                <li>

                  <input required name="phone" type="text" value="Phone no" class="text-fld2 flt_lt" onblur="//if (this.value == '') {this.value = 'Phone No (Optional)';}"
 onfocus="if (this.value == 'Phone No') {this.value = '';}"> 

 <input required name="subject" type="text" value="Subject" class="text-fld2 flt_rt" onblur="//if (this.value == '') {this.value = 'Subject';}"
 onfocus="if (this.value == 'Subject') {this.value = '';}"></li>
            </ul>
            <ul class="flt_rt">
              <li><textarea required name="message" cols="1" rows="1" class="text-fld3" onblur="//if (this.value == '') {this.value = 'Your Message';}"
 onfocus="if (this.value == 'Your Message') {this.value = '';}">Your message</textarea>
          <input v-on:click="sendMail(message)" type="submit" value="SEND IT" class="btn1">
        </li>
            </ul>
                  <div class="clear"></div>
          <br> 

        <div class="g-recaptcha" data-sitekey="6Ldk2zIUAAAAAC3Sd6k2tfEVyRFo5iXjqUerjKIP"></div>
      </form>
      <div class="clear"></div>
        </div>

        <div class="clear"></div>
        </div>
        <div class="team" >
          <h3>Our team</h3>
          <ul>
              <li>
                  <figure>
                      <img src="images/team1.png" width="186" height="186" alt="team">
                      <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                    </figure>
                    <h6>Rumen Vatkov</h6>
                     <small style="margin-left:80px;">  <a href="https://www.linkedin.com/in/rumen-vatkov-20893064" target="_blank"><img src="images/link.png" width="22" height="22" alt="icon"></a></small>

                    <small>Founder</small>
                </li>
                <li>
                  <figure>
                      <img src="images/team2.png" width="186" height="186" alt="team">
                      <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                    </figure>
                    <h6>Megi Slavova</h6>
                     <small style="margin-left:80px;">  <a href="https://www.linkedin.com/in/megi-slavova/" target="_blank"><img src="images/link.png" width="22" height="22" alt="icon"></a></small>

                    <small>Digital Marketing</small>
                </li>
                <li>
                  <figure>
                      <img src="images/Outsource-Consulting-2.png" width="186" height="186" alt="Outsource-Consulting">
                      <span><img src="images/flag18.png" width="36" height="35" alt="flag"></span>
                    </figure>
                    <h6>Outsource Consulting<span></h6>
                    <small style="margin-left:80px;">  <a href="https://www.linkedin.com/company/outsource-consulting-ltd" target="_blank"><img src="images/link.png" width="22" height="22" alt="icon"></a></small>

                    <small>Web Services<br></small>

                </li>
                <li>
                  <figure>
                      <img src="images/team4.png" width="186" height="186" alt="team">
                      <span><img src="images/flag19.png" width="36" height="35" alt="flag"></span>
                    </figure>
                    <h6>Alex Gorbanescu</h6>
                    <small style="margin-left:80px;">  <a href="https://www.linkedin.com/in/alexgorbanescu/" target="_blank"><img src="images/link.png" width="22" height="22" alt="icon"></a></small>

                    <small>Graphic Design</small>
                </li>
            </ul>
        </div>
      </span>
    </div>






<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.frontend', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>