<!doctype html>
<html lang="en">
<head>
<base href="http://aanvweb.dev/public/" target="_self">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="format-detection" content="telephone=no">
<meta name="description" content="Mega League is a concept football league, which will unite the top clubs in Southeastern Europe and will narrow the competitive gap between them and clubs of Western Europe.">
<!--<meta name="viewport" content="width=device-width">-->
<title>MEGA LEAGUE - WE CAN MAKE IT HAPPEN!</title>

<meta property="fb:app_id"        content="144085519545228" /> 
<meta property="og:url"           content="http://megaleague.org/" />
<meta property="og:type"          content="website" />
<meta property="og:title"         content="MEGA LEAGUE - WE CAN MAKE IT HAPPEN!" />
<meta property="og:description"   content="Mega League is a concept football league, which will unite the top clubs in Southeastern Europe and will narrow the competitive gap between them and clubs of Western Europe." />
<meta property="og:image"         content="http://megaleague.org/images/map.png" />

<script src='https://www.google.com/recaptcha/api.js'></script>
 <script src="http://cdnjs.cloudflare.com/ajax/libs/vue/1.0.24/vue.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue-resource/0.7.0/vue-resource.js"></script>
<!-- SET: FAVICON -->
<link rel="shortcut icon" type="image/x-icon" href="images/favicon.png">
<!-- END: FAVICON -->
<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet">

    <link href="<?php echo e(asset('public/css/style.css')); ?>" rel="stylesheet">
    <link href="<?php echo e(asset('public/css/responsive.css')); ?>" rel="stylesheet">

<!-- Start: Stylesheet-->
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/slick/slick.css')); ?>"/>
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/slick/slick-theme.css')); ?>"/>
<link href="<?php echo e(asset('public/css/ionicons.min.css')); ?>" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/style.css?ver=2.75')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/responsive.css')); ?>">
<link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/grid.css')); ?>">


    <script>
window.addEventListener("load", function(){
window.cookieconsent.initialise({
  "palette": {
    "popup": {
      "background": "#00427a",
      "text": "#ffffff"
    },
    "button": {
      "background": "#ee7e1a",
      "text": "#ffffff"
    }
  },
  "theme": "classic",
  "position": "bottom-right"
})});
</script>

<!-- End: Cookies alert-->

<!--[if lt IE 9]>
<script type="text/javascript">
  document.createElement("header");
  document.createElement("nav");
  document.createElement("section");
  document.createElement("article");
  document.createElement("aside");
  document.createElement("footer");
   document.createElement("figure");
</script>
<![endif]-->
<!-- End: Stylesheet-->




<!-- End: Stylesheet-->
<script src="<?php echo e(asset('public/js/1.11.3.jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('public/js/jquery.easing.1.3.js')); ?>"></script>
<script type="text/javascript" src="<?php echo e(asset('public/slick/slick.js')); ?>"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-K66T9ML');</script>
<!-- End Google Tag Manager --> 
<script type="text/javascript">
 $(document).ready(function(e) {
   $('.nav-icon').click(function(e) {
    $('.nav-icon').toggleClass('open');
    $('body').toggleClass('open');
    
   });
    
    $(window).scroll(function(){
       if ($(this).scrollTop() > 30) {
      $('.header').addClass('stick_header');
       } else {
      $('.header').removeClass('stick_header');
       }
     });
 
 $('.nav ul li a, .down-arrow a').bind('click',function(event){
        var $anchor = $(this);
        
        $('html, body').stop().animate({
          scrollTop: $($anchor.attr('href')).offset().top
        }, 1500,'easeInOutExpo');
        event.preventDefault();
    }); 

 
 });
</script>
<script>
// FOR Equalheight --------------------------

equalheight = function(container){

var currentTallest = 0,
     currentRowStart = 0,
     rowDivs = new Array(),
     $el,
     topPosition = 0;
 $(container).each(function() {

   $el = $(this);
   $($el).height('auto')
   topPostion = $el.position().top;

   if (currentRowStart != topPostion) {
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
     rowDivs.length = 0; // empty the array
     currentRowStart = topPostion;
     currentTallest = $el.height();
     rowDivs.push($el);
   } else {
     rowDivs.push($el);
     currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
  }
   for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
     rowDivs[currentDiv].height(currentTallest);
   }
 });
}

$(window).load(function() {
  equalheight('.block-right1 ul li, .news-section ul li span, .project-left ul li');
});

$(window).resize(function(){
  equalheight('.block-right1 ul li, .news-section ul li span, .project-left ul li');
});

$(document).ready(function(){
  equalheight('.block-right1 ul li, .news-section ul li span , .project-left ul li');
});
</script>

<script>
 $(document).ready(function() {
// Get the modal
var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");
var btn2 = document.getElementById("myBtn2");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function() {
    modal.style.display = "block";
}
btn2.onclick = function() {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}
 });
 
</script>

<!--[if IE 8]>
 <style type="text/css">

   .banner {filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/banner.jpg',sizingMethod='scale');  }
   .economic-section {filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg1.jpg',sizingMethod='scale');  }
   .teams-section {filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg2.jpg',sizingMethod='scale');  }
   .form-section {filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg3.png',sizingMethod='scale');  }
   .donate-section {filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/bg4.jpg',sizingMethod='scale');  }
   
   .cta-donate{background:#007fe7;}
   .cta1{width:55%;}
   .news-cont P span{background:#007fe7;}
   .banner h1{font-size:62px;text-transform:uppercase;}
   .title{color:#eef1f5;text-transform:uppercase;}
   .title1 h2{color:#00355d;text-transform:uppercase;}
   .title2 h2{color:#f3f4f4;text-transform:uppercase;}
   .title4 h2{color:#317bc0;text-transform:uppercase;}
   
</style>
<![endif]-->

</head>
<body>
   

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K66T9ML"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!-- Start: -->
<!-- End: -->

<!-- The Modal -->
<div id="myModal" class="modal">

  <!-- Modal content -->

      <div class="donate-section">
    <span class="close">&times;</span>

      <div id="donate"></div>
      <div class="title4">
          <h2>Make it happen</h2>
        </div>

    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
  <input type="hidden" name="cmd" value="_s-xclick">
  <input type="hidden" name="hosted_button_id" value="LU6EV29GFLE2J">
  <table>
  <tr><td><input type="hidden" name="on0" value="Donation for Mega League Project">Donation for Mega League Project</td></tr><tr><td><select name="os0">
    <option value="Bronze">Bronze : €5.00 EUR - monthly</option>
    <option value="Silver">Silver : €10.00 EUR - monthly</option>
    <option value="Gold">Gold : €15.00 EUR - monthly</option>
    <option value="Platinum">Platinum : €20.00 EUR - monthly</option>
    <option value="Diamond">Diamond : €25.00 EUR - monthly</option>
  </select> </td></tr>
  </table>
  <br>
  <input type="hidden" name="currency_code" value="EUR">
  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_subscribeCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

  </form>
  </div>
  

</div>
<!-- Start: Wrapper-->
<div id="wrapper">
<div class="emt"></div>
<!--header-->
  <header class="header">
      <div class="header-in">
        <div class="logo"><a href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('public/images/logo.png')); ?>" width="116" height="123" alt="logo"></a></div>
                <!--nav-->
                <div class="nav-icon"><span></span><span></span><span></span></div>
                  <nav class="nav">
                      <ul>
                          <li><a href="#project">Project</a></li>
                            <li><a href="#format">Teams & Format</a></li>
                            <li><a href="#news">News</a></li>
                            <li><a href="#partners">Partners</a></li>
                            <li><a href="#contact">Contacts</a></li>
                            <li><a href="#donate" class="active">Donate</a></li>
                        </ul>
                        <div class="clear"></div>
                    </nav>
                <!--nav end-->
                <div class="icons">
                  <a href="https://www.facebook.com/megaleaguefoundation/" target="_blank"><img src="images/icon1.png" width="14" height="24" alt="icon"></a>
                    <a href="https://www.linkedin.com/company-beta/18023969" target="_blank"><img src="images/icon2.png" width="22" height="22" alt="icon"></a>
                    <a href="https://www.youtube.com/channel/UC8rLF38UArYsQlonPFUmUEA" target="_blank"><img src="images/icon3.png" width="27" height="22" alt="icon"></a>
                    <div class="clear"></div>
                </div>
                <div class="clear"></div>
                
        </div>
    </header>

    <?php echo $__env->yieldContent('content'); ?>


<!--CONTACTS section end-->
<!--Donate section-->
  <div class="donate-section">
      <div id="donate"></div>
      <div class="title4">
          <h2>Make it happen</h2>
        </div>
      <div class="title3">
            <h2><span>06</span> Donate</h2>
        </div>
        
        <div class="donate-left">
          <p>The <strong>Mega League Foundation</strong> collaborates with investors, sponsors, media companies, and various football organisations, so we can create a large following of supporters among football fans in the region.</p>
            <p>You can help by subscribing to a monthly donation and by introducing the <strong>Mega League</strong> concept to as many football enthusiasts as possible.</p>
        </div>
        <div class="donate-right">
          <h6>We will spend the donations to:</h6>
            <ul>
              <li>Popularize the <strong>Mega League</strong> concept among football fans in the region</li>
                <li>Organize seminars, workshops, and meetings between the stakeholders in <strong>Mega League</strong> (football
      clubs, national and international football organizations, media companies, sponsors, fan
      organizations, etc.)</li>
                <li>Conduct research and collect data, so we can establish, quantify and present the benefits of <strong>Mega League</strong> to the stakeholders</li>
            <li>Establish an operational apparatus and structure to administer and supervise <strong>Mega League</strong></li>
                <li>Finance the operational expenses of the foundation</li>
            </ul>
        </div>
        <div class="clear"></div>
        
        <div class="cta-donate">
          <div class="cta1">
              <p>Become part of this extraordinary project and change football forever!</p>
            </div>
            <a id="myBtn2">DONATE NOW!</a>
        </div>
    </div>
<!--Donate section end-->
</div>


<!-- Wrapper end-->
</body>
</html>

   