<!doctype html>
<html lang="en">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="UTF-8">
    <title><?php if(isset($metaTitle)) echo $metaTitle. " | "; ?>eKids</title>

    <script src="https://use.fontawesome.com/1f1fa1e0ca.js"></script>
    
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>

     <script src="https://unpkg.com/vue/dist/vue.js"></script> 
     <script src="https://unpkg.com/vue-resource/dist/vue-resource.js"></script> 


      <!-- <script src="https://unpkg.com/vue/dist/vue.min.js"></script>  -->
     <!-- <script src="https://unpkg.com/vue-resource/dist/vue-resource.min.js"></script>  -->
 
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->



<!-- slider -->
    <style type="text/css">@import  url(//fonts.googleapis.com/css?family=PT+Serif:regular&subset=cyrillic);
        @import  url(//fonts.googleapis.com/css?family=PT+Serif:regular&subset=cyrillic);
        @import  url(//fonts.googleapis.com/css?family=PT+Serif:regular&subset=cyrillic);
    </style>

    <link href="http://fonts.googleapis.com/css?family=Lora:400%2C500%7CCinzel:400" rel="stylesheet" property="stylesheet" type="text/css" media="all">
    
    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('public/css/style.css')); ?>">

</head>

<body>
    
    <div class="balloons">
    <div class="clouds">
        <div class="head">&nbsp;</div>
        
        <div class="container">
            <ul class="row social-links">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
			

			<header>
				<div class="row">
	        		<img class="brand" src="<?php echo e(asset('public/images/logo.png')); ?>" alt="">
				</div>
        			<ul class="navbar-ekids row">
                    	<li class="active"><a href="#"><i class="fa fa-home"></i></a></li>
                    	<li><a href="#">Супер анимации</a></li>
                    	<li><a href="#">Страхотни предавания</a></li>
                    	<li><a href="#">Блог за детски психолог</a></li>
                    	<li><a href="#">Намерете ни</a></li>
            		</ul>					
			</header>

			<div class="slider">
				<div class="slide col-md-12" style="background-image: url('http://www.ekids.bg/wordpress/wp-content/uploads/2016/07/LPS1.jpg')" alt="">
			</div>
        </div>


        <br>
        <br>
        <br>
        <br>    <br>
        <br>
        <br>    <br>
        <br>
        <br>
        <br>
    </div>
    </div>

<?php echo $__env->yieldContent('content'); ?>   


</body>
</html>
