<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// API routes
Route::any('api/register', 'HomeController@register');
Route::any('api/login', 'HomeController@login');
Route::any('api/reset', 'HomeController@reset');
Route::any('api/saveValues', 'HomeController@saveValues');
Route::any('api/applications', 'HomeController@applications');
Route::any('api/get_category_info', 'ApiController@category_info');
Route::any('api/get_by_category', 'ApiController@category');
Route::any('api/get_product/{slug}', 'ApiController@product');


Route::any('api/emailAvailability', 'HomeController@emailAvailability');
Route::any('api/isUserLogged', 'HomeController@isUserLogged');
Route::any('api/logout', 'HomeController@logout');


Route::any('/', 'HomeController@index')->name('shop');
Route::any('/index', 'HomeController@index')->name('index');

Route::any('/image/{max}/{thumb_link?}', 'HomeController@image')->name('image');

// Manage home
Route::get('/home', 'HomeController@manage')->name('home');
// Route::get('/manage', 'HomeController@manage')->name('manage');

Route::get('404', 'HomeController@error404')->name('404');


// MANAGE ROUTES
Route::any('manage/categories/set_main_image/{itemId}/{imageId}', 'Manage\CategoriesController@set_main_image');
Route::any('manage/categories/delete_image/{id}', 'Manage\CategoriesController@delete_image');
Route::any('manage/categories/upload/{id}', 'Manage\CategoriesController@upload');
Route::any('manage/categories/delete/{id}', 'Manage\CategoriesController@delete');
Route::any('manage/categories/edit/{id}', 'Manage\CategoriesController@edit');
Route::any('manage/categories/add', 'Manage\CategoriesController@add');
Route::get('manage/categories', 'Manage\CategoriesController@index')->name('categories');
Route::get('api/categories/uploads/{id}', 'Manage\CategoriesController@listImages');
Route::get('api/categories/{parentOrder?}', 'Manage\CategoriesController@list');

Route::any('manage/vendors/set_main_image/{itemId}/{imageId}', 'Manage\VendorsController@set_main_image');
Route::any('manage/vendors/delete_image/{id}', 'Manage\VendorsController@delete_image');
Route::any('manage/vendors/upload/{id}', 'Manage\VendorsController@upload');
Route::any('manage/vendors/delete/{id}', 'Manage\VendorsController@delete');
Route::any('manage/vendors/edit/{id}', 'Manage\VendorsController@edit');
Route::any('manage/vendors/add', 'Manage\VendorsController@add');
Route::get('manage/vendors', 'Manage\VendorsController@index')->name('vendors');
Route::get('api/vendors/uploads/{id}', 'Manage\VendorsController@listImages');
Route::get('api/vendors/{parentOrder?}', 'Manage\VendorsController@list');



Route::any('manage/pages/set_main_image/{itemId}/{imageId}', 'Manage\PagesController@set_main_image');
Route::any('manage/pages/delete_image/{id}', 'Manage\PagesController@delete_image');
Route::any('manage/pages/upload/{id}', 'Manage\PagesController@upload');
Route::any('manage/pages/delete/{id}', 'Manage\PagesController@delete');
Route::any('manage/pages/edit/{id}', 'Manage\PagesController@edit');
Route::any('manage/pages/add', 'Manage\PagesController@add');
Route::get('manage/pages', 'Manage\PagesController@index')->name('pages');
Route::get('api/pages/uploads/{id}', 'Manage\PagesController@listImages');
Route::get('api/pages/{parentOrder?}', 'Manage\PagesController@list');


Route::any('manage/users/set_main_image/{itemId}/{imageId}', 'Manage\UserController@set_main_image');
Route::any('manage/users/delete_image/{id}', 'Manage\UserController@delete_image');
Route::any('manage/users/upload/{id}', 'Manage\UserController@upload');
Route::any('manage/users/delete/{id}', 'Manage\UserController@delete');
Route::any('manage/users/edit/{id}', 'Manage\UserController@edit');
Route::any('manage/users/add', 'Manage\UserController@add');
Route::get('manage/users', 'Manage\UserController@index')->name('users');
Route::get('api/users/uploads/{id}', 'Manage\UserController@listImages');
Route::get('api/users/{parentOrder?}', 'Manage\UserController@list');

Route::any('manage/orders/set_main_image/{itemId}/{imageId}', 'Manage\OrdersController@set_main_image');
Route::any('manage/orders/delete_image/{id}', 'Manage\OrdersController@delete_image');
Route::any('manage/orders/upload/{id}', 'Manage\OrdersController@upload');
Route::any('manage/orders/delete/{id}', 'Manage\OrdersController@delete');
Route::any('manage/orders/edit/{id}', 'Manage\OrdersController@edit');
Route::any('manage/orders/add', 'Manage\OrdersController@add');
Route::get('manage/orders', 'Manage\OrdersController@index')->name('orders');
Route::get('api/orders/uploads/{id}', 'Manage\OrdersController@listImages');
Route::get('api/orders/{parentOrder?}', 'Manage\OrdersController@list');

Route::any('manage/collections/set_main_image/{itemId}/{imageId}', 'Manage\CollectionsController@set_main_image');
Route::any('manage/collections/delete_image/{id}', 'Manage\CollectionsController@delete_image');
Route::any('manage/collections/upload/{id}', 'Manage\CollectionsController@upload');
Route::any('manage/collections/delete/{id}', 'Manage\CollectionsController@delete');
Route::any('manage/collections/edit/{id}', 'Manage\CollectionsController@edit');
Route::any('manage/collections/add', 'Manage\CollectionsController@add');
Route::get('manage/collections', 'Manage\CollectionsController@index')->name('collections');
Route::get('api/collections/uploads/{id}', 'Manage\CollectionsController@listImages');
Route::get('api/collections/{parentOrder?}', 'Manage\CollectionsController@list');


Route::any('manage/codes/set_main_image/{itemId}/{imageId}', 'Manage\CodesController@set_main_image');
Route::any('manage/codes/delete_image/{id}', 'Manage\CodesController@delete_image');
Route::any('manage/codes/upload/{id}', 'Manage\CodesController@upload');
Route::any('manage/codes/delete/{id}', 'Manage\CodesController@delete');
Route::any('manage/codes/edit/{id}', 'Manage\CodesController@edit');
Route::any('manage/codes/add', 'Manage\CodesController@add');
Route::get('manage/codes', 'Manage\CodesController@index')->name('codes');
Route::get('api/codes/uploads/{id}', 'Manage\CodesController@listImages');
Route::get('api/codes/{parentOrder?}', 'Manage\CodesController@list');



Route::any('manage/products/set_main_image/{itemId}/{imageId}', 'Manage\ProductsController@set_main_image');
Route::any('manage/products/delete_image/{id}', 'Manage\ProductsController@delete_image');
Route::any('manage/products/upload/{id}', 'Manage\ProductsController@upload');
Route::any('manage/products/delete/{id}', 'Manage\ProductsController@delete');
Route::any('manage/products/edit/{id}', 'Manage\ProductsController@edit');
Route::any('manage/products/add', 'Manage\ProductsController@add');
Route::get('manage/products', 'Manage\ProductsController@index')->name('products');
Route::get('api/products/uploads/{id}', 'Manage\ProductsController@listImages');
Route::get('api/products', 'Manage\ProductsController@list');



Route::any('manage/attributes/set_main_image/{itemId}/{imageId}', 'Manage\AttributesController@set_main_image');
Route::any('manage/attributes/delete_value/{id}', 'Manage\AttributesController@delete_value');
Route::any('manage/attributes/add_value/{id}', 'Manage\AttributesController@add_value');
Route::any('manage/attributes/delete/{id}', 'Manage\AttributesController@delete');
Route::any('manage/attributes/edit/{id}', 'Manage\AttributesController@edit');
Route::any('manage/attributes/add', 'Manage\AttributesController@add');
Route::get('manage/attributes', 'Manage\AttributesController@index')->name('attributes');
Route::get('api/attributes/values/{id}', 'Manage\AttributesController@listValues');
Route::get('api/attributes', 'Manage\AttributesController@list');



# временен route
Route::any('/{vue_capture?}', 'HomeController@index')->where('vue_capture', '[\/\w\.-]*');
# да се откоментира в PRODUCTION
# кеширане на output съдържанието
# за триене на кеш-а http://site.com/clear-cache
#
// Route::get('/clear-cache', function() {
//     $exitCode = Artisan::call('cache:clear');
//     // return what you want
// });


// Route::group(['middleware' => 'cachepage'], function () 
// {
// 	Route::any('/{vue_capture?}', 'HomeController@index')->where('vue_capture', '[\/\w\.-]*');
// });

####################

